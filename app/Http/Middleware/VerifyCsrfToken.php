<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        '/pcr-table',
        '/labpcr-table',
        '/analizepcr-table',
        '/pcrdikirim-table',
        '/pcrditerima-table',
        '/pcrprosessekstraksi-table',
        '/regencies',
        '/districts',
        '/villages',
        '/selectbranch',
        '/selectallbranch',
        '/pcr-destroy',
        '/pcr-generate-code',
        '/antigen-table',
        '/get-branch',
        '/filter-branch',
        '/qrpatient-table',
        '/kwitansi-table',
        '/setting-save',
        '/setting_user_save',
        '/getpatient',
        '/qrpatient-store',
        '/kwitansi-addmore',
        '/pcr-setqr',
        '/pcr-validdata'
    ];
}
