<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use File;
use Mail;
use App\Models\Pcr;
use App\Models\Cabang;
use App\Models\Inspeksi;
use App\Models\Qrpatient;
use App\Exports\PcrExport;
use App\Models\StatusSwab;
use Illuminate\Http\Request;
use App\Exports\PcrExportAll;
use App\Models\IdentitasType;
use App\Models\DetailInspeksi;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrpatientController extends Controller {
    
    public function index() 
    {

        if (get_data_login()->mitra_id !='' ){
            if (get_data_login()->branch_id !='' ){
                $branchs = Cabang::where('id',get_data_login()->branch_id)->get();
            }
            else{
                $branchs = Cabang::where('mitra_id',get_data_login()->mitra_id)->get();
            }
        }else{
            $branchs = Cabang::all();
        }

        
        return view('qrpatient.index', compact('branchs'));
        
    }

    public function table(Request $request)
    {
        $qrpatient = Qrpatient::all();
        return response()->json($qrpatient);
    }



    public function store(Request $request)
    {
        $pcr  = Pcr::where('branch_id',$request->branch_id)->where('qr',0)->get();
        
        foreach ($pcr as $key => $value) {
            $qrpatient = new Qrpatient;
            $qrpatient->id_patient = $value->pasien_no;
            $qrpatient->branch_id = $value->branch_id;
            $qrpatient->mitra_id = $value->mitra_id;
            $qrpatient->print = 0;
            $qrpatient->print_by = '';
            $qrpatient->use = 0;
            $qrpatient->use_by = '';
            $qrpatient->created_by = get_data_login()->name;
            $qrpatient->save();
            DB::select("UPDATE patient SET qr=". $qrpatient->id ." WHERE id=". $value->id ."");
        }
        
        
        return response()->json(array(
            'success' => true,'message'=>'Record has been added'));
    }

    public function printPDF(Request $request){
        $qrpatient = Qrpatient::where('branch_id', $request->branch_id)->get();
        $result = array();
        foreach ($qrpatient as $key => $value) {
            for ($i=0; $i < $request->jlm ; $i++) { 
                $data = array(
                    'qrpatient'=>base64_encode(QrCode::format('svg')->size(50)->generate($value->id_patient)),
                    'id_patient'=>$value->id_patient,
                    'id'=>$value->id
                );
                array_push($result,$data);
            }

        }
        $pdf = PDF::loadView('qrpatient.pdfAll', compact('result'));
        return $pdf->stream();
        
    }

}