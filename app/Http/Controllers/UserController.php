<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use App\Models\Cabang;
use App\Models\Mitra;
use Illuminate\Support\Facades\Hash;
use Session;

class UserController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $mitras = Mitra::all();
        return view('user',compact('mitras'));   
    }
    public function setting_user(){
        $data = User::where('id',get_data_login()->id)->first();
        return view('setting_user',compact('data')); 
    }
    public function setting_user_save(Request $request){
        $cekusername = DB::select("SELECT * FROM users where email='". $request->input('email') ."'");
        if (count($cekusername)>0){
            Session::flash('error', 'Email Sudah Digunakan');
            return redirect()->back();
        }
        else{
            $user = User::find(get_data_login()->id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->pass = $request->input('password');
            if (!empty($request->file('gambar'))){
                $filegambar = $request->file('gambar');
                $nama_filegambar = rand().$filegambar->getClientOriginalName();
                $filegambar->move('public/img',$nama_filegambar);
                $user->gambar =$nama_filegambar;
            }
            $user->save();
            return redirect()->back();
        }
    }
    public function profile(){
        $mitras = User::where('id',$request);
        return view('user',compact('mitras'));
    }
    public function getdata(){
        $result = DB::select("select (select m_name from mitra where users.mitra_id=mitra.id)as mitra,(select b_name from cabang where users.branch_id=cabang.id)as branch,id,name as u_name,username as u_username,role as u_usertype, pass as u_password from users");
        return response()->json($result);
    }
    public function store(Request $request) {
        DB::select("ALTER TABLE users AUTO_INCREMENT=0");
        if (empty($request->input('u_name'))){
            return response()->json(['errorMsg'=>'Name Not Found']);
        }
        elseif(empty($request->input('u_username'))){
            return response()->json(['errorMsg'=>'Username Not Found']);
        }
        elseif(empty($request->input('u_password'))){
            return response()->json(['errorMsg'=>'Password Not Found']);
        }
        $cekusername = DB::select("SELECT * FROM users where username='". $request->input('u_username') ."'");
        if (count($cekusername)>0){
            return response()->json(['errorMsg'=>'Username Sudah Pernah Digunakan']);
        }
        try{
            $user = new User();
            $user->name = $request->input('u_name');
            $user->username = $request->input('u_username');
            $user->password = Hash::make($request->input('u_password'));
            $user->pass = $request->input('u_password');
            $user->role = $request->input('u_usertype');
            if ($request->input('u_usertype')=="mitra"){
                $user->mitra_id = $request->input('mitra_id');
            }
            elseif ($request->input('u_usertype')=="branch"){
                $user->mitra_id = $request->input('mitra_id');
                $user->branch_id = $request->input('branch_id');
            }
            elseif ($request->input('u_usertype')!="branch" && $request->input('u_usertype')!="mitra") {

            }
            $user->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
        
    }

    public function update(Request $request, $id){
        if (empty($request->input('u_name'))){
            return response()->json(['errorMsg'=>'Name Not Found']);
        }
        elseif(empty($request->input('u_username'))){
            return response()->json(['errorMsg'=>'Username Not Found']);
        }
        elseif(empty($request->input('u_password'))){
            return response()->json(['errorMsg'=>'Password Not Found']);
        }
        try{
            $user = User::find($id);
            $user->name = $request->input('u_name');
            $user->username = $request->input('u_username');
            $user->password = Hash::make($request->input('u_password'));
            $user->pass = $request->input('u_password');
            $user->role = $request->input('u_usertype');
            if ($request->input('u_usertype')=="mitra"){
                $user->mitra_id = $request->input('mitra_id');
            }
            elseif ($request->input('u_usertype')=="branch"){
                $user->mitra_id = $request->input('mitra_id');
                $user->branch_id = $request->input('branch_id');
            }
            elseif ($request->input('u_usertype')!="branch" && $request->input('u_usertype')!="mitra") {

            }
            $user->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    
    public function delete(Request $request) {
        $user = User::find($request->input('id'));
        try{
            $user->delete();
            return response()->json(['success'=>true]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

}