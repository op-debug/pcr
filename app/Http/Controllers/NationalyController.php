<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Models\Nationaly;
use App\Imports\NationalyImport;
use Maatwebsite\Excel\Facades\Excel;

class NationalyController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function import_excel(Request $request) 
	{
		
        // menangkap file excel
		$file = 'C:/xampp/htdocs/pcr/public/pcr_template/nationaly.xlsx';
 
		// membuat nama file unik
		
		// import data
		Excel::import(new NationalyImport, $file);
    }
}