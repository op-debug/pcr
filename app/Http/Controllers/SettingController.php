<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Settings;
use Config;
class SettingController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $setting = Settings::where('id',1)->first();
        return view('setting',compact('setting'));   
    }

    public function save(Request $request){
        $setting = Settings::where('id',1)->first();

        if (!empty($request->file('favicon'))){
            $filefavicon = $request->file('favicon');
            $nama_filefavicon = rand().$filefavicon->getClientOriginalName();
            $filefavicon->move('public/img',$nama_filefavicon);
            $setting->favicon = $nama_filefavicon;
        }

        if (!empty($request->file('logo'))){
            $filelogo = $request->file('logo');
            $nama_filelogo = rand().$filelogo->getClientOriginalName();
            $filelogo->move('public/img',$nama_filelogo);
            $setting->logo =  $nama_filelogo;
        }

        if (!empty($request->file('logolab'))){
            $filelogolab = $request->file('logolab');
            $nama_filelogolab = rand().$filelogolab->getClientOriginalName();
            $filelogolab->move('public/img',$nama_filelogolab);
            $setting->logolab =$nama_filelogolab;
        }

        if (!empty($request->file('ttd_img'))){
            $filelogottd = $request->file('ttd_img');
            $nama_filelogottd = rand().$filelogottd->getClientOriginalName();
            $filelogottd->move('public/img',$nama_filelogottd);
            $setting->ttd_img =$nama_filelogottd;
        }

        $setting->name = $request->name;
        $setting->copy = $request->copy;
        $setting->max_analize_pcr = $request->analize_max;
        $setting->lab_patient_no = $request->lab_patient_no;
        $setting->no_registrasi_lab = $request->noregisterlab;
        $setting->dokter_penanggungjawab = $request->dokterpenanggungjawab;
        $setting->sip_no = $request->sipno;
        $setting->smpt = $request->host;
        $setting->email = $request->email;
        $setting->password = $request->password;
        $setting->port = $request->port;
        $setting->title = $request->title;
        $setting->subject = $request->subject;
        $setting->encryption = $request->encryption;
        $setting->alamat = $request->alamat;
        $setting->phone = $request->phone;
        $setting->title_footer = $request->title_footer;
        $setting->run_nolab = $request->run_nolab;
        $setting->mitra = $request->mitra;
        $setting->run_mitra = $request->run_mitra;
        $setting->save();
        return redirect()->back();


    }
}