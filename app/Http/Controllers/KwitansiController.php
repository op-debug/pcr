<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use File;
use Mail;
use App\Models\Pcr;
use App\Models\Cabang;
use App\Models\Inspeksi;
use App\Models\Kwitansi;
use App\Models\Qrpatient;
use App\Exports\PcrExport;
use App\Models\StatusSwab;
use Illuminate\Http\Request;
use App\Exports\PcrExportAll;
use App\Models\IdentitasType;
use App\Models\DetailInspeksi;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class KwitansiController extends Controller {
    
    public function index() 
    {
        $identitasTypes = IdentitasType::all();
        $inspeksis = Inspeksi::all();
        $statusSwabs = StatusSwab::all();
        $detailInspeksis = DetailInspeksi::all();
        $branchs = Cabang::all();
        $provinces = DB::table('provinces')->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->orderBy('name', 'ASC')->get();
        $districts = DB::table('districts')->orderBy('name', 'ASC')->get();
        $villages = DB::table('villages')->orderBy('name', 'ASC')->get();

        $pcrs = Pcr::all();
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');
        $pcrs->load('hasKwitansi');

        
        return view('kwitansi.index', compact('pcrs', 'identitasTypes', 'inspeksis', 'statusSwabs', 'detailInspeksis', 'branchs', 'provinces', 'regencies', 'districts', 'villages'));
        
    }

    public function table(Request $request)
    {
        if (get_data_login()->mitra_id !='' ){
            if (get_data_login()->branch_id !='' ){
                $kwitansi = Kwitansi::where('mitra_id', $mitra)->get();
            }
            else{ 
                $kwitansi = Kwitansi::where('mitra_id',get_data_login()->mitra_id)->get();
            }
        }
        else{
            $kwitansi = Kwitansi::all();
        }

        return response()->json($kwitansi);

    }
    
    public function store(Request $request)
    {
        $pcrs = Pcr::whereIn("id",$request->id)->get();
        foreach ($pcrs as $key => $value) {
            $kwitansi = new Kwitansi();
            $kwitansi->patient_id = $value->pasien_no;
            $kwitansi->branch_id = $value->branch_id;
            $kwitansi->mitra_id = $value->mitra_id;
            $kwitansi->year = Date('Y');
            $kwitansi->date = Date("Y-m-d H:i:s");
            $kwitansi->br_year = Date('Y');
            $kwitansi->br_date = Date("Y-m-d H:i:s");
            $kwitansi->price = $value->price;
            $kwitansi->save();

        }
        return response()->json(array(
            'success' => true,'message'=>'Record has been Add'));

    }

    public function addmore(Request $request){
        DB::select("INSERT INTO kwitansi (patient_id,branch_id,
        mitra_id,year,date,service,payment,sampling_point,br_year,br_date,booking_recipt_no,
        no_recipt,description,name,phone,price)  SELECT patient_id,branch_id,
        mitra_id,year,date,service,payment,sampling_point,br_year,br_date,booking_recipt_no,
        no_recipt,description,name,phone,price FROM kwitansi where id='". $request->id ."'");
        return response()->json(array(
            'success' => true,'message'=>'Record has been Add'));
    }
    public function update(Request $request)
    {

        $kwitansi = Kwitansi::where('id', $request->id)->first();
        $kwitansi->no_recipt = $request->no_recipt;
        $kwitansi->booking_recipt_no = $request->booking_recipt_no;
        $kwitansi->payment = $request->payment;
        $kwitansi->name = $request->name;
        $kwitansi->phone = $request->phone;
        $kwitansi->price = $request->price;
        $kwitansi->description = $request->description;
        $kwitansi->save();
        return response()->json(array(
            'success' => true,'message'=>'Record has been Updated'));

    }

    public function printkwitansi(Request $request){
        $qrpatient = Kwitansi::where('id', $request->id)->first();
        $customPaper = array(0,0,567.00,353.80);
        $pdf = PDF::loadView('kwitansi.pdfAll',compact('qrpatient'))->setPaper($customPaper);;
        return $pdf->stream();
    }   
    public function destroy(Request $request) {
        $kwitansi = Kwitansi::whereIn("id",$request->id);
        try{
            $kwitansi->delete();
            return response()->json(['success'=>true,"message"=>"Success Delete"]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
   
}