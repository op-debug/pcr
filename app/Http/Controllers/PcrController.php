<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use File;
use Mail;
use App\Models\Pcr;
use App\Models\Cabang;
use App\Models\Mitra;
use App\Models\Inspeksi;
use App\Exports\PcrExport;
use App\Models\StatusSwab;
use Illuminate\Http\Request;
use App\Exports\PcrExportAll;
use App\Imports\PCRImport;
use App\Models\IdentitasType;
use App\Models\DetailInspeksi;
use App\Models\Qrpatient;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Carbon\Carbon;
use ZipArchive;

class PcrController extends Controller {
    
    public function index() 
    {
        $identitasTypes = IdentitasType::all();
        $inspeksis = Inspeksi::all();
        $statusSwabs = StatusSwab::all();
        $detailInspeksis = DetailInspeksi::all();
        
        if (get_data_login()->mitra_id !='' ){
            if (get_data_login()->branch_id !='' ){
                $branchs = Cabang::where('id',get_data_login()->branch_id)->get();
            }
            else{
                $branchs = Cabang::where('mitra_id',get_data_login()->mitra_id)->get();
            }
        }else{
            $branchs = Cabang::all();
        }
        if (get_data_login()->mitra_id !='' ){
            $mitras = Mitra::where('id',get_data_login()->mitra_id)->get();
        }
        else{
            $mitras = Mitra::all();
        }
        $provinces = DB::table('provinces')->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->orderBy('name', 'ASC')->get();
        $districts = DB::table('districts')->orderBy('name', 'ASC')->get();
        $villages = DB::table('villages')->orderBy('name', 'ASC')->get();
        
        return view('pcr.index', compact('identitasTypes', 'inspeksis', 'mitras', 'statusSwabs', 'detailInspeksis', 'branchs', 'provinces', 'regencies', 'districts', 'villages'));
        
    }

    public function table(Request $request)
    {
        $c = $request->cari ? $request->cari : '';
        $cari = $request->cari ? $request->cari : '';

        if($cari){
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs =  Pcr::where('fullname', 'like', '%'.$cari.'%')
                    ->where('branch_id',get_data_login()->branch_id)
                    ->orWhere('branch_name', 'like', '%'.$cari.'%')
                    ->orWhere('nik', 'like', '%'.$cari.'%')
                    ->orWhere('email', 'like', '%'.$cari.'%')
                    ->get();
                }
                else{
                    $pcrs =  Pcr::where('fullname', 'like', '%'.$cari.'%')
                    ->where('mitra_id',get_data_login()->mitra_id)
                    ->orWhere('branch_name', 'like', '%'.$cari.'%')
                    ->orWhere('nik', 'like', '%'.$cari.'%')
                    ->orWhere('email', 'like', '%'.$cari.'%')
                    ->get();
                }
            }
            else{
                $pcrs =  Pcr::where('fullname', 'like', '%'.$cari.'%')
                ->orWhere('branch_name', 'like', '%'.$cari.'%')
                ->orWhere('nik', 'like', '%'.$cari.'%')
                ->orWhere('email', 'like', '%'.$cari.'%')
                ->get();
            }

            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);
            return response()->json($pcrs);
        }
        if (get_data_login()->mitra_id !='' ){
            if (get_data_login()->branch_id !='' ){
                $pcrs = Pcr::where('branch_id',get_data_login()->branch_id)->get();
            }
            else{
                $pcrs = Pcr::where('mitra_id',get_data_login()->mitra_id)->get();
            }
        }
        else{
            $pcrs = Pcr::all();
        }
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');
        $pcrs->load('hasMitra');

        return response()->json($pcrs);
    }

    public function filter(Request $request)
    {
        $branch = $request->branch_id;
        $mitra = $request->mitra_id;
        $descNarStatus = $request->desc_nar_status;
        $order = $request->order;
        // dd($descNarStatus);
        
        //all feld not null
        if($branch != '' && $mitra !='' && $descNarStatus != '' && $order == ''){
            
            
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('branch_id',get_data_login()->branch_id)
                    ->where('mitra_id', $mitra)
                    ->where('desc_nar_status', $descNarStatus)->get();
                }
                else{
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('mitra_id',get_data_login()->mitra_id)
                    ->where('mitra_id', $mitra)
                    ->where('desc_nar_status', $descNarStatus)->get();
                }
            }
            else{
                $pcrs = Pcr::where('branch_id', $branch)
                ->where('mitra_id', $mitra)
                ->where('desc_nar_status', $descNarStatus)->get();
            }
            
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }


        //all desc nar status
        if($branch != '' && $mitra !='' && $descNarStatus == '' && $order == ''){
                       
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('branch_id',get_data_login()->branch_id)
                    ->where('mitra_id', $mitra)->get();    
                }
                else{
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('mitra_id',get_data_login()->mitra_id)
                    ->where('mitra_id', $mitra)->get();
                }
            }
            else{
                $pcrs = Pcr::where('branch_id', $branch)
                ->where('mitra_id', $mitra)->get();    
            }

            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc mitra
        if($branch == '' && $mitra !='' && $descNarStatus == '' && $order == ''){
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('mitra_id', $mitra)
                    ->where('branch_id',get_data_login()->branch_id)->get();
                }
                else{
                    $pcrs = Pcr::where('mitra_id', $mitra)
                    ->where('mitra_id',get_data_login()->mitra_id)->get();
                }
            }
            else{
                $pcrs = Pcr::where('mitra_id', $mitra)->get();
            }
            
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch
        if($branch == '' & $mitra !='' && $descNarStatus != '' && $order == ''){
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                    ->where('branch_id',get_data_login()->branch_id)->get()
                    ->where('mitra_id', $mitra)->get();
                }
                else{
                    $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                    ->where('mitra_id',get_data_login()->mitra_id)->get()
                    ->where('mitra_id', $mitra)->get();
                }
            }
            else{
                $pcrs = Pcr::where('desc_nar_status', $descNarStatus)->where('mitra_id', $mitra)->get();
            }
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status get last record
        if($branch != '' && $descNarStatus == '' && $order == '1'){
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('branch_id',get_data_login()->branch_id)->get()
                    ->orderBy('created_at', 'desc')->get();
                }
                else{
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('mitra_id',get_data_login()->mitra_id)->get()
                    ->orderBy('created_at', 'desc')->get();
                }
            }
            else{
                $pcrs = Pcr::where('branch_id', $branch)
                ->orderBy('created_at', 'desc')->get();
            }
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch get last record
        if($branch == '' && $mitra == '' && $descNarStatus != '' && $order == '1'){
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                    ->where('branch_id',get_data_login()->branch_id)->get()
                    ->orderBy('created_at', 'desc')->get();
                }
                else{
                    $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                    ->where('mitra_id',get_data_login()->mitra_id)->get()
                    ->orderBy('created_at', 'desc')->get();
                }
            }
            else{
                $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                ->orderBy('created_at', 'desc')->get();
            }
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status get new record
        if($branch != '' && $descNarStatus == '' && $order == '2'){
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('branch_id',get_data_login()->branch_id)->get()
                    ->orderBy('created_at', 'asc')->get();
                }
                else{
                    $pcrs = Pcr::where('branch_id', $branch)
                    ->where('mitra_id',get_data_login()->mitra_id)->get()
                    ->orderBy('created_at', 'asc')->get();
                }
            }
            else{
                $pcrs = Pcr::where('branch_id', $branch)
                ->orderBy('created_at', 'asc')->get();
            }
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch get new record
        if($branch == '' && $descNarStatus != '' && $order == '1'){
            if (get_data_login()->mitra_id !='' ){
                if (get_data_login()->branch_id !='' ){
                    $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                    ->where('branch_id',get_data_login()->branch_id)->get()
                    ->orderBy('created_at', 'asc')->get();
                }
                else{
                    $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                    ->where('mitra_id',get_data_login()->mitra_id)->get()
                    ->orderBy('created_at', 'asc')->get();
                }
            }
            else{
                $pcrs = Pcr::where('desc_nar_status', $descNarStatus)
                ->orderBy('created_at', 'asc')->get();
            }
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //allfield null
        if (get_data_login()->mitra_id !='' ){
            if (get_data_login()->branch_id !='' ){
                $pcrs = Pcr::where('mitra_id', $mitra)->get();
            }
            else{ 
                $pcrs = Pcr::where('mitra_id',get_data_login()->mitra_id)->get();
            }
        }
        else{
            $pcrs = Pcr::all();
        }
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');
        $pcrs->load('hasMitra');

        //get searching value
        $cari = $request->input('cari') ? $request->input('cari') : '';
        if($cari){
            // dd($cari);
            $pcrs = $pcrs->where('fullname', 'like', '%'.$cari.'%')->get();
            dd($pcrs);
            return response()->json($pcrs);
        }

        return response()->json($pcrs);
    }

    public function getBranch()
    {
        $branchs = Branch::all();

        return response()->json($branchs);
    }

    public function printlabelsatu(Request $request){
        $pcr = DB::select("SELECT * FROM patient where id in(". $request->id .")");

        $result = array();
        foreach ($pcr as $key => $value) {
            $date=date_create($value->dob);
            $data = array(
                'qrpatient'=>base64_encode(QrCode::format('svg')->size(50)->generate($value->pasien_no)),
                'id_patient'=>$value->pasien_no,
                'nik'=>$value->nik,
                'fullname'=>$value->fullname,
                'dob'=>date_format($date,'d M Y')
            );
            array_push($result,$data);
            DB::select("UPDATE patient SET jml_print=jml_print+1 where id=". $value->id ."");
        }
        $customPaper = array(0,0,147.00,68.80);
        $pdf = PDF::loadView('pcr.pdfLabel1',compact('result'))->setPaper($customPaper);
        return $pdf->stream();
    }  

    public function printlabeldua(Request $request){
        $pcr = DB::select("SELECT * FROM patient where id in(". $request->id .")");

        $result = array();
        foreach ($pcr as $key => $value) {
            $date=date_create($value->dob);
            $data = array(
                'qrpatient'=>base64_encode(QrCode::format('svg')->size(50)->generate($value->pasien_no)),
                'id_patient'=>$value->pasien_no,
                'nik'=>$value->nik,
                'fullname'=>$value->fullname,
                'dob'=>date_format($date,'d M Y')
            );
            array_push($result,$data);
            DB::select("UPDATE patient SET jml_print=jml_print+1 where id=". $value->id ."");

        }
        $customPaper = array(0,0,294.00,68.80);
        $pdf = PDF::loadView('pcr.pdfLabel2',compact('result'))->setPaper($customPaper);
        
        return $pdf->stream();
    }  

    public function generateCode(){
        if (get_data_login()->mitra_id !='' ){
            if (get_data_login()->branch_id !='' ){
                $pcrs = Pcr::where('branch_id',get_data_login()->branch_id)->last();
            }
            else{
                $pcrs = Pcr::where('mitra_id',get_data_login()->mitra_id)->last();
            }
        }
        else{
            $pcrs = Pcr::all();
        }
        $no_patient = 	get_no_mitra();//$pcrs[0]['pasien_no']+1;
        return json_encode(array("data"=>$no_patient,"success"=>true,"message"=>"Success"));
    }

    public function destroy(Request $request) {
        $pcr = Pcr::whereIn("id",$request->id);
        try{
            $pcr->delete();
            return response()->json(['success'=>true,"message"=>"Success Delete"]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'branch_name' => 'required',
            'identitas_type_id' => 'required',
            'nik' => 'required',
            'nik_type' => 'required',
            'fullname' => 'required',
            'email' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'provinsi_id' => 'required',
            'kab_kota_id' => 'required',
            'kecamatan_id' => 'required',
            'kelurahan_id' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'alamat' => 'required',
            'no_phone' => 'required',
            'perusahaan_name' => 'required',
            'inspeksi_id' => 'required',
            'detail_inspeksi_id' => 'required',
            'status_swab_id' => 'required',
            'swabber_name' => 'required',
            'swabber_date' => 'required',
            'pasien_no' => 'required',
            'external_no' => 'required',
        ]);
        DB::select("ALTER TABLE patient AUTO_INCREMENT=0");
        $pcr = new Pcr();
        $pcr->branch_id = $request->branch_id;
        $pcr->branch_name = $request->branch_name;
        $pcr->status_profile = $request->status_profile;
        $pcr->identitas_type_id = $request->identitas_type_id;
        $pcr->nik = $request->nik;
        $pcr->nik_type = $request->nik_type;
        $pcr->fullname = $request->fullname;
        $pcr->email = $request->email;
        $pcr->dob = $request->dob;
        $pcr->gender = $request->gender;
        $pcr->domisili_status = $request->domisili_status;
        $pcr->provinsi_id = $request->provinsi_id;
        $pcr->kab_kota_id = $request->kab_kota_id;
        $pcr->kecamatan_id = $request->kecamatan_id;
        $pcr->kelurahan_id = $request->kelurahan_id;
        $pcr->rt = $request->rt;
        $pcr->rw = $request->rw;
        $pcr->alamat = $request->alamat;
        $pcr->no_phone = $request->no_phone;
        $pcr->perusahaan_name = $request->perusahaan_name;
        $pcr->inspeksi_id = $request->inspeksi_id;
        $pcr->detail_inspeksi_id = $request->detail_inspeksi_id;
        $pcr->status_swab_id = $request->status_swab_id;
        $pcr->swabber_name = $request->swabber_name;
        $pcr->swabber_date = $request->swabber_date;
        $pcr->pasien_no = $request->pasien_no;
        $pcr->external_no = $request->external_no;
        $pcr->test = "PCR";

        $getbranch = Cabang::where('id',$request->branch_id)->first();
        $getmitra = Mitra::where('id',$getbranch->mitra_id)->first();
        $pcr->mitra_id = $getmitra->id;
        $pcr->price = $getmitra->m_harga_langganan;
        $pcr->run_nolab = get_runno_lab();
        $pcr->lab_patient_no = get_no_lab();
        $pcr->run_nomitra = get_runno_mitra($pcr->mitra_id);
         
        $pcr->save();
        return response()->json(array(
            'success' => true,'message'=>'Record has been added'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'branch_id' => 'required',
            'identitas_type_id' => 'required',
            'nik' => 'required',
            'nik_type' => 'required',
            'fullname' => 'required',
            'email' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'provinsi_id' => 'required',
            'kab_kota_id' => 'required',
            'kecamatan_id' => 'required',
            'kelurahan_id' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'alamat' => 'required',
            'no_phone' => 'required',
            'perusahaan_name' => 'required',
            'inspeksi_id' => 'required',
            'detail_inspeksi_id' => 'required',
            'status_swab_id' => 'required',
            'swabber_name' => 'required',
            'swabber_date' => 'required',
            'pasien_no' => 'required',
            'external_no' => 'required',
        ]);

        $pcr = Pcr::where('id', $request->id)->first();
        $pcr->branch_id = $request->branch_id;
        $pcr->status_profile = $request->status_profile;
        $pcr->identitas_type_id = $request->identitas_type_id;
        $pcr->nik = $request->nik;
        $pcr->nik_type = $request->nik_type;
        $pcr->fullname = $request->fullname;
        $pcr->email = $request->email;
        $pcr->dob = $request->dob;
        $pcr->gender = $request->gender;
        $pcr->domisili_status = $request->domisili_status;
        $pcr->provinsi_id = $request->provinsi_id;
        $pcr->kab_kota_id = $request->kab_kota_id;
        $pcr->kecamatan_id = $request->kecamatan_id;
        $pcr->kelurahan_id = $request->kelurahan_id;
        $pcr->rt = $request->rt;
        $pcr->rw = $request->rw;
        $pcr->alamat = $request->alamat;
        $pcr->no_phone = $request->no_phone;
        $pcr->perusahaan_name = $request->perusahaan_name;
        $pcr->inspeksi_id = $request->inspeksi_id;
        $pcr->detail_inspeksi_id = $request->detail_inspeksi_id;
        $pcr->status_swab_id = $request->status_swab_id;
        $pcr->swabber_name = $request->swabber_name;
        $pcr->swabber_date = $request->swabber_date;
        $pcr->pasien_no = $request->pasien_no;
        $pcr->external_no = $request->external_no;
        $getbranch = Cabang::where('id',$request->branch_id)->first();
        $getmitra = Mitra::where('id',$getbranch->mitra_id)->first();
        $pcr->mitra_id = $getmitra->id;
        $pcr->price = $getmitra->m_harga_langganan;
         
        $pcr->save();
        
        return response()->json(array(
            'success' => true,'message'=>'Record has been Updated'));
    }

    public function excelAll(Request $request)
    {
            $from_date = $request->from_date;
            $to_date = $request->to_date;
            
            return Excel::download(new PcrExportAll($from_date,$to_date), 'PCR Report.xlsx');
    }

    public function pdfAll(Request $request)
    {
        $pcr = DB::select("SELECT * FROM patient where id in(". $request->id .")");

        $result = array();
        foreach ($pcr as $key => $value) {
            $date=date_create($value->dob);
            $swabber_date = date_create($value->swabber_date);
            $result_receive_date= date_create($value->result_receive_date);
            $result_analysis_date= date_create($value->result_analysis_date);
            $result_date= date_create($value->result_date);
            $now = Carbon::now(); // Tanggal sekarang
            $b_day = Carbon::parse($value->dob); // Tanggal Lahir
            $umur = $b_day->diffInYears($now);  // Menghitung umur
            if ($value->result==1){
                $hasil_test= "Positive";
            }
            elseif($value->result==2){
                $hasil_test= "Negative";
            }
            else{
                $hasil_test= "-";
            }
            if ($value->gender=="m"){
                $jenis_kelamin = "Laki-Laki (m)";
            }else{
                $jenis_kelamin = "Perempuan (f)";
            }
            $data = array(
                'logo'=>base64_encode(file_get_contents(get_logolab())),
                'qrpatient'=>base64_encode(QrCode::format('svg')->size(100)->generate(url('lappdf?no_patient='.base64_encode($value->pasien_no).'&lab_patient_no='.base64_encode($value->lab_patient_no) ))),
                'id_patient'=>$value->pasien_no,
                'nik'=>$value->nik,
                'fullname'=>$value->fullname,
                'nama_cabang'=>get_branch_name($value->branch_id),
                'no_registrasi_lab'=>get_no_registrasi_lab(),
                'jenis_kelamin'=>$jenis_kelamin,
                'umur'=>$umur,
                'dob'=>date_format($date,'d M Y'),
                'test_name'=>$value->test,
                'swabber_date'=>date_format($swabber_date,'d M Y'),
                'result_receive_date'=>date_format($result_receive_date,'d M Y'),
                'result_analysis_date'=>date_format($result_analysis_date,'d M Y'),
                'result_date'=>date_format($result_date,'d M Y'),
                'nik_type'=>$value->nik_type,
                'hasil_test'=>$hasil_test,
                'lab_patient_no'=>$value->lab_patient_no,
                'dokter'=>dokter(),
                'no_sip'=>no_sip(),
                'title_footer'=>get_title_footer(),
                'ttd_img'=>base64_encode(file_get_contents(get_ttd_img())),
                'phone'=>get_phone(),
                'alamat'=>get_alamat(),
            );
            array_push($result,$data);
            DB::select("UPDATE patient SET jml_print=jml_print+1 where id=". $value->id ."");

        }
        $pdf = PDF::loadView('pcr.pdfAll',compact('result'));
        return $pdf->stream();

    }
    public function pcrsetqr(Request $request){
        $pcr  = Pcr::whereIn('id',$request->id)->where('qr',0)->get();
        foreach ($pcr as $key => $value) {
            $qrpatient = new Qrpatient;
            $qrpatient->id_patient = $value->pasien_no;
            $qrpatient->branch_id = $value->branch_id;
            $qrpatient->mitra_id = $value->mitra_id;
            $qrpatient->print = 0;
            $qrpatient->print_by = '';
            $qrpatient->use = 0;
            $qrpatient->use_by = '';
            $qrpatient->created_by = get_data_login()->name;
            $qrpatient->save();
            DB::select("UPDATE patient SET qr=". $qrpatient->id ." WHERE id=". $value->id ."");
        }
        
        
        return response()->json(array(
            'success' => true,'message'=>'Set QR Success'));
    }

    public function validdata(Request $request){
        $pcr  = Pcr::whereIn('id',$request->id)->get();
        foreach ($pcr as $key => $value) {
            
        }
        return response()->json(array(
            'success' => true,'message'=>'Check Validation Success'));
    }

    public function send_to_lab(Request $request){
        $params =  json_decode($request->input()['id']);
        try{
            for ($i=0; $i < count($params); $i++) { 
                DB::select("UPDATE patient set r_status = 'dikirim' where r_status='' and id=". $params[$i] ."");
            }
            return response()->json(['success'=>true,"message"=>"Success Send To Lab"]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    public function sendEmail(Request $request)
    {
        $pcrs = Pcr::where('id', $request->id)->first();
        $pcrs->increment('jml_print');
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');
        $pcrs->load('hasMitra');
        $data = [
            "name" => $request->name,
        ];
        $email = $request->email;
        $pdf = PDF::loadView('pcr.pdfAll', [
            'pcrs' => $pcrs,
        ]);
        Mail::send('email.myDemoMail', $data, function ($message)  use($email,$pdf){
			$message->to($email)
					->subject('PCR REPORT')
                    ->attachData($pdf->output(), "PCR Report.pdf");
            $message->from('no-reply@te-pos.com', 'K-Lab');
		});

    	return response()->json(['message' =>'success']);
    }

    public function import_excel(Request $request) 
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
        DB::select("ALTER TABLE patient AUTO_INCREMENT=0");
        // menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
		$file->move('public/file_pcr',$nama_file);

		// import data
		Excel::import(new PCRImport, public_path('/file_pcr/'.$nama_file));
        DB::select("DELETE FROM `patient` WHERE nik is null");
        return back();
    }

    public function downloadzip()
    {
        $zip = new \ZipArchive();
        $tempFileUri = "../zip/report".time() .".zip";
    
        if ($zip->open($tempFileUri, \ZipArchive::CREATE) !== TRUE) {
            echo 'Could not open ZIP file.';
            return;
        }
        $files = File::files(public_path('report'));
        foreach ($files as $key => $value) {
            $relativeNameInZipFile = basename($value);
            $zip->addFile($value, $relativeNameInZipFile);
        }
        $zip->close();

        return response()->download(public_path($tempFileUri));
    }

}