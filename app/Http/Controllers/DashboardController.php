<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Transaction;
use Auth;
use DB;

class DashboardController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $total_patient = DB::select("SELECT count(*) as jumlah from patient");
        $total_process = DB::select("SELECT count(*) as jumlah from patient where r_status='ekstraksi'");
        $total_positive = DB::select("SELECT SUM(CASE WHEN patient.result=1 THEN 1 ELSE 0 END) as jumlah from patient");
        $total_negative = DB::select("SELECT SUM(CASE WHEN patient.result=2 THEN 1 ELSE 0 END) as jumlah from patient");
        return view('dashboard',compact('total_patient','total_negative','total_positive','total_process'));
        
    }

    public function json_grafik() {
        $positive  = $this->chart_positive();
        $negative = $this->chart_negative();
        $positive_string  = '';
        $negative_string = '';
        $months         = '"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"';

        foreach ($positive as $i) {
            $positive_string = $positive_string . $i->jumlah . ",";
        }
        $positive_string = rtrim($positive_string, ",");
        foreach ($negative as $i) {
            $negative_string = $negative_string . $i->jumlah . ",";
        }
        $negative_string = rtrim($negative_string, ",");
        echo '{"Months":[' . $months . '], "positive":[' . $positive_string . '],"negative":[' . $negative_string . ']}';
        exit();
    }


    public function chart_positive(){
        $date       = date("Y-m-d");
        $result      = DB::select("SELECT m.month, SUM(CASE WHEN patient.result=1 THEN 1 ELSE 0 END) as jumlah
		FROM ( SELECT 1 AS MONTH UNION SELECT 2 AS MONTH UNION SELECT 3 AS MONTH UNION SELECT 4 AS MONTH
		UNION SELECT 5 AS MONTH UNION SELECT 6 AS MONTH UNION SELECT 7 AS MONTH UNION SELECT 8 AS MONTH
		UNION SELECT 9 AS MONTH UNION SELECT 10 AS MONTH UNION SELECT 11 AS MONTH UNION SELECT 12 AS MONTH ) AS m
		LEFT JOIN patient ON m.month = MONTH(created_at) AND YEAR(patient.created_at)=YEAR('$date') GROUP BY m.month ORDER BY m.month ASC");
        return $result;
    }

    public function chart_negative(){
        $date       = date("Y-m-d");
        $result      = DB::select("SELECT m.month, SUM(CASE WHEN patient.result=2 THEN 1 ELSE 0 END) as jumlah
		FROM ( SELECT 1 AS MONTH UNION SELECT 2 AS MONTH UNION SELECT 3 AS MONTH UNION SELECT 4 AS MONTH
		UNION SELECT 5 AS MONTH UNION SELECT 6 AS MONTH UNION SELECT 7 AS MONTH UNION SELECT 8 AS MONTH
		UNION SELECT 9 AS MONTH UNION SELECT 10 AS MONTH UNION SELECT 11 AS MONTH UNION SELECT 12 AS MONTH ) AS m
		LEFT JOIN patient ON m.month = MONTH(created_at) AND YEAR(patient.created_at)=YEAR('$date') GROUP BY m.month ORDER BY m.month ASC");
        return $result;
    }

}