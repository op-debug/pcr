<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use File;
use App\Models\Pcr;
use App\Models\Cabang;
use App\Models\Mitra;
use App\Models\Inspeksi;
use App\Exports\PcrExport;
use App\Models\StatusSwab;
use Illuminate\Http\Request;
use App\Exports\PcrExportAll;
use App\Models\IdentitasType;
use App\Models\DetailInspeksi;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class AnalizePCRController extends Controller {
    
    public function index() 
    {
        $identitasTypes = IdentitasType::all();
        $inspeksis = Inspeksi::all();
        $statusSwabs = StatusSwab::all();
        $detailInspeksis = DetailInspeksi::all();
        $branchs = Cabang::all();
        $mitras = Mitra::all();
        $provinces = DB::table('provinces')->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->orderBy('name', 'ASC')->get();
        $districts = DB::table('districts')->orderBy('name', 'ASC')->get();
        $villages = DB::table('villages')->orderBy('name', 'ASC')->get();
        
        return view('analizepcr.index', compact('mitras','identitasTypes', 'inspeksis', 'statusSwabs', 'detailInspeksis', 'branchs', 'provinces', 'regencies', 'districts', 'villages'));
        
    }

    public function table(Request $request)
    {
        $c = $request->cari ? $request->cari : '';
        $cari = $request->cari ? $request->cari : '';
        if($cari){
            $pcrs =  Pcr::where('fullname', 'like', '%'.$cari.'%')
                ->where('r_status','analize')
                ->orWhere('branch_name', 'like', '%'.$cari.'%')
                ->orWhere('nik', 'like', '%'.$cari.'%')
                ->orWhere('email', 'like', '%'.$cari.'%')
                ->get();

            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);
            return response()->json($pcrs);
        }
 
        $pcrs = Pcr::where('r_status','analize')->get();
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');
        $pcrs->load('hasMitra');

        return response()->json($pcrs);
    }

    public function filter(Request $request)
    {
        $branch = $request->branch_id;
        $descNarStatus = $request->desc_nar_status;
        $order = $request->order;
        // dd($descNarStatus);

        //all feld not null
        if($branch != '' && $descNarStatus != '' && $order == ''){
            $pcrs = Pcr::where('branch_id', $branch)->where('r_status','analize')->where('desc_nar_status', $descNarStatus)->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status
        if($branch != '' && $descNarStatus == '' && $order == ''){
            $pcrs = Pcr::where('branch_id', $branch)->where('r_status','analize')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch
        if($branch == '' && $descNarStatus != '' && $order == ''){
            $pcrs = Pcr::where('desc_nar_status', $descNarStatus)->where('r_status','analize')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status get last record
        if($branch != '' && $descNarStatus == '' && $order == '1'){
            $pcrs = Pcr::where('branch_id', $branch)->where('r_status','analize')->orderBy('created_at', 'desc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch get last record
        if($branch == '' && $descNarStatus != '' && $order == '1'){
            $pcrs = Pcr::where('desc_nar_status', $descNarStatus)->where('r_status','analize')->orderBy('created_at', 'desc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status get new record
        if($branch != '' && $descNarStatus == '' && $order == '2'){
            $pcrs = Pcr::where('branch_id', $branch)->where('r_status','analize')->orderBy('created_at', 'asc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch get new record
        if($branch == '' && $descNarStatus != '' && $order == '1'){
            $pcrs = Pcr::where('desc_nar_status', $descNarStatus)->where('r_status','analize')->orderBy('created_at', 'asc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            $pcrs->load('hasMitra');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //allfield null
        $pcrs = Pcr::where('r_status','analize')->get();
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');
        $pcrs->load('hasMitra');

        //get searching value
        $cari = $request->input('cari') ? $request->input('cari') : '';
        if($cari){
            // dd($cari);
            $pcrs = $pcrs->where('fullname', 'like', '%'.$cari.'%')->get();
            dd($pcrs);
            return response()->json($pcrs);
        }

        return response()->json($pcrs);
    }

 
}