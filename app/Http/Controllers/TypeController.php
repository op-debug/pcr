<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Type;

class TypeController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('type');   
    }
    public function getdata(){
        $result = DB::select("select id,t_name from type");
        return response()->json($result);
    }
    public function store(Request $request) {
        DB::select("ALTER TABLE type AUTO_INCREMENT=0");
        if (empty($request->input('t_name'))){
            return response()->json(['errorMsg'=>'Isi Jenis Identitas']);
        }
        try{
            $type = new Type();
            $type->t_name = $request->input('t_name');
            $type->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
        
    }

    public function update(Request $request, $id){
        if (empty($request->input('t_name'))){
            return response()->json(['errorMsg'=>'Isi Nama Type']);
        }
        try{
            $type = Type::find($id);
            $type->t_name = $request->input('t_name');
            $type->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    
    public function delete(Request $request) {
        $type = Type::find($request->input('id'));
        try{
            $type->delete();
            return response()->json(['success'=>true]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

}