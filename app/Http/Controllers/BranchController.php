<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Models\Cabang;
use App\Models\Mitra;
use App\Models\Pcr;

class BranchController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $mitras = Mitra::all();
        return view('branch',compact('mitras'));   
    }
    public function getdata(){
        $result = DB::select("select id,(select m_name from mitra where mitra.id=cabang.mitra_id)as mitra,b_name,b_alamat,b_telp,b_email from cabang");
        return response()->json($result);
    }
    public function store(Request $request) {
        DB::select("ALTER TABLE cabang AUTO_INCREMENT=0");
        if (empty($request->input('b_name'))){
            return response()->json(['errorMsg'=>'Isi Nama Klinik']);
        }
        try{
            $cabang = new Cabang();
            $cabang->b_name = $request->input('b_name');
            $cabang->b_alamat = $request->input('b_alamat');
            $cabang->b_telp = $request->input('b_telp');
            $cabang->b_email = $request->input('b_email');
            $cabang->mitra_id = $request->input('mitra_id');
            $cabang->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
        
    }

    public function update(Request $request, $id){
        if (empty($request->input('b_name'))){
            return response()->json(['errorMsg'=>'Isi Nama Klinik']);
        }
        try{
            $cabang = Cabang::find($id);
            $cabang->b_name = $request->input('b_name');
            $cabang->b_alamat = $request->input('b_alamat');
            $cabang->b_telp = $request->input('b_telp');
            $cabang->b_email = $request->input('b_email');
            $cabang->mitra_id = $request->input('mitra_id');
            $cabang->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    
    public function delete(Request $request) {
        $cabang = Cabang::find($request->input('id'));
        try{
            $chekpatient = Pcr::where("branch_id",$request->input('id'))->get();
            if (count($chekpatient)>0){
                return response()->json(['errorMsg'=>'Data In Use.']);       
            }else{
                $cabang->delete();
                return response()->json(['success'=>true]);
            }
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

}