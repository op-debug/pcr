<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Models\Mitra;
use App\Models\Pcr;
use App\Models\Cabang;

class MitraController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('mitra');   
    }
    public function getdata(){
        $result = DB::select('select id,m_name,m_penanggung_jawab,REPLACE(REPLACE(REPLACE(FORMAT(m_harga_langganan, 2), ".", "@"), ",", "."), "@", ",") as m_harga_langganan from mitra');
        return response()->json($result);
    }
    public function store(Request $request) {
        DB::select("ALTER TABLE mitra AUTO_INCREMENT=0");
        if (empty($request->input('m_name'))){
            return response()->json(['errorMsg'=>'Isi Nama Mitra']);
        }
        try{
            $mitra = new Mitra();
            $mitra->m_name = $request->input('m_name');
            $mitra->m_penanggung_jawab = $request->input('m_penanggung_jawab');
            $mitra->m_harga_langganan = $request->input('m_harga_langganan');
            $mitra->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
        
    }

    public function update(Request $request, $id){
        if (empty($request->input('m_name'))){
            return response()->json(['errorMsg'=>'Isi Nama Mitra']);
        }
        try{
            $mitra = Mitra::find($id);
            $mitra->m_name = $request->input('m_name');
            $mitra->m_penanggung_jawab = $request->input('m_penanggung_jawab');
            $mitra->m_harga_langganan = $request->input('m_harga_langganan');
            $mitra->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    
    public function delete(Request $request) {
        $mitra = Mitra::find($request->input('id'));
        try{
            $chekpatient = Pcr::where("mitra_id",$request->input('id'))->get();
            $chekbanch = Cabang::where("mitra_id",$request->input('id'))->get();
            if (count($chekpatient)>0){
                return response()->json(['errorMsg'=>'Data In Use.']);
            }
            elseif (count($chekbranch)>0){
                return response()->json(['errorMsg'=>'Data In Use.']);       
            }else{
                $mitra->delete();
                return response()->json(['success'=>true]);
            }
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

}