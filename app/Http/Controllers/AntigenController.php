<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Branch;
use App\Models\Antigen;
use App\Models\Inspeksi;
use App\Models\StatusSwab;
use Illuminate\Http\Request;
use App\Models\IdentitasType;
use App\Models\DetailInspeksi;
use Illuminate\Support\Facades\Session;

class AntigenController extends Controller {
   
    public function index() 
    {
        $identitasTypes = IdentitasType::all();
        $inspeksis = Inspeksi::all();
        $statusSwabs = StatusSwab::all();
        $detailInspeksis = DetailInspeksi::all();
        $branchs = Branch::all();
        $provinces = DB::table('provinces')->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->orderBy('name', 'ASC')->get();
        $districts = DB::table('districts')->orderBy('name', 'ASC')->get();
        $villages = DB::table('villages')->orderBy('name', 'ASC')->get();

        return view('antigen.index', compact('identitasTypes', 'inspeksis', 'statusSwabs', 'detailInspeksis', 'branchs', 'provinces', 'regencies', 'districts', 'villages'));
        
    }
    public function table()
    {
        $antigen = Antigen::all();
        $antigen->load('hasInspeksi');
        $antigen->load('hasIdentitasType');
        $antigen->load('hasDetailInspeksi');
        $antigen->load('hasBranch');

        return response()->json($antigen);
    }

    public function generate_code(){
        return json_encode(array("data"=>00213273.00007437,"success"=>true,"message"=>"Success"));
    }
    
    public function filter(Request $request)
    {
        $branch = $request->branch_id;
        $descNarStatus = $request->desc_nar_status;
        $order = $request->order;
        // dd($descNarStatus);

        //all feld not null
        if($branch != '' && $descNarStatus != '' && $order == ''){
            $pcrs = Pcr::where('branch_id', $branch)->where('desc_nar_status', $descNarStatus)->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status
        if($branch != '' && $descNarStatus == '' && $order == ''){
            $pcrs = Pcr::where('branch_id', $branch)->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch
        if($branch == '' && $descNarStatus != '' && $order == ''){
            $pcrs = Pcr::where('desc_nar_status', $descNarStatus)->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status get last record
        if($branch != '' && $descNarStatus == '' && $order == '1'){
            $pcrs = Pcr::where('branch_id', $branch)->orderBy('created_at', 'desc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch get last record
        if($branch == '' && $descNarStatus != '' && $order == '1'){
            $pcrs = Pcr::where('desc_nar_status', $descNarStatus)->orderBy('created_at', 'desc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all desc nar status get new record
        if($branch != '' && $descNarStatus == '' && $order == '2'){
            $pcrs = Pcr::where('branch_id', $branch)->orderBy('created_at', 'asc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //all branch get new record
        if($branch == '' && $descNarStatus != '' && $order == '1'){
            $pcrs = Pcr::where('desc_nar_status', $descNarStatus)->orderBy('created_at', 'asc')->get();
            $pcrs->load('hasInspeksi');
            $pcrs->load('hasIdentitasType');
            $pcrs->load('hasDetailInspeksi');
            $pcrs->load('hasBranch');
            // dd($pcrs);

            return response()->json($pcrs);
        }

        //allfield null
        $pcrs = Pcr::all();
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');

        //get searching value
        $cari = $request->input('cari') ? $request->input('cari') : '';
        if($cari){
            // dd($cari);
            $pcrs = $pcrs->where('fullname', 'like', '%'.$cari.'%')->get();
            dd($pcrs);
            return response()->json($pcrs);
        }

        return response()->json($pcrs);
    }

    public function getBranch()
    {
        $branchs = Branch::all();

        return response()->json($branchs);
    }

    public function generateCode(){
        return json_encode(array("data"=>00213273.00007437,"success"=>true,"message"=>"Success"));
    }

    public function destroy(Request $request) {
        // $params = json_decode($request->input()['id']);
        $antigen = Antigen::whereIn("id",$request->id);
        try{
            $antigen->delete();
            return response()->json(['success'=>true,"message"=>"Success Delete"]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'branch_name' => 'required',
            'identitas_type_id' => 'required',
            'nik' => 'required',
            'passport' => 'required',
            'nik_type' => 'required',
            'fullname' => 'required',
            'email' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'provinsi_id' => 'required',
            'kab_kota_id' => 'required',
            'kecamatan_id' => 'required',
            'kelurahan_id' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'alamat' => 'required',
            'no_phone' => 'required',
            'registrasi_type' => 'required',
            'perusahaan_name' => 'required',
            'inspeksi_id' => 'required',
            'detail_inspeksi_id' => 'required',
            'destination_country' => 'required',
            'status_swab_id' => 'required',
            'swabber_name' => 'required',
            'swabber_date' => 'required',
            'pasien_no' => 'required',
            'external_no' => 'required',
            'payment_service' => 'required',
            'payment_ref' => 'required',
            'payment_card_no' => 'required',
            'payment_name' => 'required',
            'payment_date' => 'required',
            'product_price' => 'required',
            'coupon' => 'required',
            'price' => 'required',
        ]);

        $antigen = new Antigen();
        $antigen->branch_id = $request->branch_id;
        $antigen->branch_name = $request->branch_name;
        $antigen->status_profile = $request->status_profile;
        $antigen->identitas_type_id = $request->identitas_type_id;
        $antigen->nik = $request->nik;
        $antigen->passport = $request->passport;
        $antigen->nik_type = $request->nik_type;
        $antigen->fullname = $request->fullname;
        $antigen->email = $request->email;
        $antigen->dob = $request->dob;
        $antigen->gender = $request->gender;
        $antigen->domisili_status = $request->domisili_status;
        $antigen->provinsi_id = $request->provinsi_id;
        $antigen->kab_kota_id = $request->kab_kota_id;
        $antigen->kecamatan_id = $request->kecamatan_id;
        $antigen->kelurahan_id = $request->kelurahan_id;
        $antigen->rt = $request->rt;
        $antigen->rw = $request->rw;
        $antigen->alamat = $request->alamat;
        $antigen->no_phone = $request->no_phone;
        $antigen->registrasi_type = $request->registrasi_type;
        $antigen->perusahaan_name = $request->perusahaan_name;
        $antigen->inspeksi_id = $request->inspeksi_id;
        $antigen->detail_inspeksi_id = $request->detail_inspeksi_id;
        $antigen->status_swab_id = $request->status_swab_id;
        $antigen->destination_country = $request->destination_country;
        $antigen->swabber_name = $request->swabber_name;
        $antigen->swabber_date = $request->swabber_date;
        $antigen->pasien_no = $request->pasien_no;
        $antigen->external_no = $request->external_no;
        $antigen->payment_service = $request->payment_service;
        $antigen->payment_ref = $request->payment_ref;
        $antigen->payment_card_no = $request->paymen_card_no;
        $antigen->payment_name = $request->paymen_name;
        $antigen->payment_date = $request->paymen_date;
        $antigen->product_price = $request->product_price;
        $antigen->coupon = $request->coupon;
        $antigen->price = $request->price;
        $antigen->save();

        Session::flash('sukses','Record has been added');
        return redirect()->back();
      
    }

    public function update(Request $request)
    {
        
        $this->validate($request, [
            'branch_name' => 'required',
            'identitas_type_id' => 'required',
            'nik' => 'required',
            'passport' => 'required',
            'nik_type' => 'required',
            'fullname' => 'required',
            'email' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'provinsi_id' => 'required',
            'kab_kota_id' => 'required',
            'kecamatan_id' => 'required',
            'kelurahan_id' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'alamat' => 'required',
            'no_phone' => 'required',
            'registrasi_type' => 'required',
            'perusahaan_name' => 'required',
            'inspeksi_id' => 'required',
            'detail_inspeksi_id' => 'required',
            'destination_country' => 'required',
            'status_swab_id' => 'required',
            'swabber_name' => 'required',
            'swabber_date' => 'required',
            'pasien_no' => 'required',
            'external_no' => 'required',
            'payment_service' => 'required',
            'payment_ref' => 'required',
            'payment_card_no' => 'required',
            'payment_name' => 'required',
            'payment_date' => 'required',
            'product_price' => 'required',
            'coupon' => 'required',
            'price' => 'required',
        ]);

        $antigen = Antigen::where('id', $request->id)->first();
        $antigen->branch_id = $request->branch_id;
        $antigen->branch_name = $request->branch_name;
        $antigen->status_profile = $request->status_profile;
        $antigen->identitas_type_id = $request->identitas_type_id;
        $antigen->nik = $request->nik;
        $antigen->passport = $request->passport;
        $antigen->nik_type = $request->nik_type;
        $antigen->fullname = $request->fullname;
        $antigen->email = $request->email;
        $antigen->dob = $request->dob;
        $antigen->gender = $request->gender;
        $antigen->domisili_status = $request->domisili_status;
        $antigen->provinsi_id = $request->provinsi_id;
        $antigen->kab_kota_id = $request->kab_kota_id;
        $antigen->kecamatan_id = $request->kecamatan_id;
        $antigen->kelurahan_id = $request->kelurahan_id;
        $antigen->rt = $request->rt;
        $antigen->rw = $request->rw;
        $antigen->alamat = $request->alamat;
        $antigen->no_phone = $request->no_phone;
        $antigen->registrasi_type = $request->registrasi_type;
        $antigen->perusahaan_name = $request->perusahaan_name;
        $antigen->inspeksi_id = $request->inspeksi_id;
        $antigen->detail_inspeksi_id = $request->detail_inspeksi_id;
        $antigen->status_swab_id = $request->status_swab_id;
        $antigen->destination_country = $request->destination_country;
        $antigen->swabber_name = $request->swabber_name;
        $antigen->swabber_date = $request->swabber_date;
        $antigen->pasien_no = $request->pasien_no;
        $antigen->external_no = $request->external_no;
        $antigen->payment_service = $request->payment_service;
        $antigen->payment_ref = $request->payment_ref;
        $antigen->payment_card_no = $request->paymen_card_no;
        $antigen->payment_name = $request->paymen_name;
        $antigen->payment_date = $request->paymen_date;
        $antigen->product_price = $request->product_price;
        $antigen->coupon = $request->coupon;
        $antigen->price = $request->price;
        $antigen->save();

        Session::flash('sukses','Record has been updated');
        return redirect()->back();
    }

    public function excelAll(Request $request)
    {
            $from_date = $request->from_date;
            $to_date = $request->to_date;
            
            return Excel::download(new PcrExportAll($from_date,$to_date), 'PCR Report.xlsx');
    }

    public function pdfAll(Request $request)
    {
        $pcrs = Pcr::where('id', $request->id)->first();
        $pcrs->increment('jml_print');
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');

       

        $pdf = PDF::loadView('pcr.pdfAll', [
            'pcrs' => $pcrs,
        ]);
        return $pdf->stream('PCR_Report.pdf');

    }

    public function sendEmail(Request $request)
    {
        $pcrs = Pcr::where('id', $request->id)->first();
        $pcrs->increment('jml_print');
        $pcrs->load('hasInspeksi');
        $pcrs->load('hasIdentitasType');
        $pcrs->load('hasDetailInspeksi');
        $pcrs->load('hasBranch');

        $data = [
            "name" => $request->name,
        ];
        $email = $request->email;
        $pdf = PDF::loadView('pcr.pdfAll', [
            'pcrs' => $pcrs,
        ]);
        Mail::send('email.myDemoMail', $data, function ($message)  use($email,$pdf){
			$message->to($email)
					->subject('PCR REPORT')
                    ->attachData($pdf->output(), "PCR Report.pdf");
            $message->from('no-reply@te-pos.com', 'K-Lab');
		});

    	return response()->json(['message' =>'success']);
    }
}