<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Models\Ekstraksi;
use App\Models\Interpretasi;
use App\Imports\EkstraksiImport;
use Maatwebsite\Excel\Facades\Excel;

class EkstraksiController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('ekstraksi');   
    }
    public function getdata(){
        $result = DB::select("select * from ekstraksi where interprektasi=0");
        return response()->json($result);
    }

    public function import_excel(Request $request) 
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
        DB::select("ALTER TABLE ekstraksi AUTO_INCREMENT=0");
        // menangkap file excel
		$file = $request->file('file');
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
        // upload ke folder file_siswa di dalam folder public
		$file->move('public/file_ekstraksi',$nama_file);
		// import data
		Excel::import(new EkstraksiImport, public_path('/file_ekstraksi/'.$nama_file));
        DB::select("DELETE FROM `ekstraksi` WHERE hole is null");
        return back();
    }

    public function update(Request $request, $id){
        try{
            $ekstrasi = Ekstraksi::find($id);
            $ekstrasi->ct = $request->input('ct');
            
            $ekstrasi->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    
    public function destroy(Request $request) {
        $params =  json_decode($request->input()['id']);
        $ekstrasi = Ekstraksi::whereIn("id",$params);
        try{
            $ekstrasi->delete();
            return response()->json(['success'=>true,"message"=>"Success Delete"]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    public function validasi(){
        $posvalid = 'false';
        $ntcvalid = 'false';
        try{
            $result = DB::select('
            select type,id_prosess_excel,
            (SELECT SUM(a.ct) FROM `ekstraksi` a WHERE a.type=ekstraksi.type and a.channel="FAM" and a.id_prosess_excel=ekstraksi.id_prosess_excel) as FAM,
            (SELECT SUM(a.ct) FROM `ekstraksi` a WHERE a.type=ekstraksi.type and a.channel="HEX" and a.id_prosess_excel=ekstraksi.id_prosess_excel) as HEX,
            (SELECT SUM(a.ct) FROM `ekstraksi` a WHERE a.type=ekstraksi.type and a.channel="ROX" and a.id_prosess_excel=ekstraksi.id_prosess_excel) as ROX
            from ekstraksi where type IN ("POS","NTC") AND (posvalid="false" or posvalid="") and (ntcvalid="false" or ntcvalid="") group by type,id_prosess_excel
            ');
                foreach ($result as $key => $value) {
                    if($value->type=="POS"){
                        if(($value->FAM>1 && $value->FAM<35) && ($value->HEX>1 && $value->HEX<35) && ($value->ROX>1 && $value->ROX<35)){
                            $posvalid = 'true';
                        }
                        else{
                            $posvalid = 'false';
                        }
                        DB::select("UPDATE ekstraksi set posvalid='". $posvalid ."' where id_prosess_excel=". $value->id_prosess_excel ."");
                    }
                    elseif($value->type=="NTC"){
                        if(($value->FAM==0) && ($value->HEX==0) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM==0) && ($value->HEX==0) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX==0) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX==0) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM==0) && ($value->HEX>40) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM==0) && ($value->HEX>40) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX>40) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX>40) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        else{
                            $ntcvalid = 'false';
                        }

                        DB::select("UPDATE ekstraksi set ntcvalid='". $ntcvalid ."' where id_prosess_excel=". $value->id_prosess_excel ."");
                    }
                }
            return response()->json(['success'=>true,"message"=>"Success Valid"]);
        }
        catch(e $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

    public function interpretasi(){
        try{
            $eks =DB::select("SELECT hole,id_prosess_excel,posvalid,ntcvalid,
            (select sum(a.ct) from ekstraksi a where a.id_prosess_excel = ekstraksi.id_prosess_excel and a.hole=ekstraksi.hole and a.channel='FAM') as fam,
            (select sum(a.ct) from ekstraksi a where a.id_prosess_excel = ekstraksi.id_prosess_excel and a.hole=ekstraksi.hole and a.channel='HEX') as hex,
            (select sum(a.ct) from ekstraksi a where a.id_prosess_excel = ekstraksi.id_prosess_excel and a.hole=ekstraksi.hole and a.channel='ROX') as rox
            FROM `ekstraksi` where interprektasi=0  group by hole,id_prosess_excel,posvalid,ntcvalid");
            foreach ($eks as $key => $value) {
                $interpretasi = new Interpretasi();
                $interpretasi->hole = $value->hole;
                $interpretasi->id_ektraksi_excel = $value->id_prosess_excel;
                $interpretasi->id_sample = "";
                $interpretasi->pos_status = $value->posvalid;
                $interpretasi->ntc_status = $value->ntcvalid;
                $interpretasi->fam = $value->fam;
                $interpretasi->hex = $value->hex;
                $interpretasi->rox = $value->rox;
                if ($value->posvalid=="true" && $value->ntcvalid=="true"){
                    if(($value->fam>1 && $value->fam<40) && ($value->hex>1 && $value->hex<40) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Positive";
                    }elseif(($value->fam==0) && ($value->hex==0) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Negative";
                    }elseif(($value->fam==0) && ($value->hex>40) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Negative";
                    }elseif(($value->fam>40) && ($value->hex==0) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Negative";
                    }elseif(($value->fam>40) && ($value->hex>0) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Negative";
                    }elseif(($value->fam==0) && ($value->hex>1 && $value->hex<40) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Inconclusive";
                    }elseif(($value->fam>1 && $value->fam<40 ) && ($value->hex==0) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Inconclusive";
                    }elseif(($value->fam>40) && ($value->hex>1 && $value->hex>40) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Inconclusive";
                    }elseif(($value->fam>1 && $value->fam<40) && ($value->hex>40) && ($value->rox>1 && $value->rox<35)){
                        $interpretasi->sample_status = "Inconclusive";
                    }else{
                        $interpretasi->sample_status = "Invalid";
                    }
                    

                }
                $interpretasi->save();
                DB::select("UPDATE ekstraksi set interprektasi=1 WHERE id_prosess_excel=". $value->id_prosess_excel ."");
            }
            return response()->json(['success'=>true,"message"=>"Success Interpretasi"]);
        }
        catch(e $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
}