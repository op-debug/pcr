<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Models\Interpretasi;
use App\Models\PCR;
use Maatwebsite\Excel\Facades\Excel;

class InterpretasiController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('interpretasi');   
    }
    public function getdata(){
        $result = DB::select("select * from interpretasi order by id_sample");
        return response()->json($result);
    }

    public function update(Request $request, $id){
        try{
            $inter = Interpretasi::find($id);
            $cekid = DB::select("SELECT * FROM patient where r_status='analize' and lab_patient_no='". $request->id_sample ."' and result=0");
            if (empty($cekid)){
                return response()->json(['errorMsg'=>'Lab. Patient No. Not Found Or Status Not Analize PCR']);
            }
            $result = 0;
            if ($inter->sample_status=='Negative'){
                $result = 2;
                DB::select("UPDATE patient set r_status='finish',result=". $result .",result_date=now() where lab_patient_no='". $request->id_sample ."'");
                $inter->id_sample = $request->input('id_sample');
                $inter->save();
            }elseif($inter->sample_status=='Positive'){
                $result = 1;
                DB::select("UPDATE patient set r_status='finish',result=". $result .",result_date=now() where lab_patient_no='". $request->id_sample ."'");
                $inter->id_sample = $request->input('id_sample');
                $inter->save();
            }elseif($inter->sample_status=='Invalid'){
                DB::select("UPDATE patient set r_status='diterima',result=". $result ." where lab_patient_no='". $request->id_sample ."'");
                $inter->id_sample = $request->input('id_sample');
                $inter->save();
            }
            elseif($inter->sample_status=='Inconclusive'){
                DB::select("UPDATE patient set r_status='ekstraksi',result=". $result ." where lab_patient_no='". $request->id_sample ."'");
                $inter->id_sample = $request->input('id_sample');
                $inter->save();
            }

        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    
    public function destroy(Request $request) {
        $params =  json_decode($request->input()['id']);
        $ekstrasi = Ekstraksi::whereIn("id",$params);
        try{
            $ekstrasi->delete();
            return response()->json(['success'=>true,"message"=>"Success Delete"]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
    public function validasi(){
        $posvalid = 'false';
        $ntcvalid = 'false';
        try{
            $result = DB::select('
            select type,id_prosess_excel,
            (SELECT SUM(a.ct) FROM `ekstraksi` a WHERE a.type=ekstraksi.type and a.channel="FAM" and a.id_prosess_excel=ekstraksi.id_prosess_excel) as FAM,
            (SELECT SUM(a.ct) FROM `ekstraksi` a WHERE a.type=ekstraksi.type and a.channel="HEX" and a.id_prosess_excel=ekstraksi.id_prosess_excel) as HEX,
            (SELECT SUM(a.ct) FROM `ekstraksi` a WHERE a.type=ekstraksi.type and a.channel="ROX" and a.id_prosess_excel=ekstraksi.id_prosess_excel) as ROX
            from ekstraksi where type IN ("POS","NTC") AND (posvalid="false" or posvalid="") and (ntcvalid="false" or ntcvalid="") group by type,id_prosess_excel
            ');
                foreach ($result as $key => $value) {
                    if($value->type=="POS"){
                        if(($value->FAM>1 && $value->FAM<35) && ($value->HEX>1 && $value->HEX<35) && ($value->ROX>1 && $value->ROX<35)){
                            $posvalid = 'true';
                        }
                        else{
                            $posvalid = 'false';
                        }
                        DB::select("UPDATE ekstraksi set posvalid='". $posvalid ."' where id_prosess_excel=". $value->id_prosess_excel ."");
                    }
                    elseif($value->type=="NTC"){
                        if(($value->FAM==0) && ($value->HEX==0) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM==0) && ($value->HEX==0) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX==0) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX==0) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM==0) && ($value->HEX>40) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM==0) && ($value->HEX>40) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX>40) && ($value->ROX==0)){
                            $ntcvalid = 'true';
                        }
                        elseif(($value->FAM>40) && ($value->HEX>40) && ($value->ROX>35)){
                            $ntcvalid = 'true';
                        }
                        else{
                            $ntcvalid = 'false';
                        }

                        DB::select("UPDATE ekstraksi set ntcvalid='". $ntcvalid ."' where id_prosess_excel=". $value->id_prosess_excel ."");
                    }
                }
            return response()->json(['success'=>true,"message"=>"Success Valid"]);
        }
        catch(e $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

}