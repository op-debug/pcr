<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use Auth; 

class LoginController extends Controller
{
    public function login(){
        if (Auth::check()) {
            return redirect('dashboard');
        }else{
            return view('auth.login');
        }
    }

    public function actionlogin(Request $request)
    {
        $data = [
            'username' => $request->username,
            'password' => $request->password,
        ];
        if (Auth::Attempt($data)) {
            return redirect('dashboard');
        }else{
            Session::flash('error', 'Username atau Password Salah');
            return redirect('/');
        }
    }
    
    public function actionlogout()
    {
        Auth::logout();
        return redirect('/');
    }
}
