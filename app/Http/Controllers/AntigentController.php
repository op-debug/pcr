<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Branch;
use App\Models\Antigent;
use App\Models\Inspeksi;
use App\Models\StatusSwab;
use Illuminate\Http\Request;
use App\Models\IdentitasType;
use App\Models\DetailInspeksi;

class AntigentController extends Controller {
   
    public function index() 
    {
        $identitasTypes = IdentitasType::all();
        $inspeksis = Inspeksi::all();
        $statusSwabs = StatusSwab::all();
        $detailInspeksis = DetailInspeksi::all();
        $branchs = Branch::all();
        $provinces = DB::table('provinces')->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->orderBy('name', 'ASC')->get();
        $districts = DB::table('districts')->orderBy('name', 'ASC')->get();
        $villages = DB::table('villages')->orderBy('name', 'ASC')->get();

        return view('antigent.index', compact('identitasTypes', 'inspeksis', 'statusSwabs', 'detailInspeksis', 'branchs', 'provinces', 'regencies', 'districts', 'villages'));
        
    }
    public function table()
    {
        $antigent = Antigent::all();

        return response()->json($antigent);
    }

    public function add(){
        return view('modal.addpcr');
    }
    public function generate_code(){
        return json_encode(array("data"=>00213273.00007437,"success"=>true,"message"=>"Success"));
    }
    public function delete(Request $request) {
        $params = json_decode($request->input()['f_patient_id']);
        $pcr = Pcr::whereIn("f_patient_id",$params);
        try{
            $pcr->delete();
            return response()->json(['success'=>true,"message"=>"Success Delete"]);
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }

    public function store(Request $request){
        if (empty($request->input('f_patient_id'))){
            return response()->json(['errorMsg'=>'Nomor Pasien Kosong']);
        }
        try{
            $pcr = new Pcr();
            $pcr->f_patient_id = $request->input('f_patient_id');
            $pcr->f_nar_mod = $request->input('f_nar_mod');
            $pcr->f_nar_status_mod = $request->input('f_nar_status_mod');
            $pcr->f_result_name = $request->input('f_result_name');
            $pcr->f_print_label_mod = $request->input('f_print_label_mod');
            $pcr->f_handover_date_ddmmmyyyy = $request->input('f_handover_date_ddmmmyyyy');
            $pcr->f_id_type_mod = $request->input('f_id_type_mod');
            $pcr->f_nik = $request->input('f_nik');
            $pcr->f_identitas_2 = $request->input('f_identitas_2');
            $pcr->f_name = $request->input('f_name');
            $pcr->f_dob_ddmmmyyyy = $request->input('f_dob_ddmmmyyyy');
            $pcr->f_gender = $request->input('f_gender');
            $pcr->f_patient_no = $request->input('f_patient_no');
            $pcr->f_klab_patient_no = $request->input('f_klab_patient_no');
            $pcr->f_phone_no = $request->input('f_phone_no');
            $pcr->f_email = $request->input('f_email');
            $pcr->f_corporate_name = $request->input('f_corporate_name');
            $pcr->f_pemeriksaan_tujuan_name = $request->input('f_pemeriksaan_tujuan_name');
            $pcr->f_detail_tujuan_name = $request->input('f_detail_tujuan_name');
            $pcr->f_ct_1 = $request->input('f_ct_1');
            $pcr->f_ct_3 = $request->input('f_ct_3');
            $pcr->f_swab_dt = $request->input('f_swab_dt');
            $pcr->f_swabber_name = $request->input('f_swabber_name');
            $pcr->f_receive_mod = $request->input('f_receive_mod');
            $pcr->f_examination_dt = $request->input('f_examination_dt');
            $pcr->f_result_dt = $request->input('f_result_dt');
            $pcr->f_status_name = $request->input('f_status_name');
            $pcr->f_branch_owner_name = $request->input('f_branch_owner_name');
            $pcr->f_branch_name = $request->input('f_branch_name');
            $pcr->f_nar_status_desc = $request->input('f_nar_status_desc');
            $pcr->f_print = $request->input('f_print');
            $pcr->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        } 
    }

    public function update(Request $request,$id){
        if (empty($request->input('f_patient_id'))){
            return response()->json(['errorMsg'=>'Nomor Pasien Kosong']);
        }
        try{
            $pcr = Pcr::find($id);
            $pcr->f_patient_id = $request->input('f_patient_id');
            $pcr->f_nar_mod = $request->input('f_nar_mod');
            $pcr->f_nar_status_mod = $request->input('f_nar_status_mod');
            $pcr->f_result_name = $request->input('f_result_name');
            $pcr->f_print_label_mod = $request->input('f_print_label_mod');
            $pcr->f_handover_date_ddmmmyyyy = $request->input('f_handover_date_ddmmmyyyy');
            $pcr->f_id_type_mod = $request->input('f_id_type_mod');
            $pcr->f_nik = $request->input('f_nik');
            $pcr->f_identitas_2 = $request->input('f_identitas_2');
            $pcr->f_name = $request->input('f_name');
            $pcr->f_dob_ddmmmyyyy = $request->input('f_dob_ddmmmyyyy');
            $pcr->f_gender = $request->input('f_gender');
            $pcr->f_patient_no = $request->input('f_patient_no');
            $pcr->f_klab_patient_no = $request->input('f_klab_patient_no');
            $pcr->f_phone_no = $request->input('f_phone_no');
            $pcr->f_email = $request->input('f_email');
            $pcr->f_corporate_name = $request->input('f_corporate_name');
            $pcr->f_pemeriksaan_tujuan_name = $request->input('f_pemeriksaan_tujuan_name');
            $pcr->f_detail_tujuan_name = $request->input('f_detail_tujuan_name');
            $pcr->f_ct_1 = $request->input('f_ct_1');
            $pcr->f_ct_3 = $request->input('f_ct_3');
            $pcr->f_swab_dt = $request->input('f_swab_dt');
            $pcr->f_swabber_name = $request->input('f_swabber_name');
            $pcr->f_receive_mod = $request->input('f_receive_mod');
            $pcr->f_examination_dt = $request->input('f_examination_dt');
            $pcr->f_result_dt = $request->input('f_result_dt');
            $pcr->f_status_name = $request->input('f_status_name');
            $pcr->f_branch_owner_name = $request->input('f_branch_owner_name');
            $pcr->f_branch_name = $request->input('f_branch_name');
            $pcr->f_nar_status_desc = $request->input('f_nar_status_desc');
            $pcr->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        } 
    }
    public function updateprint(Request $request,$id){
        try{
            $pcr = Pcr::find($id);
            $pcr->f_print = $request->input('f_print');
            $pcr->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }       
    }

    public function updatelabpatientno(Request $request,$id){
        try{
            $pcr = Pcr::find($id);
            $pcr->f_klab_patient_no = $request->input('f_klab_patient_no');
            $pcr->save();
        }
        catch(Exception $e){
            return response()->json(['errorMsg'=>'Some errors occured.']);
        }
    }
}