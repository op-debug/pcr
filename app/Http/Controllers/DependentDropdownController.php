<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class DependentDropdownController extends Controller {
    
    public function regencies(Request $request) 
    {
        
        $regencies = DB::table('regencies')->where('province_id', $request->id_provinces)->orderBy('name', 'ASC')->get();
        
        foreach ($regencies as $regency) {
            echo '<option value="' . $regency->id . '">' . $regency->name . '</option>';
        }
    }

    public function districts(Request $request) 
    {
        
        $districts = DB::table('districts')->where('regency_id', $request->id_regencies)->orderBy('name', 'ASC')->get();
        
        foreach ($districts as $district) {
            echo '<option value="' . $district->id . '">' . $district->name . '</option>';
        } 
    }

    public function villages(Request $request) 
    {
        
        $villages = DB::table('villages')->where('district_id', $request->id_district)->orderBy('name', 'ASC')->get();
        
        foreach ($villages as $village) {
            echo '<option value="' . $village->id . '">' . $village->name . '</option>';
        }
    }

    public function branch(Request $request) 
    {
        
        $branchs = DB::table('cabang')->where('mitra_id', $request->mitra_id)->orderBy('b_name', 'ASC')->get();
        echo '<option value="">All Branch</option>';
        foreach ($branchs as $branch) {
            echo '<option value="' . $branch->id . '">' . $branch->b_name . '</option>';
        }
    }

    public function getpatient(Request $request) 
    {
        
        $pcrs = DB::table('patient')->where('r_status', 'analize')->orderBy('lab_patient_no', 'ASC')->get();
        echo '<option value="">All No Lab.</option>';
        foreach ($pcrs as $pcr) {
            echo '<option value="' . $pcr->lab_patient_no . '">' . $pcr->lab_patient_no . '</option>';
        }
    }
    public function allbranch(Request $request) 
    {
        if (get_data_login()->branch_id !='' ){
            $branchs = DB::table('cabang')->where('id',get_data_login()->branch_id)->orderBy('b_name', 'ASC')->get();
            echo '<option value="">All Branch</option>';
            foreach ($branchs as $branch) {
                echo '<option value="' . $branch->id . '">' . $branch->b_name . '</option>';
            }
        }
        else{
            $branchs = DB::table('cabang')->orderBy('b_name', 'ASC')->get();
            echo '<option value="">All Branch</option>';
            foreach ($branchs as $branch) {
                echo '<option value="' . $branch->id . '">' . $branch->b_name . '</option>';
            }
        }
    }
}