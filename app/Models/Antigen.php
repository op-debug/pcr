<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Antigen extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'patient';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'branch_id',
        'branch_name',
        'status_profile',
        'identitas_type_id',
        'nik',
        'nik_type',
        'passport',
        'fullname',
        'email',
        'dob',
        'gender',
        'domisili_status',
        'provinsi_id',
        'kab_kota_id',
        'kecamatan_id',
        'kelurahan_id',
        'rt',
        'rw',
        'alamat',
        'no_phone',
        'registrasi_type',
        'perusahaan_name',
        'inspeksi_id',
        'detail_inspeksi_id',
        'destination_country',
        'status_swab_id',
        'swabber_name',
        'swabber_date',
        'pasien_no',
        'external_no',
        'payment_service',
        'payment_ref',
        'payment_card_no',
        'payment_name',
        'trans_stat',
        'payment_date',
        'product_price',
        'coupon',
        'price',
        'linkqr',
    ];

    public function hasInspeksi()
    {
        return $this->hasOne('App\Models\Inspeksi', 'id', 'inspeksi_id');
    }

    public function hasIdentitasType()
    {
        return $this->hasOne('App\Models\IdentitasType', 'id', 'identitas_type_id');
    }

    public function hasDetailInspeksi()
    {
        return $this->hasOne('App\Models\DetailInspeksi', 'id', 'detail_inspeksi_id');
    }

    public function hasBranch()
    {
        return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
    }

    public function hasProvinces()
    {
        return $this->hasOne('App\Models\Provinces', 'id', 'provinsi_id');
    }

    public function hasRegencies()
    {
        return $this->hasOne('App\Models\Regencies', 'id', 'kab_kota_id');
    }

    public function hasDistricts()
    {
        return $this->hasOne('App\Models\Districts', 'id', 'kecamatan_id');
    }

    public function hasVillages()
    {
        return $this->hasOne('App\Models\Villages', 'id', 'kelurahan_id');
    }

    public function hasKwitansi()
    {
        return $this->hasOne('App\Models\Kwitansi', 'id', 'partient_id');
    }
}