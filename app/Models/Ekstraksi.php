<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ekstraksi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ekstraksi';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'hole',
        'channel',
        'ct',
        'type',
        'purpose',
        'group_dt',
        'sample',
        'id_sampel_real',
        'target',
        'gene',
        'id_prosess_excel'
    ];
}