<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nationaly extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nationaly';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'id',
        'name',
    ];
}