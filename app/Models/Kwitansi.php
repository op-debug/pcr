<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kwitansi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kwitansi';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'patient_id',
        'no_recipt',
        'description',
    ];

    public function hasPcr()
    {
        return $this->belongsTo('App\Models\Pcr', 'patient_id', 'id');
    }

    public function hasAntigen()
    {
        return $this->belongsTo('App\Models\Antigen', 'patient_id', 'id');
    }

    public function hasQrpatient()
    {
        return $this->belongsTo('App\Models\Qrpatient', 'patient_id', 'id');
    }
}