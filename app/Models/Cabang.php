<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cabang';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'name',
    ];
}