<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IdentitasType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'identitas_type';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'name',
    ];
}