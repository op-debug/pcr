<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusSwab extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'status_swab';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'name',
    ];
}