<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inspeksi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inspeksi';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'name',
    ];
}