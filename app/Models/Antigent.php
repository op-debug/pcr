<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Antigent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'antigent';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'branch_id',
        'status_profile',
        'identitas_type_id',
        'nik',
        'nik_type',
        'fullname',
        'email',
        'dob',
        'gender',
        'domisili_status',
        'provinsi_id',
        'kab_kota_id',
        'kecamatan_id',
        'kelurahan_id',
        'rt',
        'rw',
        'alamat',
        'no_phone',
        'perusahaan_name',
        'inspeksi_id',
        'detail_inspeksi_id',
        'status_swab_id',
        'swabber_name',
        'pasien_no',
        'external_no',
    ];

    public function hasInspeksi()
    {
        return $this->hasOne('App\Models\Inspeksi', 'id', 'inspeksi_id');
    }

    public function hasProvinces()
    {
        return $this->hasOne('App\Models\Provinces', 'id', 'provinsi_id');
    }

    public function hasRegencies()
    {
        return $this->hasOne('App\Models\Regencies', 'id', 'kab_kota_id');
    }

    public function hasDistricts()
    {
        return $this->hasOne('App\Models\Districts', 'id', 'kecamatan_id');
    }

    public function hasVillages()
    {
        return $this->hasOne('App\Models\Villages', 'id', 'kelurahan_id');
    }
}