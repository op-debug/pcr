<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailInspeksi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'detail_inspeksi';
    protected $guard = ['id'];
    public $timesstamps = true;

    protected $fillable = [
        'name',
    ];
}