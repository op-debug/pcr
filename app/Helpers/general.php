<?php


if (!function_exists('get_name')) {
    function get_name() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['name']; 
    }
}


if (!function_exists('get_no_registrasi_lab')) {
    function get_no_registrasi_lab() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['no_registrasi_lab']; 
    }
}

if (!function_exists('dokter')) {
    function dokter() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['dokter_penanggungjawab']; 
    }
}

if (!function_exists('get_alamat')) {
    function get_alamat() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['alamat']; 
    }
}

if (!function_exists('get_phone')) {
    function get_phone() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['phone']; 
    }
}

if (!function_exists('get_no_lab')) {
    function get_no_lab() {
        $result = \App\Models\Settings::where("id", 1)->first();
        $cekNo = \App\Models\PCR::orderBy('id', 'DESC')->first();
        if ($cekNo){
            return $result['lab_patient_no'].($cekNo['run_nolab']+1);
        }
        else{
            return $result['lab_patient_no'].($result['run_nolab']+1); 
        }
    }
}

if (!function_exists('get_runno_lab')) {
    function get_runno_lab() {
        $result = \App\Models\Settings::where("id", 1)->first();
        $cekNo = \App\Models\PCR::orderBy('id', 'DESC')->first();
        if ($cekNo){
            return ($cekNo['run_nolab']+1);
        }
        else{
            return ($result['run_nolab']+1); 
        }
    }
}

if (!function_exists('get_runno_mitra')) {
    function get_runno_mitra($mitra_id) {
        $result = \App\Models\Settings::where("id", 1)->first();
        $cekNo = \App\Models\PCR::where('mitra_id')->orderBy('id', 'DESC')->first();
        if ($cekNo){
            return ($cekNo['run_nomitra']+1);
        }
        else{
            return ($result['run_mitra']+1); 
        }
    }
}

if (!function_exists('get_no_mitra')) {
    function get_no_mitra() {
        $result = \App\Models\Settings::where("id", 1)->first();
        $cekNo = \App\Models\PCR::orderBy('id', 'DESC')->first();
        if ($cekNo){
            return $result['mitra'].($cekNo['run_nomitra']+1);
        }
        else{
            return $result['mitra'].($result['run_mitra']+1); 
        }
    }
}

if (!function_exists('get_ttd_img')) {
    function get_ttd_img() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return 'public/img/'.$result['ttd_img'];  
    }
}

if (!function_exists('get_title_footer')) {
    function get_title_footer() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['title_footer']; 
    }
}

if (!function_exists('no_sip')) {
    function no_sip() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['sip_no']; 
    }
}

if (!function_exists('get_favicon')) {
    function get_favicon() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return 'public/img/'.$result['favicon']; 
    }
}

if (!function_exists('get_logo')) {
    function get_logo() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return 'public/img/'.$result['logo']; 
    }
}

if (!function_exists('get_logolab')) {
    function get_logolab() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return 'public/img/'.$result['logolab']; 
    }
}

if (!function_exists('get_copy')) {
    function get_copy() {
        $result = \App\Models\Settings::where("id", 1)->first();
        return $result['copy']; 
    }
}

if (!function_exists('get_data_login')) {
    function get_data_login() {
        $result = \Auth::user();
        return $result; 
    }
}

if (!function_exists('identitas_type')) {
    function identitas_type($id) {
        if($id=="KTP"){
            return 1;
        }
        else{
            return 2;
        } 
    }
}


if (!function_exists('get_branch')) {
    function get_branch() {
        if(get_data_login()->branch_id!=''){
            return get_data_login()->branch_id;
        }
        else{
            if(get_data_login()->mitra_id!=''){
                $result = \App\Models\Cabang::where("mitra_id", 1)->first();
                if (count($result)>0){
                    return $result->id;
                }
                else{
                    return 1;
                }
            }else{
                return 1;
            }
        } 
    }
}


if (!function_exists('get_branch_name')) {
    function get_branch_name($id) {
        $result = \App\Models\Cabang::where("id", $id)->first();
        return $result->b_name;
    }    
}

if (!function_exists('get_nik_type')) {
    function get_nik_type($id) {
        $result = \App\Models\Nationaly::where("id", $id)->first();
        return $result->name;
        
    }
}

if (!function_exists('get_mitra')) {
    function get_mitra() {
        if(get_data_login()->mitra_id!=''){
            return get_data_login()->mitra_id;
        }
        else{
            return 1;
        } 
    }
}

if (!function_exists('get_price')) {
    function get_price() {
        if(get_data_login()->mitra_id!=''){
            $getmitra = \App\Models\Mitra::where('id',$request->mitra_id)->first();
            return  $getmitra->m_harga_langganan;
        }
        else{
            return 100000;
        }
    }
}