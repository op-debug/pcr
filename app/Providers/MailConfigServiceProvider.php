<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Config;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $emailServices = DB::table('settings')->where('id',1)->first();

        if ($emailServices) {
            $config = array(
                'driver'     => 'smtp',
                'host'       => $emailServices->smpt,
                'port'       => $emailServices->port,
                'username'   => $emailServices->email,
                'password'   => $emailServices->password,
                'encryption' => $emailServices->encryption,
                'from'       => array('address' => $emailServices->email, 'name' => 'Hasil Test'),
                'sendmail'   => '/usr/sbin/sendmail -bs',
                'pretend'    => false,
            );

            Config::set('mail', $config);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
       
    }
}
