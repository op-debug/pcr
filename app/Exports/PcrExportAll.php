<?php

namespace App\Exports;


use App\Models\Pcr;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
// use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class PcrExportAll implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithEvents
// class PcrExportAll implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize
{
    protected $from_date;
    protected $to_date;

    function __construct($from_date,$to_date) {
            $this->from_date = $from_date;
            $this->to_date = $to_date;
    }
   
    public function collection()
    {
        return  $pcrs = Pcr::all();
                $pcrs->load('hasInspeksi');
                $pcrs->load('hasIdentitasType');
                $pcrs->load('hasDetailInspeksi');
                $pcrs->load('hasBranch');
    }

    public function registerEvents(): array {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
  
                $event->sheet->getDelegate()->getStyle('A1:AI1')
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setARGB('0000FF');
  
            },
        ];
    }

    public function headings(): array
    {
        return[
            
            // ['Title'],
            [
                'No',
                'Input Date',
                'Nationality',
                'Identity Type',
                'Identity 1',
                'Identity 2',
                'Patient Name',
                'DOB',
                'Gender',
                'Phone/Tlpn',
                'Email',
                'Penjaminan',
                'Lab Patient No',
                'Swab Datetime',
                'Swabber Name',
                'Receive Datetime',
                'Analysis Datetime',
                'Result Datetime',
                'PCR Result',
                'Egene',
                'RdRP gene',
                'Status Transaction',
                'Paid Datetime',
                'Status',
                'NAR',
                'NAR STATUS',
                'NAR DESC',
                'NAR DATE',
                'Inspection Purpose',
                'Detail Purpose',
                'Status Desc',
                'Status By',
                'Status Datetime',
                'Refferal Owner',
                'Refferal'
            ]
        ];
    }

    public function map($pcrs): array
    {    
        // if(isset($Pcr->hasAccount)){
        //     foreach($Pcr->hasAccount as $item){
        //         $kodeAkun = $item->kode_akun;
        //         $namaAkun = $item->name;
        //     }
        // }
        // if(isset($Pcr->hasSubAccount)){
        //     foreach($Pcr->hasSubAccount as $item){
        //         $kodeAkun = $item->kode_akun;
        //         $namaAkun = $item->name_sub;
        //     }
        // }
        return [
            $pcrs->id,
            $pcrs->created_at,
            $pcrs->nick_type,
            $pcrs->hasIdentitasType->name,
            $pcrs->nik,
            $pcrs->identitas_2,
            $pcrs->fullname,
            $pcrs->dob,
            $pcrs->gender,
            $pcrs->no_phone,
            $pcrs->email,
            $pcrs->penjamin,
            $pcrs->external_no,
            $pcrs->swabber_date,
            $pcrs->swabber_name,
            $pcrs->result_date,
            $pcrs->result_receive_date,
            $pcrs->result_analysis_date,
            $pcrs->result_date,
            $pcrs->result,
            $pcrs->egene,
            $pcrs->rdrp_gene,
            $pcrs->status_transaksi,
            $pcrs->status_print,
            $pcrs->nar,
            $pcrs->nar_status,
            $pcrs->desc_nar_status,
            $pcrs->nar_date,
            $pcrs->hasInspeksi->name,
            $pcrs->hasDetailInspeksi->name,
            $pcrs->status_desc,
            $pcrs->status_by,
            $pcrs->status_date,
            $pcrs->hasBranch->owner,
            $pcrs->hasBranch->name,
        ];
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //         'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //         'N' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //     ];
    // }
}
