<?php

namespace App\Imports;

use App\Models\PCR;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;

class PCRImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    use Importable;
 
    public function startRow(): int
    {
        return 23;
    }

    public function model(array $row)
    {
        return new PCR([
            'pasien_no'  =>$row[1],
            'identitas_type_id' =>identitas_type($row[2]),
            'nationaly'=>$row[3],
            'nik'=>$row[4],
            'fullname'=>$row[5],
            'dob'=>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[6]),
            'gender'=>$row[7],
            'relation'=>$row[8],
            'nik_type'=>$row[9],
            'no_phone'=>$row[10],
            'email'=>$row[11],
            'duration'=>$row[12],
            'swabber_date'=>$row[13],
            'swabber_name'=>$row[14],
            'inspeksi_id'=>$row[16],
            'detail_inspeksi_id'=>$row[17],
            'provinsi_id'=>$row[18],
            'kab_kota_id'=>$row[19],
            'kecamatan_id'=>(empty($row[20]))? '':$row[20],
            'kelurahan_id'=>(empty($row[21]))? '':$row[21],
            'alamat'=>$row[22],
            'mitra_id'=>get_mitra(),
            'branch_id'=>get_branch(),
            'branch_name'=>get_branch_name(get_branch()),
            'rt'=>'',
            'rw'=>'',
            'perusahaan_name'=>'',
            'status_swab_id'=>1,
            'external_no'=>'',
            'test'=>'PCR',
            'price'=>get_price()
        ]);
    }
}
