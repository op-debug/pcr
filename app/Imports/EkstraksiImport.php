<?php

namespace App\Imports;

use App\Models\Ekstraksi;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;

class EkstraksiImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    use Importable;
 
    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        return new Ekstraksi([
            'hole'  =>$row[0],
            'channel'   =>$row[1],
            'ct'    =>$row[2],
            'type'  =>$row[3],
            'purpose'   =>$row[4],
            'group_dt'  =>$row[5],
            'sample'    =>$row[6],
            'id_sampel_real'=>"",
            'target'    => $row[7] , 
            'gene'  =>$row[8],
            'id_prosess_excel'  =>date("YmdHis"),
        ]);
    }
}
