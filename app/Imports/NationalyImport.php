<?php

namespace App\Imports;

use App\Models\Nationaly;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;

class NationalyImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    use Importable;
 
    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        return new Nationaly([
            'id'  =>$row[0],
            'name'   =>$row[1],
        ]);
    }
}
