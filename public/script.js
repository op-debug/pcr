$(function(){
    $(".preloader_onclick").on("click", function(){
      preloader_block();
    })
  })
  
  function my_qstring_update(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
      return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
      return uri + separator + key + "=" + value;
    }
  }
  
  
  function preloader_block(){
    $(".preloader").css("display","block");
  }
  function preloader_none(){
    $(".preloader").css("display","none");
  }