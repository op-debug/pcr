$(function(){
    $(".modal").on("shown.bs.modal", function()  {
        window.history.pushState(null, null, "#modal-open");
    });

    $(window).on('popstate', function(event) {
        if ($(".modal:visible").length){
            window.history.forward();
            // alert("Dialog sedang dibuka");
            // $eu.messager.alert("Message","Dialog sedang terbuka");
        }else{
            // preloader_block();
        }
    });

    $(window).on('pushstate', function(event) {
        preloader_none();
        // alert();
    });

    $(".modal").on('hidden.bs.modal', function () {
        my_modal_onclose();
    });
})



function my_modal_create(my_modal_id = "my_modal_id", my_modal_size = "", my_modal_url = '#', my_modal_title='My Modal Title', my_modal_display = 1, my_modal_size_2 = 0){
    if($('div#my_modal_frame').length == 0){
        $("body").append("<div id='my_modal_frame'></div>");
    }

    var my_modal_frame = $("#my_modal_frame");
    var my_modal = "";
    my_modal += '<div class="modal fade" id="'+my_modal_id+'">';
        my_modal += '<div class="modal-dialog '+my_modal_size+'">';
             my_modal += '<div class="modal-content">';
                my_modal += '<div class="modal-header my_bg_inatti text-white">';
                    my_modal += '<h5 class="modal-title">'+my_modal_title+'</h5>';
                    // my_modal += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    my_modal += '<button type="button" class="close" onclick="my_modal_close(\''+my_modal_id+'\')"><span aria-hidden="true">&times;</span></button>';
                my_modal += '</div>';
                my_modal += '<div class="modal-body">Loading...';
                my_modal += '</div>';
                my_modal += '<div class="modal-footer">';
                    // my_modal += '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>';
                    // my_modal += '<button type="button" class="btn btn-primary">Save changes</button>';
                    // my_modal += '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
                my_modal += '</div>';
            my_modal += '</div>';
        my_modal += '</div>';
    my_modal += '</div>';

    $("#"+my_modal_id).remove();
    my_modal_frame.append(my_modal);
    if(my_modal_size_2 > 0){
        $("#"+my_modal_id+" .modal-dialog").css("max-width",my_modal_size_2+"px !important");
    }
    if(my_modal_display == 0){
        $("#"+my_modal_id+" .modal-dialog").css("display","none");
    }
    
    my_modal_url = my_qstring_update(my_modal_url, "my_modal_id", my_modal_id);

    preloader_block();
    $("#"+my_modal_id+" .modal-dialog .modal-content .modal-body").load(my_modal_url, function(){
        var my_modal_footer = $("#"+my_modal_id+" #my_modal_footer").html();
        $("#"+my_modal_id+" #my_modal_footer").remove();
        $("#"+my_modal_id+" .modal-dialog .modal-content .modal-footer").html(my_modal_footer);
        window.history.pushState(null, null, "#modal-open");
        preloader_none();
        setTimeout(function(){
            $(".my_btn_request_camera_permissions").trigger("click");
        },1000)

    });

    
    $("#"+my_modal_id).on('hidden.bs.modal', function () {
        $(".my_btn_stop_scanning", this).trigger("click");
        my_modal_onclose();
    });
    
}

function my_modal_close(my_modal_id = "my_modal_id"){
    $('#'+my_modal_id).modal('toggle');
}

function my_modal_onclose(){
    if($("#my_modal_frame > div.show").length > 0){
        setTimeout(function(){
            $("body").addClass("modal-open");
        },150)
        window.history.pushState(null, null, "#modal-open");
    }else{
        window.history.back();
    }
}




function my_modal_create_local(my_modal_id = "my_modal_id", my_modal_size = "", my_modal_url = '#', my_modal_title='My Modal Title'){
    if($('div#my_modal_frame').length == 0){
        $("body").append("<div id='my_modal_frame'></div>");
    }

    if($("#"+my_modal_id).length == 0){
        var my_modal_frame = $("#my_modal_frame");
        var my_modal = "";
        my_modal += '<div class="modal fade" id="'+my_modal_id+'">';
            my_modal += '<div class="modal-dialog '+my_modal_size+'">';
                my_modal += '<div class="modal-content">';
                    my_modal += '<div class="modal-header my_bg_inatti text-white">';
                        my_modal += '<h5 class="modal-title">'+my_modal_title+'</h5>';
                        // my_modal += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                        my_modal += '<button type="button" class="close" onclick="my_modal_close(\''+my_modal_id+'\')"><span aria-hidden="true">&times;</span></button>';
                    my_modal += '</div>';
                    my_modal += '<div class="modal-body">';
                    my_modal += '</div>';
                    my_modal += '<div class="modal-footer">';
                        // my_modal += '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>';
                        // my_modal += '<button type="button" class="btn btn-primary">Save changes</button>';
                        // my_modal += '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
                    my_modal += '</div>';
                my_modal += '</div>';
            my_modal += '</div>';
        my_modal += '</div>';

        $("#"+my_modal_id).remove();
        my_modal_frame.append(my_modal);

        
        my_modal_url = my_qstring_update(my_modal_url, "my_modal_id", my_modal_id);

        preloader_block();
        $("#"+my_modal_id+" .modal-dialog .modal-content .modal-body").load(my_modal_url, function(){
            var my_modal_footer = $("#"+my_modal_id+" #my_modal_footer").html();
            $("#"+my_modal_id+" #my_modal_footer").remove();
            $("#"+my_modal_id+" .modal-dialog .modal-content .modal-footer").html(my_modal_footer);
            // window.history.pushState(null, null, "#modal-open");
            preloader_none();
            // setTimeout(function(){
            //     $(".my_btn_request_camera_permissions").trigger("click");
            // },1000)

        });

        
        $("#"+my_modal_id).on('hidden.bs.modal', function () {
            $(".my_btn_stop_scanning", this).trigger("click");
            my_modal_onclose();
        });
    }else{
        // window.history.pushState(null, null, "#modal-open");
        // setTimeout(function(){
        //     $(".my_btn_request_camera_permissions").trigger("click");
        // },1000)
    }

}