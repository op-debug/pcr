function my_select2_generate_option(selector = null, data = []){
    if($(selector).length > 0) {
      for (var i = 0; i < data.length; i++) {
        var option  = data[i];
        var selected = "";
        if(option.hasOwnProperty('selected') && option['selected']==true){ selected='selected' }
        $(selector).append("<option value='"+option['id']+"' "+selected+">"+option['text']+"</option>")
      }
    }
  }