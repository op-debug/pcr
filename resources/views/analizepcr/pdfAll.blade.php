<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<style>
    .divider{
        border-bottom: 5px solid gray;
        margin-top: -20px;
    }
    .divider-1{
        border-bottom: 5px solid gray;
       
    }
    .title p{
        font-size: 14px;
        font-weight: bold;
        text-align: center;
        padding-top: 20px;
    }
    td{
        font-size: 10px;
        line-height: 1.1;
    }
    .table-wrapper-1{
        margin-top: 50px;
    }
    .flex-1{
        margin-bottom: 50px;
    }
    th{
        font-size: 12px;
    }
</style>
<body>
    

        <div>
            <div style="float: left; margin-top: -70px;">
                <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/app-assets/images/logo/logo_print.jpg';?>" width="100px" height="100px"/>
                {{-- <img src="{{ URL::to('app-assets/images/logo/logo_print.jpg') }}" alt="logo" width="100px" height="100px"> --}}
            </div>
            <div style="float: right;">
                <p style="font-size: 9px; margin-top: 2px;">No. Registrasi Lab. Pemeriksa PCR: 123456</p>
            </div>
            {{-- <p class="text-danger text-center">{{Carbon\Carbon::parse($from)->translatedFormat('d F Y')}}&nbsp;-&nbsp;{{Carbon\Carbon::parse($to)->translatedFormat('d F Y')}}</p> --}}
        </div>
        <div class="divider"></div>

        <div class="title" style="margin-top: 20px;">
            <p>HASIL LAPORAN TES CORONAVIRUS (COVID-19) <br> CORONAVIRUS (COVID-19) real time RT-PCR RESULT REPORT</p>
        </div>

        <div class="flex-1">

            <div style="float: left;">
            <div class="">
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama Pasien / Name  </td>
                                <td>:</td>
                                <td><strong>{{$pcrs->fullname}}</strong></td>
                            </tr>
                            <tr>
                                <td>No. Identitas / Patient ID</td>
                                <td>:</td>
                                <td>{{$pcrs->nik}}</td>
                            </tr>
                            <tr>
                                <td>Kebangsaan / Nationality</td>
                                <td>:</td>
                                <td>{{$pcrs->nik_type}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td><strong>Sampling Point</strong></td>
                                <td>:</td>
                                <td><strong>{{$pcrs->hasBranch->name}}</strong></td>
                            </tr>
                            <tr>
                                <td>Pengambilan Spesimen <br> &nbsp; Date of Specimen Taken</td>
                                <td>:</td>
                                <td><?= date('d-M-Y');?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Sample Diterima <br> Receiving date</td>
                                <td>:</td>
                                <td><?= date('d-M-Y');?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>

            <div style="float: right;">
            <div class="">
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td>Jenis Kelamin / Gender  &nbsp;</td>
                                <td>:</td>
                                @if($pcrs->gender == 'm')
                                <td>Laki-Laki ({{$pcrs->gender}})</td>
                                @else
                                <td>Perempuan ({{$pcrs->gender}})</td>
                                @endif
                            </tr>
                            <tr>
                                <td>Tgl. Lahir / DOB</td>
                                <td>:</td>
                                <td>12/12/2004</td>
                            </tr>
                            <tr>
                                <td>Umur / Age&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>
                                <td>:</td>
                                <td>{{Carbon\Carbon::parse($pcrs->dob)->translatedFormat('d/m/Y')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td>Tanggal Analisis Date of <br>  Specimen Taken Date of Analysis</td>
                                <td>:</td>
                                <td><?= date('d-M-Y');?></td>
                            </tr>
                            <tr>
                                <td>Tanggal hasil keluar <br> Receiving date Result date</td>
                                <td>:</td>
                                <td><?= date('d-M-Y');?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>

        </div>

        <div class="table-wrapper-1" style="padding-top: 100px;">
            <table class="table">
                <thead>
                    <tr class="table-info">
                        <th class="text-center">Test Name</th>
                        <th class="text-center">Reagent</th>
                        <th class="text-center">Gent Target</th>
                        <th class="text-center">Ct. Value</th>
                        <th class="text-center">Result</th>
                        <th class="text-center">Test Method</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">{{$pcrs->swabber_name}}</td>
                        <td align="center">Biosewoom</td>
                        <td align="center">RdRP -E Gene</td>
                        <td align="center">-</td>
                        @if($pcrs->result == 0)
                        <td align="center"><strong>Negative</strong></td>
                        @else
                        <td align="center"><strong>Positive</strong></td>
                        @endif
                        <td align="center">Real Time RT-PCR</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">Bahwa hasil tes Coronavirus (COVID-19) adalah/That the Corona Virus (COVID-19) test result is: 
                            @if($pcrs->result == 0)
                            <strong>Negative</strong> 
                            @else
                            <strong>Positive</strong>
                            @endif
                        </td>
                    </tr>
                </tfoot>
            </table>
            <p style="font-size: 12px; margin-left:10px;">Jenis Spesimen / Specimen type : Naso and Oropharyngeal swab</p>
            <p style="font-size: 12px; margin-left:10px; font-weight: bold;">Catatan / Note:</p>
            <div>
                <ol style="font-size: 10px;">
                    <ul style="font-size: 10px;">
                        <li>Hasil hanya menggambarkan kondisi saat pengambilan sampel, sesuaikan dengan klinis pasien. Pemeriksaan ulang
                            dapat dilakukan atas saran dokter penanggung jawab./ The result may only reflect the day when specimen was
                            taken, please consult to your physician for follow up examination.
                        </li>
                        <li>Cut Off CT Gen Target : 38</li>
                    </ul>
                </ol>
            </div>
            
        </div>

        <div style="padding-top: 200px;">
            <div style="float: left;">
                <div>
                    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/app-assets/images/logo/test-qr.png';?>" width="150px" height="150px"/>
                    {{-- <img src="{{ URL::to('app-assets/images/logo/test-qr.png') }}" alt="logo" width="150px" height="150px"> --}}
                </div>
                <div>
                    <p style="font-size: 14px;">C.511.00.2203260117</p>
                </div>
            </div>
            <div style="float: right;">
                <div>
                    <p style="font-size: 12px;">Dokter Penanggung Jawab Doctor in charge</p>
                </div>
                <div>
                    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/app-assets/images/logo/logo_print.jpg';?>" width="100px" height="100px"/>
                    {{-- <img src="{{ URL::to('app-assets/images/logo/logo_print.jpg') }}" alt="logo" width="100px" height="100px"> --}}
                </div>
                <div>
                    <p style="font-size: 12px; border-bottom: 2px solid gray; font-weight: 700;">dr. Erike A. Suwarsono, Sp.MK</p>
                    <p style="font-size: 10px; margin-top: -10px;">SIP.No. 36/B.15B/31.74.05.1002.05.028.R.9/3/-1.779.3/e/2021</p>
                </div>
            </div>      
        </div>
        
        
        <div style="padding-top: 200px">
            <div class="divider-1"></div>
            <strong>Smart Mobile Laboratory (SML-BSL2)</strong>
            <p style="font-size: 12px;">Pluit Village Mall, Jl. Pluit Indah Raya, Kel. Pluit, Kec. Penjaringan, Kota Adm. Jakarta Utara, Prov. DKI Jakarta, 14450</p>
            <p style="font-size: 12px;">Contact Person : +62 813-8136-7915</p>
        </div>
        
        
        
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>

</html>