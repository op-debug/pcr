<div class="modal fade" id="addPcr" tabindex="-1" aria-labelledby="addPcrLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<h5 class="modal-title" id="addPcrLabel">Add Patient</h5>
				<button type="button" class="btn-close btn-secondary" data-bs-dismiss="modal" aria-label="Close">X</button>
			</div>

			<div class="modal-body">
				<div class="modal-content">

					<form id="form-addPcr" method="POST" action="{{route('pcr-store')}}" enctype="multipart/form-data">
						@csrf
						<div class="pt-2 pb-2 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-6"><b>Branch Data</b></span>
								</div>
							</div>
							@if(isset($branchs))
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Branch</label>
								<select name="branch_id" id="branch_id"
									class="form-control {{$errors->has('branch_id') ? 'validatebox-invalid' : '' }}">
									<option value="">-Pilih-</option>
									@foreach ($branchs as $branch)
									<option value="{{$branch->id}}" {{ old('branch_id')==$branch->id ?
										'selected="selected"': '' }}>{{$branch->name}}</option>
									@endforeach
								</select>
								<input type="hidden" id="branch_name" name="branch_name">
								@error('branch_id') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							@endif
						</div>
						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-6"><b>Profile</b></span>
									<div class="col-lg-6">
										<div class="float-right" id="label_use_nar_biodata">
											<label class="textbox-label textbox-label-before" for="_easyui_checkbox_7"
												style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use
													Profile From SISLAB?</b></label>
											<input class="easyui-checkbox checkbox-f" value="1" name="status_profile"
												type="checkbox" style="margin-left: 0px; margin-top: 0px;">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Identity
									Type</label>
								@if(isset($identitasTypes))
								<div class="col-lg-12">
									<select name="identitas_type_id"
										class="form-control {{$errors->has('identitas_type_id') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										@foreach ($identitasTypes as $identitasType)
										<option value="{{$identitasType->id}}" {{
											old('identitas_type_id')==$identitasType->id ? 'selected="selected"': ''
											}}>{{$identitasType->name}}</option>
										@endforeach
									</select>
									@error('identitas_type_id') <span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
								@endif
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">NIK</label>
								<div class="col-lg-5 mb-1">
									<select name="nik_type"
										class="form-control {{$errors->has('nik_type') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										<option value="id_indonesia" {{ old('nik_type')=="id_indonesia"
											? 'selected="selected"' : '' }}> ID(Indonesia) </option>
										<option value="national" {{ old('nik_type')=="national" ? 'selected="selected"'
											: '' }}> National </option>
									</select>
									@error('nik_type') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								<div class="mb-1 col-lg-7" id="field_nik">
									<input type="number" name="nik" value="{{old('nik')}}"
										class="form-control {{$errors->has('nik') ? 'validatebox-invalid' : '' }}"
										placeholder="NIK">
									@error('nik') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Fullname</label>
								<div class="col-lg-12">
									<input type="text" name="fullname" value="{{old('fullname')}}"
										class="form-control {{$errors->has('fullname') ? 'validatebox-invalid' : '' }}"
										placeholder="Fullname">
									@error('fullname') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="_102_f_dob" class="col-lg-12 col-form-label pb-1">DOB</label>
								<div class="col-lg-12">
									<input type="date" name="dob" value="{{old('dob')}}"
										class="form-control {{$errors->has('dob') ? 'validatebox-invalid' : '' }}"
										placeholder="DOB">
									@error('dob') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Gender</label>
								<div class="col-lg-12">
									<select name="gender"
										class="form-control {{$errors->has('gender') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										<option value="m" {{ old('gender')=="m" ? 'selected="selected"' : '' }}> Male
										</option>
										<option value="f" {{ old('gender')=="f" ? 'selected="selected"' : '' }}> Female
										</option>
									</select>
									@error('gender') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>

						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-6"><b>Domicile address</b></span>
									<div class="col-lg-6">
										<div class="float-right" id="label_use_nar_address">
											<label class="textbox-label textbox-label-before"
												style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use
													NAR Data?</b></label>
											<input class="easyui-checkbox checkbox-f" value="1" name="domisili_status"
												type="checkbox" style="margin-left: 0px; margin-top: 0px;">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Provinsi</label>
								@if(isset($provinces))
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('provinsi_id') ? 'validatebox-invalid' : '' }}"
										name="provinsi_id" id="provinsi">
										<option value="">-Pilih Provinsi-</option>
										@foreach ($provinces as $item)
										<option value="{{ $item->id}}">{{ $item->name}}</option>
										@endforeach
									</select>
									<img src="{{ asset('public/app-assets/images/loading.gif')}}" width="35" id="load1"
										style="display:none;" />
									@error('provinsi_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								@endif
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Kabupaten/Kota</label>
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('kab_kota_id') ? 'validatebox-invalid' : '' }}"
										name="kab_kota_id" id="kota">
										<option value="">-Pilih Kabupaten/Kota-</option>
									</select>
									<img src="{{ asset('public/app-assets/images/loading.gif')}}" width="35" id="load2"
										style="display:none;" />
									@error('kab_kota_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Kecamatan</label>
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('kecamatan_id') ? 'validatebox-invalid' : '' }}"
										name="kecamatan_id" id="kecamatan">
										<option value="">-Pilih Kecamatan-</option>
									</select>
									<img src="{{ asset('public/app-assets/images/loading.gif')}}" width="35" id="load3"
										style="display:none;" />
									@error('kecamatan_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="_102_f_kelurahan_code"
									class="col-lg-12 col-form-label pb-1">Kelurahan</label>
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('kelurahan_id') ? 'validatebox-invalid' : '' }}"
										name="kelurahan_id" id="kelurahan">
										<option value="">-Pilih Kelurahan-</option>
									</select>
									@error('kelurahan_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<div class="col-lg-6">
									<div class="row">
										<label class="col-lg-12 col-form-label pb-1">RT</label>
										<div class="col-lg-12">
											<input type="text" name="rt" value="{{old('rt')}}"
												class="form-control {{$errors->has('rt') ? 'validatebox-invalid' : '' }}"
												placeholder="RT" />
											@error('rt') <span class="text-danger">{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="row">
										<label class="col-lg-12 col-form-label pb-1">RW</label>
										<div class="col-lg-12">
											<input type="text" name="rw" value="{{old('rw')}}"
												class="form-control {{$errors->has('rw') ? 'validatebox-invalid' : '' }}"
												placeholder="RW" />
											@error('rw') <span class="text-danger">{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="_102_f_address" class="col-lg-12 col-form-label pb-1">Alamat</label>
								<div class="col-lg-12">
									<textarea name="alamat"
										class="form-control {{$errors->has('alamat') ? 'validatebox-invalid' : '' }}"
										autocomplete="off" tabindex="" placeholder="Alamat..."
										style="vertical-align: top;">{{old('alamat')}}
										</textarea>
									@error('alamat') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>

						</div>

						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-12"><b>More</b></span>
								</div>
							</div>

							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Handphone
									No.</label>
								<div class="col-lg-12">
									<input type="number" name="no_phone" value="{{old('no_phone')}}"
										class="form-control {{$errors->has('no_phone') ? 'validatebox-invalid' : '' }}"
										placeholder="Handphone No." />
									@error('no_phone') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Email</label>
								<div class="col-lg-12">
									<input type="text" name="email" value="{{old('email')}}"
										class="form-control {{$errors->has('email') ? 'validatebox-invalid' : '' }}"
										placeholder="Email" />
									@error('email') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>

							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Corporate
									Name</label>
								<div class="col-lg-12">
									<input type="text" name="perusahaan_name" value="{{old('perusahaan_name')}}"
										class="form-control {{$errors->has('perusahaan_name') ? 'validatebox-invalid' : '' }}"
										placeholder="Corporate Name" />
									@error('perusahaan_name') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>

						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-12"><b>Detail Test</b></span>
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Inspection
									purpose</label>
								@if($inspeksis)
								<div class="col-lg-12">
									<select name="inspeksi_id"
										class="form-control {{$errors->has('inspeksi_id') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										@foreach($inspeksis as $inspeksi)
										<option value="{{$inspeksi->id}}" {{ old('inspeksi_id')==$inspeksi->id ?
											'selected="selected"': '' }}>{{$inspeksi->name}}</option>
										@endforeach
									</select>
									@error('inspeksi_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								@endif
							</div>

							<div id="form_detail" class="d-none d-block">
								<div class="form-group row mb-1">
									<label class="col-lg-12 col-form-label pb-1">Detail</label>
									@if($detailInspeksis)
									<div class="col-lg-12">
										<select name="detail_inspeksi_id" id="f_branch_name"
											class="form-control {{$errors->has('detail_inspeksi_id') ? 'validatebox-invalid' : '' }}">
											<option value="">-Pilih-</option>
											@foreach ($detailInspeksis as $detailInspeksi)
											<option value="{{$detailInspeksi->id}}" {{
												old('detail_inspeksi_id')==$detailInspeksi->id ? 'selected="selected"':
												'' }}>{{$detailInspeksi->name}}</option>
											@endforeach
										</select>
										@error('detail_inspeksi_id') <span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
									@endif
								</div>
							</div>

							<div class="form-group row mb-1">
								<label for="_102_f_status" class="col-lg-12 col-form-label pb-1">Status</label>
								@if($statusSwabs)
								<div class="col-lg-12">
									<select name="status_swab_id" id="f_branch_name"
										class="form-control {{$errors->has('status_swab_id') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										@foreach ($statusSwabs as $statusSwab)
										<option value="{{$statusSwab->id}}" {{ old('status_swab_id')==$statusSwab->id ?
											'selected="selected"': '' }}>{{$statusSwab->name}}</option>
										@endforeach
									</select>
									@error('status_swab_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								@endif
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Swabber
									Name</label>
								<div class="col-lg-12">
									<input type="text" name="swabber_name" value="{{old('swabber_name')}}"
										class="form-control {{$errors->has('swabber_name') ? 'validatebox-invalid' : '' }}">
									@error('swabber_name') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Swab Date</label>
								<div class="col-lg-12">
									<input type="datetime-local" name="swabber_date" value="{{old('swabber_date')}}"
										class="form-control {{$errors->has('swabber_date') ? 'validatebox-invalid' : '' }}">
									@error('swabber_date') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Pasien
									No.</label>
								<div class="col-lg-9 col-9">
									<input type="text" name="pasien_no" id="pasien_no_create" value="{{old('pasien_no')}}"
										class="form-control {{$errors->has('pasien_no') ? 'validatebox-invalid' : '' }}">
									@error('pasien_no') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								<div class="col-lg-3 col-3">
									<div class="btn btn-sm btn-block btn-info" onclick="generateCodePcr()">
										Get No.</div>
								</div>
							</div>

							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">External No.</label>
								<div class="col-lg-12">
									<input type="text" name="external_no" value="{{old('external_no')}}"
										class="form-control {{$errors->has('external_no') ? 'validatebox-invalid' : '' }}">
									@error('external_no') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button href="javascript:void(0)" class="btn btn-block btn-md btn-primary" id="btnSubmit"
								type="submit">Save</button>
						</div>
					</form>
					{{-- <div class="alert alert-danger" style="display:none"></div> --}}
				</div>
			</div>
			{{-- <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div> --}}
		</div>
	</div>
</div>
@push('scripts')

<script>
	function generateCodePcr(){
        $.ajax({
            type: 'GET',
            dataType: "JSON",
            url: "{{ route('pcr-generate-code') }}",
            success: function(res) {
				if(res.success == true){
					$('#pasien_no_create').val(res.data);
				}
				// console.log(res)
			},
            error:function(err){
               console.log(err)
            }
        })
    }

	$(document).ready(function(){
		$('#branch_id').change(function() {
			$('#branch_name').val($('option:selected', this).text());
		});
		
		//saat pilihan provinsi di pilih maka mengambil data di data-wilayah menggunakan ajax
        $("#provinsi").change(function(){
            $("img#load1").show();
            var id_provinces = $(this).val(); 
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "{{route('regencies')}}",
                data: "id_provinces="+id_provinces,
                success: function(res){
                
                    $("select#kota").html(res);                                                       
                    $("img#load1").hide();
                    getAjaxKota();                                                        
                }
            });                    
        });  

        $("#kota").change(getAjaxKota);
        function getAjaxKota(){
            $("img#load2").show();
            var id_regencies = $("#kota").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "{{route('districts')}}",
                data: "id_regencies="+id_regencies,
                success: function(res){
                    $("select#kecamatan").html(res);                              
                    $("img#load2").hide(); 
                getAjaxKecamatan();                                                    
                }
            });
        }

        $("#kecamatan").change(getAjaxKecamatan);
        function getAjaxKecamatan(){
            $("img#load3").show();
            var id_district = $("#kecamatan").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "{{route('villages')}}",
                data: "id_district="+id_district,
                success: function(res){
                    $("select#kelurahan").html(res);                              
                    $("img#load3").hide();                                                 
                }
            });
        }

	});
</script>
{{-- @if (count($errors) > 0)
    <script>
        $( document ).ready(function() {
            $('#addPcr').modal('show');
        });
    </script>
@endif --}}
@endpush