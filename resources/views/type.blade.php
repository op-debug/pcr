@extends('app')

@section('content')
<section id="branch">
  <div class="row match-height">
      <div class="col-xl-12 col-md-6 col-12">
        <div class="easyui-layout" data-options="fit:true" style="width: 1237px; height: 154px;">
          <div data-options="region:'center', border:false, split:true">
            <div class="easyui-layout" data-options="fit:true">
              <div data-options="region:'center', split:true, title:'List Jenis Identitas'" >
                <table id="_type_grid"
                url="{{url('api/type/getdata')}}",>
                </table>

                  <div id="_type_grid_toolbar" class="p-1">
                      <div class="row">
                        <div class="col-12">
                          
                          <div style="float: left">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_type_grid_add()"><i class="fa fa-plus fa-fw fa-lg" style="color:green"></i>&nbsp;Add</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_type_grid_edit()"><i class="fa fa-edit fa-fw fa-lg" style="color:green"></i>&nbsp;Edit</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_type_grid_delete()"><i class="fa fa-trash fa-fw fa-lg" style="color:green"></i>&nbsp;Delete</a>
                          </div>
                        </div>
                      </div>
              </div>
          </div>
        </div>
    </div>
  </div>

  <div id="_type_grid_dialog" class="easyui-dialog" style="width:50%;height:220px;padding:10px 25px"
        closed="true" buttons="#dlg-buttons">
    <form id="fm" method="post"  novalidate>
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="t_name" class="form-control" required="true">
        </div>
    </form>
  </div>
  <div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savebranch()" style="width:90px">Save</a>
</div>
</section>
<script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script>

<script type="text/javascript">
  var url;
  $(function(){

    $("#_type_grid").datagrid({
      toolbar: "#_type_grid_toolbar",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "id",
      
      columns: [[
        {field: "t_name",title: "<b>Jenis Identitas</b>",align: "left",width: 450},
      ]],
    })
  })

  function _jf_type_grid_add(){
    $('#_type_grid_dialog').dialog('open').dialog('setTitle','New Jenis Identitas');
    $('#fm').form('clear');
    url= "{{url('api/type/store')}}";
  }
   
  
  function _jf_type_grid_edit(){
    var row = $('#_type_grid').datagrid('getSelected');
		if (row){
      $('#_type_grid_dialog').dialog('open').dialog('setTitle','Edit Jenis Identitas');
      $('#fm').form('load',row);
      url = "{{url('api/type/update')}}/"+row.id;
    }
  }

  function savebranch(){
    $.ajax({
        type: "POST",
        url: url,
        data:$('#fm').serialize(),
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#_type_grid_dialog').dialog('close'); 
                $('#_type_grid').datagrid('reload');   
            }
        }
    });
  }

  function _jf_type_grid_delete(){
			var row = $('#_type_grid').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this mitra?',function(r){
					if (r){
						$.post("{{url('api/type/delete')}}",{id:row.id},function(result){
							if (result.success){
								$('#_type_grid').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							}
						},'json');
					}
				});
			}
		}
 
</script>
@endsection

