<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
	<meta name="description"
		content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
	<meta name="keywords"
		content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
	<meta name="author" content="PIXINVENT">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>{{ get_name() }}</title>
	<link rel="apple-touch-icon" href="{{ get_favicon()}}">
	<link rel="shortcut icon" type="image/x-icon" href="{{ get_favicon()}}">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
		rel="stylesheet">

	<!-- BEGIN: Vendor CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/vendors/css/vendors.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/vendors/css/charts/apexcharts.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/vendors/css/extensions/toastr.min.css')}}">
	<!-- END: Vendor CSS-->

	<!-- BEGIN: Theme CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/bootstrap-extended.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/colors.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/components.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/themes/dark-layout.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/themes/bordered-layout.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/themes/semi-dark-layout.min.css')}}">

	<!-- BEGIN: Page CSS-->
	<link rel="stylesheet" type="text/css"
		href="{{ asset('public/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/pages/dashboard-ecommerce.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/app-assets/css/plugins/charts/chart-apex.min.css')}}">
	<link rel="stylesheet" type="text/css"
		href="{{ asset('public/app-assets/css/plugins/extensions/ext-component-toastr.min.css')}}">
	<!-- END: Page CSS-->

	<!-- BEGIN: Custom CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/style.css')}}">
	<!-- END: Custom CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/easyui/easyui.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/easyui/icon.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/easyui/color.css')}}">

	<link href="{{ asset('public/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	
	<!-- END: Head-->

	<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click"
	data-menu="vertical-menu-modern" data-col="">

	<!-- BEGIN: Header-->
	<nav
		class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
		<div class="navbar-container d-flex content">
			<div class="bookmark-wrapper d-flex align-items-center">
				<ul class="nav navbar-nav d-xl-none">
					<li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon"
								data-feather="menu"></i></a></li>
				</ul>
			</div>
			<ul class="nav navbar-nav align-items-center ml-auto">
				<li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link"
						id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">
						<div class="user-nav d-sm-flex d-none"><span
								class="user-name font-weight-bolder">{{ get_data_login()->name }}</span><span class="user-status">{{get_data_login()->role}}</span>
						</div><span class="avatar">
							<img class="round" src="public/img/{{ get_data_login()->gambar }}" alt="avatar"
								height="40" width="40"><span class="avatar-status-online"></span></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
						<a class="dropdown-item" href="{{ route('setting_user')}}"><i class="mr-50" data-feather="user"></i>
							Profile</a>
						<a class="dropdown-item" href="{{ route('actionlogout') }}"><i class="mr-50"
								data-feather="power"></i>
							Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>
	<!-- END: Header-->

	@include('menu')

	<!-- BEGIN: Content-->
	<div class="app-content content ">
		<div class="content-overlay"></div>
		<div class="header-navbar-shadow"></div>
		<div class="content-wrapper container-xxl p-0">
			<div class="content-header row">
			</div>
			<div class="content-body">
				<!-- Dashboard Ecommerce Starts -->
				@yield('content')
				<!-- Dashboard Ecommerce ends -->
			</div>
		</div>
	</div>
	<!-- END: Content-->


	<div class="sidenav-overlay"></div>
	<div class="drag-target"></div>

	<!-- BEGIN: Footer-->
	<footer class="footer footer-static footer-light">
		<p class="clearfix mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">{{ get_copy()}}</span></span><span
				class="float-md-right d-none d-md-block"></span></p>
	</footer>
	<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
	<!-- END: Footer-->

	<script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
	{{-- <script src="{{ asset('public/modal.js')}}"></script> --}}
	<script src="{{ asset('public/script.js')}}"></script>
	<script src="{{ asset('public/select2.js')}}"></script>
	<script type="text/javascript" src="{{ asset('public/html5-qrcode.min.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<!-- BEGIN: Vendor JS-->
	<script src="{{ asset('public/app-assets/vendors/js/vendors.min.js')}}"></script>
	
	<!-- BEGIN Vendor JS-->

	<!-- BEGIN: Theme JS-->
	<script src="{{ asset('public/app-assets/js/core/app-menu.min.js')}}"></script>
	<script src="{{ asset('public/app-assets/js/core/app.min.js')}}"></script>
	<script src="{{ asset('public/app-assets/js/scripts/customizer.min.js')}}"></script>
	<!-- END: Theme JS-->

	<!-- BEGIN: Page JS-->
	{{-- <script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script> --}}
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="{{ asset('public/easyui/jquery.easyui.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('public/easyui/jquery.color.js')}}"></script>
	<script type="text/javascript" src="{{ asset('public/easyui/datagrid-cellediting.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script src="{{ asset('public/app-assets/vendors/js/momment/moment-with-locales.js')}}"></script>

	<!-- END: Page JS-->
	@stack('scripts')

	<script>
		$(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      })
	</script>



</body>
<!-- END: Body-->

</html>