@extends('app')

@section('content')
<section id="pcr">
	<div class="row match-height">
		<div class="col-xl-12 col-md-12 col-12">
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Opps Something went wrong</strong>
                <button type="button" class="btn-close btn-secondary" data-bs-dismiss="alert" aria-label="Close">X</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
              </div>
            @endif
            
            @include('pcrekstraksi.table')
        </div>
    </div>
</section>

<style>
    .select2-selection__arrow b{
        display:none !important;
    }
</style>

@include('pcrekstraksi.create')
@include('pcrekstraksi.edit')
@endsection

@push('scripts')
<script>


    $(function(){
        var dg = $('#pcr-table').datagrid();
        
        dg.datagrid({
            toolbar: "#pcr-table-toolbar",
            striped: true,
            pagination: true,
            fit: true,
            fitColumns: false,
            rownumbers: true,
            checkbox: true,
            singleSelect: true,
            selectOnCheck: false,
            checkOnSelect: false,
            nowrap: false,
            pageList: [10, 20, 30, 40, 50, 100],
            pageSize: 100,
            method: "POST",
            idField: "id",
            frozenColumns: [[
                {field: "ck",checkbox:true},
            ]],
            columns:[[
                {field:'r_status',title:'Status', align:'center', sortable:false,width: 120,
                    formatter: function(value,row,index){
                        if(value == ''){
                            return '<span style="color: #000;">Entry</span>';
                        }else if(value == 'dikirim'){
                            return '<span style="color: skyblue;">Dikirim</span>';
                        }else if(value == 'diterima'){
                            return '<span style="color: blue;">Diterima</span>';
                        }else if(value == "ekstraksi"){
                            return '<span style="color: red;">Prosess Ekstraksi</span>';
                        }else if(value=="selesaiekstraksi"){
                            return '<span style="color: green;">Finish Ekstraksi</span>';
                        }else if(value=="analize"){
                            return '<span style="color: red;">Analize PCR</span>';
                        }
                        else{
                            return '-';
                        }
                        
                    }
                },
                {field:'nar',title:'NAR', align:'center', sortable:false,width: 65,
                    styler : function(value,row,index){
                        if(value == 1){
                            return {class:'some-class', style:'color:white;background-color:#ff0000;'}
                        }else if(value==2){
                            return {class:'some-class', style:'color:white;background-color:#008000;'}
                        }
                    },
                    formatter: function(value,row,index){
                        if(value == 1){
                            return '<div class="bg-success text-white" style="height:100%; width:100%;">-</div>';
                        }else if (value==2){
                            return '<div class="bg-danger text-white" style="height:100%; width:100%;">-</div>';
                        }
                        else{
                            return '';
                        }
                    }
                },
                {field:'nar_status',title:'NAR Status', align:'center', sortable:false,width: 165,
                    styler : function(value,row,index){
                        if(value == 1){
                            return {class:'some-class', style:'color:white;background-color:#ff0000;'}
                        }else if(value==2){
                            return {class:'some-class', style:'color:white;background-color:#008000;'}
                        }
                    },
                    formatter: function(value,row,index){
                        if(value == 1){
                            return '<div class="bg-success text-white" style="height:100%; width:100%;">-</div>';
                        }else if (value==2){
                            return '<div class="bg-danger text-white" style="height:100%; width:100%;">-</div>';
                        }
                        else{
                            return '';
                        }
                    }
                },
                {field:'result',title:'Result', align:'center', sortable:false,width: 165,
                    styler : function(value,row,index){
                        if(value == 1){
                            return {class:'some-class', style:'color:white;background-color:#ff0000;'}
                        }else if(value==2){
                            return {class:'some-class', style:'color:white;background-color:#008000;'}
                        }
                    },
                    formatter: function(value,row,index){
                        if(value == 1){
                            return '<div>Positive</div>';
                        }else if(value==2){
                            return '<div>Negative</div>';
                        }
                        else{
                            return '';
                        }
                    }
                },
                {field:'label',title:'LABEL', align:'center', sortable:false,width: 85},
                {field:'created_at',title:'Date', align:'center', sortable:true,width: 185,
                    formatter: function(value,row,index){
                        return moment(value).format("DD MMM YYYY"); 
                    }
                },
                {field:'identitas_type_id',title:'Type', align:'center', sortable:false,width: 65,
                    formatter : function(value,row,index)
                    {
                        if (row.has_identitas_type)
                        {
                            return row.has_identitas_type.label;
                        } else {
                            return value;
                        }
                        
                    }
                },
                {field:'nik',title:'NIK.', align:'center', sortable:false,width: 140},
                {field:'identitas_2',title:'Identitas 2.', align:'center', sortable:false,width: 140},
                {field:'fullname',title:'Fullname.', align:'left', sortable:false,width: 200},
                {field:'dob',title:'Date Of Birth', align:'center', sortable:false,width: 120,
                    formatter: function(value,row,index){
                        return moment(value).format("DD MMM YYYY"); 
                    }
                },
                {field:'gender',title:'Gender', align:'center', sortable:false,width: 80},
                {field:'pasien_no',title:'Patient No.', align:'center', sortable:false,width: 150},
                {field:'external_no',title:'Lab Patient No.', align:'center', sortable:false,width: 150},
                {field:'no_phone',title:'Phone / Tlpn', align:'center', sortable:false,width: 150},
                {field:'email',title:'Email', align:'center', sortable:false,width: 200},
                {field:'penjamin',title:'Penjaminan', align:'center', sortable:false,width: 150},
                {field:'inspeksi_id',title:'Pemeriksaan', align:'center', sortable:false,width: 150,
                    formatter : function(value,row,index)
                    {
                        if (row.has_inspeksi)
                        {
                            return row.has_inspeksi.name;
                        } else {
                            return value;
                        }
                    }
                },
                {field:'detail_inspeksi_id',title:'Detail', align:'center', sortable:false,width: 150,
                    formatter : function(value,row,index)
                    {
                        if (row.has_detail_inspeksi)
                        {
                            return row.has_detail_inspeksi.name;
                        } else {
                            return value;
                        }
                    }
                },
                {field:'ct_1',title:'CT 1', align:'center', sortable:false,width: 80},
                {field:'ct_2',title:'CT 2', align:'center', sortable:false,width: 80},
                {field:'swabber_date',title:'Swab Datetime', align:'center', sortable:false,width: 150,
                    formatter: function(value,row,index){
                        return moment(value).format("YYYY-MM-DD h:mm:ss"); 
                    }
                },
                {field:'swabber_name',title:'Swabber Name', align:'center', sortable:false,width: 150},
                {field:'result_receive_date',title:'Receive Datetime', align:'center', sortable:false,width: 150},
                {field:'result_analysis_date',title:'Analiysis Datetime', align:'center', sortable:false,width: 150},
                {field:'result_date',title:'Result Datetime', align:'center', sortable:false,width: 150},
                {field:'branch_id',title:'OWNER', align:'center', sortable:false,width: 120,
                    formatter : function(value,row,index)
                    {
                        if (row.has_mitra.m_name)
                        {
                            return row.has_mitra.m_name;
                        } else {
                            return value;
                        }
                    }
                },
                {field:'branch_name',title:'Branch', align:'center', sortable:false,width: 120},
                {field:'desc_nar_status',title:'Desc NAR Status', align:'center', sortable:false,width: 350,
                    formatter: function(value,row,index){
                        if(value == 1){
                            return '<span>Desc NAR VALID</span>';
                        }else{
                            return '<span>Desc NAR NOT VALID</span>';
                        }
                    }
                },
                {field:'jml_print',title:'Print', align:'center', sortable:false,width: 50},
            ]],
            
        });
    });

    
    $(function(){
        $('#filter_branch_owner').change(function() {
			
            if($('#filter_branch_owner').val() == ''){
                $('#filter_referral').val('All Referral');
                $('#filter_referral').attr('disabled', true);
            } else {
                $('#filter_referral').val($('option:selected', this).text());
                $('#filter_referral').attr('disabled', true);
            }
		});
    });

    function filterOpen(){
        // alert('a');
        var dg = $('#pcr-table').datagrid({
            pageSize: 5,
            pageList: [5, 10, 15, 20, 25],
            remoteSort:false,
        })
        var branchId = $('#filter_branch_owner').val();
        var descNarStatus = $('#filter_desc_nar_status').val();
        var order = $('#filter_order').val();
        // console.log(dg)
        $.ajax({
            url: '{{ route("pcrprosessekstraksi-filter") }}',
            type: 'POST',
            data: {
                _token : "{{ csrf_token() }}",
                branch_id : branchId,
                desc_nar_status : descNarStatus,
                order : order,
            },
            success: function(data){
                // console.log(data);
                dg.datagrid('loadData', data);
            }
        });
    }

    function resetFilter(){
        $('#pcr-table').datagrid('reload');
        $('#filter_branch_owner').val('');
        $('#filter_referral').val('');
        $('#filter_desc_nar_status').val('');
        $('#filter_order').val('');
    }

    function addPcr(){
		$('#addPcr').modal('show');
        $('#addPcr').on('hidden.bs.modal', function(e) {
            $(this).find('form').trigger('reset');
        })
	}

    function editPcr(){
        $('#editPcr').trigger('reset');
        var data = $("#pcr-table").datagrid('getSelected');
        // console.log(data);
        if(data){
            $('#editPcr').modal('show');
            $(".modal-body #patient_id").val( data.id );
            $(".modal-body #branch_id").val( data.branch_id );
            $(".modal-body #identitas_type_id").val( data.identitas_type_id );
            $(".modal-body #nik_type").val( data.nik_type );
            $(".modal-body #nik").val( data.nik );
            $(".modal-body #fullname").val( data.fullname );
            $(".modal-body #dob").val( data.dob );
            $(".modal-body #gender").val( data.gender );
            // $(".modal-body #provinsi-edit").val( data.provinsi_id );
            // $(".modal-body #provinsi-edit option:selected").val( data.provinsi_id );
            $(".modal-body #kota").val( data.kab_kota_id );
            $(".modal-body #rt").val( data.rt );
            $(".modal-body #rw").val( data.rw );
            $(".modal-body #alamat").val( data.alamat );
            $(".modal-body #no_phone").val( data.no_phone );
            $(".modal-body #email").val( data.email );
            $(".modal-body #perusahaan_name").val( data.perusahaan_name );
            $(".modal-body #inspeksi_id").val( data.inspeksi_id );
            $(".modal-body #detail_inspeksi_id").val( data.detail_inspeksi_id );
            $(".modal-body #status_swab_id").val( data.status_swab_id );
            $(".modal-body #swabber_name").val( data.swabber_name );
            $(".modal-body #swabber_date").val( data.swabber_date );
            $(".modal-body #pasien_no_edit").val( data.pasien_no );
            $(".modal-body #external_no").val( data.external_no );
            
        }else{
            $.messager.alert("Edit Patient","Select Patient first.");
        }
    }

    function deletePcr(){
        var sel = $("#pcr-table").datagrid('getSelected');
        var check = $("#pcr-table").datagrid('getChecked');
        
        if(sel){
            var patientIdList = [];
            patientIdList.push(sel.id);

            var message_confirm = "Delete Selected data ?";

            execute_delete(patientIdList,message_confirm);

            } else if(check.length>0) {
                var patientIdList = [];

                check.forEach(function(value, index){
                    patientIdList.push(value.id);
            });        

            var message_confirm = "Delete Checklist data ?";

            execute_delete(patientIdList,message_confirm);

            } else {
                alert("Please select or checklist data!");
            }
    }

    function doSearch(val){
       
       var dg = $('#pcr-table').datagrid({
           pageSize: 5,
           pageList: [5, 10, 15, 20, 25],
           remoteSort:false,
       });
       $.ajax({
           url: '{{ route("pcrprosessekstraksi-table") }}',
           type: 'POST',
           data: {
               _token : "{{ csrf_token() }}",
               cari : val,

           },
           success: function(data){
               // console.log(data);
               // console.log(val);
               // prevQueryParams = dg.datagrid('options')['queryParams'];
               // newQueryParams = $.extend(prevQueryParams, { cari: val} );

               // dg.datagrid('loadData', newQueryParams);
               dg.datagrid('loadData', data);
           }
       });
      

    }

    
    $(document).ready(function(){
        var msg = '{{Session::get('sukses')}}';
        var exist = '{{Session::has('sukses')}}';
        if(exist){
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: msg,
                showConfirmButton: false,
                timer: 2500
            })
        }

    })
    $(function(){
        $('#filter_mitra_owner').change(function() {
			
            if($('#filter_mitra_owner').val() == ''){
                $('#filter_branch_owner').val('All Branch');
            } else {
                $("img#load1").show();
                var mitra_id = $(this).val();  
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: "{{route('selectbranch')}}",
                    data: "mitra_id="+mitra_id,
                    success: function(res){
                        $("select#filter_branch_owner").html(res);                                                       
                        $("img#load1").hide();                                                      
                    }
                });    
            }
		});
    });

    function _jf_102_grid02_send_selesaiekstraksi(){
        var sel = $("#pcr-table").datagrid('getSelected');
        var check = $("#pcr-table").datagrid('getChecked');
        if(sel){
        var patientIdList = [];
        patientIdList.push(sel.id);
        var message_confirm = "Finish Ekstraksi Selected data ?";
        execute_send_selesaiekstraksi(patientIdList,message_confirm);
        }else if(check.length>0){
        var patientIdList = [];
        check.forEach(function(value, index){
            patientIdList.push(value.id);
        });        
        var message_confirm = "Finish Ekstraksi Checklist data ?";
        execute_send_selesaiekstraksi(patientIdList,message_confirm);
        }else{
        alert("Please select or checklist data!");
        }
    }

    function execute_send_selesaiekstraksi(patientIdList="", message_confirm=""){
        if(patientIdList!=""){
        $.messager.confirm("Send Sample", message_confirm,function(r){
            if(r){
            preloader_block();
            $.ajax({
                type: 'POST',
                dataType: "JSON",
                url:"{{ url('api/pcrekstraksi/send_to_finishekstraksi?id=')}}"+JSON.stringify(patientIdList),
                success: function(r) {
                $.messager.alert("Finish Ekstraksi Sample Patient", r.message);
                if (r.success) {
                    $("#pcr-table").datagrid('reload');
                    $("#pcr-table").datagrid('clearSelections');
                    $("#pcr-table").datagrid('clearChecked');
                }
                preloader_none();
                },
                error:function(){
                $.messager.alert("Finish Ekstraksi Sample Patient", "Request error, please try again.");
                preloader_none();
                }
            })
            }
        })
        }else{
        alert("Please select or checklist data!");
        }
    }
    function _jf_102_grid02_send_diterima(){
        var sel = $("#pcr-table").datagrid('getSelected');
        var check = $("#pcr-table").datagrid('getChecked');
        if(sel){
        var patientIdList = [];
        patientIdList.push(sel.id);
        var message_confirm = "Send Selected data ?";
        execute_send_diterima(patientIdList,message_confirm);
        }else if(check.length>0){
        var patientIdList = [];
        check.forEach(function(value, index){
            patientIdList.push(value.id);
        });        
        var message_confirm = "Send Checklist data ?";
        execute_send_diterima(patientIdList,message_confirm);
        }else{
        alert("Please select or checklist data!");
        }
    }

    function execute_send_diterima(patientIdList="", message_confirm=""){
        if(patientIdList!=""){
        $.messager.confirm("Send Sample", message_confirm,function(r){
            if(r){
            preloader_block();
            $.ajax({
                type: 'POST',
                dataType: "JSON",
                url:"{{ url('api/pcrekstraksi/send_to_diterima?id=')}}"+JSON.stringify(patientIdList),
                success: function(r) {
                $.messager.alert("Send Sample Patient To Entry", r.message);
                if (r.success) {
                    $("#pcr-table").datagrid('reload');
                    $("#pcr-table").datagrid('clearSelections');
                    $("#pcr-table").datagrid('clearChecked');
                }
                preloader_none();
                },
                error:function(){
                $.messager.alert("Send Sample Patient To Entry", "Request error, please try again.");
                preloader_none();
                }
            })
            }
        })
        }else{
        alert("Please select or checklist data!");
        }
    }
</script>

@endpush