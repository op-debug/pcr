@extends('app')

@section('content')
<section id="interpretasi">
  <div class="row match-height">
      <div class="col-xl-12 col-md-6 col-12">
        <div class="easyui-layout" data-options="fit:true" style="width: 1237px; height: 304px;">
          <div data-options="region:'center', border:false, split:true">
            <div class="easyui-layout" data-options="fit:true">
              <div data-options="region:'center', split:true, title:'List Interpretasi'" >
                <table id="_interpretasi_grid"
                url="{{url('api/interpretasi/getdata')}}",>
                </table>
                  <div id="_interpretasi_grid_toolbar" class="p-1">
                      <div class="row">
                        <div class="col-12">
                          
                          <div style="float: left">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_interpretasi_grid_edit()"><i class="fa fa-edit fa-fw fa-lg" style="color:green"></i>&nbsp;Edit</a>
                          </div>
                        </div>
                      </div>
              </div>
          </div>
        </div>
    </div>
  </div>

    <div id="_interpretasi_grid_dialog" class="easyui-dialog" style="width:20%;height:220px;padding:10px 25px"
          closed="true" buttons="#dlg-buttons">
          <form method="post" action="{{url('/interpretasi/import_excel')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Pilih file excel</label>
                <input type="file" name="file" required="required">
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </form>
    </div>


    <div id="_interpretasi_edit_dialog" class="easyui-dialog" style="width:20%;height:220px;padding:10px 25px"
        closed="true" buttons="#dlg-buttons">
      <form id="fme" method="post"  novalidate>
          <div class="form-group">
              <label>Lab. Patient No</label>
              <select id="filter_patient" class="form-control" name="id_sample">
                <option value="">Pilih No Lab.</option>
              <select>
          </div>
      </form>
    </div>
    <div id="dlg-buttons">
      <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveinterpretasi()" style="width:90px">Save</a>
  </div>

</section>
<script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script>

<script type="text/javascript">
  var url;
  $(function(){

    $("#_interpretasi_grid").datagrid({
      toolbar: "#_interpretasi_grid_toolbar",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "id",
      frozenColumns: [[
        {field: "check",checkbox:true},
      ]],
      columns: [[
        {field: "hole",title: "<b>Hole</b>",align: "left",width: 50},
        {field: "id_sample",title: "<b>Lab. Patient No.</b>",align: "left",width: 150},
        {field: "pos_status",title: "<b>POS STATUS</b>",align: "left",width: 185,
          formatter: function(value,row,index){
              if(value == 'false'){
                  return '<div class="bg-danger px-5 text-white" style="height:100%; width:100%;">INVALID</div>';
              }else if(value == 'true'){
                  return '<div class="bg-success px-5 text-white" style="height:100%; width:100%;">VALID</div>';
              }
              else{
                return'';
              }
          }
        },
        {field: "ntc_status",title: "<b>NTC STATUS</b>",align: "left",width: 185,
          formatter: function(value,row,index){
              if(value == 'false'){
                  return '<div class="bg-danger px-5 text-white" style="height:100%; width:100%;">INVALID</div>';
              }else if(value == 'true'){
                  return '<div class="bg-success px-5 text-white" style="height:100%; width:100%;">VALID</div>';
              }
              else{
                return'';
              }
          }
        },
        {field: "fam",title: "<b>FAM</b>",align: "left",width: 85},
        {field: "hex",title: "<b>HEX</b>",align: "left",width: 85},
        {field: "rox",title: "<b>ROX</b>",align: "left",width: 85},
        {field: "sample_status",title: "<b>Sample STATUS</b>",align: "left",width: 135}
      ]],
    })
  })


  function _jf_interpretasi_grid_import(){
    $('#_interpretasi_grid_dialog').dialog('open').dialog('setTitle','Import interpretasi');
    $('#fm').form('clear');
    url= "{{url('api/interpretasi/store')}}";
  }
   
  
  function _jf_interpretasi_grid_edit(){
    var row = $('#_interpretasi_grid').datagrid('getSelected');

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "{{route('getpatient')}}",
        success: function(res){
            $("select#filter_patient").html(res);                                                                                                          
        }
    }); 

		if (row){
      $('#_interpretasi_edit_dialog').dialog('open').dialog('setTitle','Edit interpretasi');
      $('#fme').form('load',row);
      url = "{{url('api/interpretasi/update')}}/"+row.id;
    }
    else{
      $.messager.alert("Edit interpretasi","Select Data first.");
    }
  }

  function saveinterpretasi(){
    $.ajax({
        type: "POST",
        url: url,
        data:$('#fme').serialize(),
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#_interpretasi_edit_dialog').dialog('close'); 
                $('#_interpretasi_grid').datagrid('reload');   
            }
        }
    });
  }

  function _jf_interpretasi_grid_delete(){
    var sel = $("#_interpretasi_grid").datagrid('getSelected');
    var check = $("#_interpretasi_grid").datagrid('getChecked');
    if(sel){

      var patientIdList = [];
      patientIdList.push(sel.id);
      var message_confirm = "Delete Selected data ?";
      execute_delete(patientIdList,message_confirm);

    }else if(check.length>0){
      var patientIdList = [];
      check.forEach(function(value, index){
        patientIdList.push(value.id);
      });        
      var message_confirm = "Delete Checklist data ?";
      execute_delete(patientIdList,message_confirm);
    }else{
      alert("Please select or checklist data!");
    }
	}
 

  function execute_delete(patientIdList="", message_confirm=""){
    if(patientIdList!=""){
      $.messager.confirm("Delete Patient", message_confirm,function(r){
        if(r){
          preloader_block();
          $.ajax({
            type: 'DELETE',
            dataType: "JSON",
            url:"{{url('api/interpretasi/delete?id=')}}"+JSON.stringify(patientIdList),
            success: function(r) {
              $.messager.alert("Delete", r.message);
              if (r.success) {
                $("#_interpretasi_grid").datagrid('reload');
                $("#_interpretasi_grid").datagrid('clearSelections');
                $("#_interpretasi_grid").datagrid('clearChecked');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Delete Patient", "Request error, please try again.");
              preloader_none();
            }
          })
        }
      })
    }else{
      alert("Please select or checklist data!");
    }
  }
  function _jf_prosess_validasi(){
    var message_confirm = "Validasi Data data ?";
    $.messager.confirm("Validasi Data?", message_confirm,function(r){
      if(r){
        preloader_block();
          $.ajax({
            type: 'GET',
            dataType: "JSON",
            url:"{{url('api/interpretasi/validasi')}}",
            success: function(r) {
              $.messager.alert("Validasi", r.message);
              if (r.success) {
                $("#_interpretasi_grid").datagrid('reload');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Validasi", "Request error, please try again.");
              preloader_none();
            }
          })
      }
    })

  }
</script>
@endsection

