@extends('app')

@section('content')
<section id="branch">
  <div class="row match-height">
      <div class="col-xl-12 col-md-6 col-12">
        <div class="easyui-layout" data-options="fit:true" style="width: 1237px; height: 154px;">
          <div data-options="region:'center', border:false, split:true">
            <div class="easyui-layout" data-options="fit:true">
              <div data-options="region:'center', split:true, title:'List Branch'" >
                <table id="_branch_grid"
                url="{{url('api/branch/getdata')}}",>
                </table>

                  <div id="_branch_grid_toolbar" class="p-1">
                      <div class="row">
                        <div class="col-12">
                          
                          <div style="float: left">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_branch_grid_add()"><i class="fa fa-plus fa-fw fa-lg" style="color:green"></i>&nbsp;Add</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_branch_grid_edit()"><i class="fa fa-edit fa-fw fa-lg" style="color:green"></i>&nbsp;Edit</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_branch_grid_delete()"><i class="fa fa-trash fa-fw fa-lg" style="color:green"></i>&nbsp;Delete</a>
                          </div>

                        </div>
                      </div>
              </div>
          </div>
        </div>
    </div>
  </div>

  <div id="_branch_grid_dialog" class="easyui-dialog" style="width:50%;height:420px;padding:10px 25px"
        closed="true" buttons="#dlg-buttons">
    <form id="fm" method="post"  novalidate>
        <div  class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Mitra</label>
            <select name="mitra_id" id="mitra_id"
									class="form-control {{$errors->has('mitra_id') ? 'validatebox-invalid' : '' }}">
                  <option value="">-Pilih-</option>
									@foreach ($mitras as $mitra)
									<option value="{{$mitra->id}}" {{ old('mitra_id')==$mitra->id ?
										'selected="selected"': '' }}>{{$mitra->m_name}}</option>
									@endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Nama Branch</label>
            <input type="text" name="b_name" class="form-control" required="true">
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Email</label>
            <input type="email" name="b_email" class="form-control" validType="email">
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Telp</label>
            <input name="b_telp" class="form-control">
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Alamat</label>
            <textarea name="b_alamat" class="form-control" ></textarea>
        </div>
    </form>
  </div>

  <div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savebranch()" style="width:90px">Save</a>
  </div>

  <div id="_branch_grid_dialog_edit" class="easyui-dialog" style="width:50%;height:420px;padding:10px 25px"
        closed="true" buttons="#dlg-buttons-edit">
    <form id="fme" method="post"  novalidate>
        <div  class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Mitra</label>
            <select name="mitra_id" id="mitra_id"
									class="form-control {{$errors->has('mitra_id') ? 'validatebox-invalid' : '' }}">
                  <option value="">-Pilih-</option>
									@foreach ($mitras as $mitra)
									<option value="{{$mitra->id}}" {{ old('mitra_id')==$mitra->id ?
										'selected="selected"': '' }}>{{$mitra->m_name}}</option>
									@endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Nama Branch</label>
            <input type="text" name="b_name" class="form-control" required="true">
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Email</label>
            <input type="email" name="b_email" class="form-control" validType="email">
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Telp</label>
            <input name="b_telp" class="form-control">
        </div>
        <div class="form-group">
            <label class="col-lg-12 col-form-label pb-1">Alamat</label>
            <textarea name="b_alamat" class="form-control" ></textarea>
        </div>
    </form>
  </div>

  <div id="dlg-buttons-edit">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savebranchedit()" style="width:90px">Save</a>
  </div>
</section>
<script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script>

<script type="text/javascript">
  var url;
  $(function(){
    $("#_mitra_grid_filter_nar_status_front").combobox({
        prompt:"Status NAR",
        editable:false,
        panelHeight:"auto",
        valueField:"f_nar_status",
        textField:"f_status_name",
        data:[
          {
            f_status_name: '-- ALL STATUS NAR--', f_nar_status: 'sAll',selected:'true'
          },
          {
            f_status_name: 'VALID', f_nar_status: '1'
          },
          {
            f_status_name: 'NOT VALID', f_nar_status: '0'
          },
        ],
        onChange:function(newValue, oldValue){
            $("#_mitra_grid_filter_nar_status").combobox('setValue',newValue);
           // _jf_pcr_grid_filter_p();
        }
    })
    $("#_branch_grid").datagrid({
      toolbar: "#_branch_grid_toolbar",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "id",
      
      columns: [[
        {field: "b_name",title: "<b>Nama Branch</b>",align: "left",width: 200},
        {field: "mitra",title: "<b>Mitra</b>",align: "left",width: 200},
        {field: "b_email",title: "<b>Email</b>",align: "left",width: 225},
        {field: "b_telp",title: "<b>No. Telp</b>",align: "left",width: 240},
        {field: "b_alamat",title: "<b>Alamat</b>",align: "left",width: 385},
      ]],
    })
  })

  function _jf_pcr_grid_filter_g(){
        var narStatus = $("#_mitra_grid_filter_nar_status").combobox('getValue');
        $("#_mitra_grid_filter_nar_status_front").combobox('setValue',narStatus);

        var filterParams = $("#_mitra_grid_filter_f").serialize();
        // filterParams += "";
        return filterParams;
    }
 

    function _jf_mitra_grid_filter_r(){
        $("#_mitra_grid_filter_nar_status").combobox('setValue','sAll');
        _jf_mitra_grid_filter_p();
    }

  function _jf_branch_grid_add(){
    $('#_branch_grid_dialog').dialog('open').dialog('setTitle','New Branch');
    $('#fm').form('clear');
    url= "{{url('api/branch/store')}}";
  }
   
  
  function _jf_branch_grid_edit(){
    var row = $('#_branch_grid').datagrid('getSelected');
		if (row){
      $('#_branch_grid_dialog_edit').dialog('open').dialog('setTitle','Edit Branch');
      $('#fme').form('load',row);
      url = "{{url('api/branch/update')}}/"+row.id;
    }
    else{
      $.messager.alert("Edit Branch","Select Branch first.");
    }
  }

  function savebranch(){
    $.ajax({
        type: "POST",
        url: url,
        data:$('#fm').serialize(),
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#_branch_grid_dialog').dialog('close'); 
                $('#_branch_grid').datagrid('reload');   
            }
        }
    });
  }

  function savebranchedit(){
    $.ajax({
        type: "POST",
        url: url,
        data:$('#fme').serialize(),
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#_branch_grid_dialog_edit').dialog('close'); 
                $('#_branch_grid').datagrid('reload');   
            }
        }
    });
  }

  function _jf_branch_grid_delete(){
			var row = $('#_branch_grid').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this branch?',function(r){
					if (r){
						$.post("{{url('api/branch/delete')}}",{id:row.id},function(result){
							if (result.success){
								$('#_branch_grid').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							}
						},'json');
					}
				});
			}
		}
 
</script>
@endsection

