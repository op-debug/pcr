@extends('app')

@section('content')
<section id="branch">
  <div class="card">
    <div class="card-body">
      <h1>Setting Program</h1>
      <form method="post" action="{{route('setting-save')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        
        <div class="form-group">
          <label>Nama Program</label>
          <input type="text" name="name" value="{{ $setting['name'] }}" class="form-control" required="true">
        </div>
        
        <div class="form-group">
          <label>Copyright</label>
          <input type="text" name="copy" value="{{ $setting['copy'] }}" class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Max Analize PCR</label>
          <input type="number" name="analize_max" value="{{ $setting['max_analize_pcr'] }}" class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Lab Patient No</label>
          <div class="row">
            <div class="col-5">
              <input type="text" name="lab_patient_no" value="{{ $setting['lab_patient_no'] }}" class="form-control" required="true">
            </div>
            <div class="col-5">
              <input type="number" name="run_nolab" value="{{ $setting['run_nolab'] }}" class="form-control" required="true">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label>Patient No</label>
          <div class="row">
            <div class="col-5">
              <input type="text" name="mitra" value="{{ $setting['mitra'] }}" class="form-control" required="true">
            </div>
            <div class="col-5">
              <input type="text" name="run_mitra" value="{{ $setting['run_mitra'] }}" class="form-control" required="true">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label>No Registrasi Lab</label>
          <input type="text" name="noregisterlab" value="{{ $setting['no_registrasi_lab'] }}" class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Dokter Penanggung Jawab</label>
          <input type="text" name="dokterpenanggungjawab" value="{{ $setting['dokter_penanggungjawab'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>SIP NO</label>
          <input type="text" name="sipno" value="{{ $setting['sip_no'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Alamat</label>
          <input type="text" name="alamat" value="{{ $setting['alamat'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" value="{{ $setting['phone'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Title Footer</label>
          <input type="text" name="title_footer" value="{{ $setting['title_footer'] }}"  class="form-control" required="true">
        </div>


        <div class="form-group">
          <label>SMTP/Host</label>
          <input type="text" name="host" value="{{ $setting['smpt'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>PORT</label>
          <input type="text" name="port" value="{{ $setting['port'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Mail Encryption</label>
            <select class="form-control" name="encryption" >
              <option value="ssl" @if($setting['encryption']=="ssl") selected @endif>SSL</option>
              <option value="tls"  @if($setting['encryption']=="tls") selected @endif>TLS</option>
            </select>
        </div>


        <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" value="{{ $setting['email'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" value="{{ $setting['password'] }}"  class="form-control" required="true">
        </div>


        <div class="form-group">
          <label>Title Email</label>
          <input type="text" name="title" value="{{ $setting['title'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Subject Email</label>
          <input type="text" name="subject" value="{{ $setting['subject'] }}"  class="form-control" required="true">
        </div>

        <div class="form-group">
          <img src="public/img/{{ $setting['favicon'] }}" width="100px" height="100px">
          <br/>
          <label>Favicon</label>
          <input type="file" name="favicon" class="form-control">
        </div>

        <div class="form-group">
          <img src="public/img/{{ $setting['logo'] }}" width="100px" height="100px">
          <br>
          <label>Logo</label>
          <input type="file" name="logo"  class="form-control">
        </div>

        <div class="form-group">
          <img src="public/img/{{ $setting['logolab'] }}" width="100px" height="100px">
          <br/>
          <label>Logo Lab</label>
          <input type="file" name="logolab" class="form-control" >
        </div>

        <div class="form-group">
          <img src="public/img/{{ $setting['ttd_img'] }}" width="100px" height="100px">
          <br/>
          <label>TTD Print PCR</label>
          <input type="file" name="ttd_img" class="form-control" >
        </div>

        <div class="modal-footer">
          <button href="javascript:void(0)" class="btn btn-block btn-md btn-primary"  style="width:100px;" id="btnSubmit"
            type="submit">Save</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection

