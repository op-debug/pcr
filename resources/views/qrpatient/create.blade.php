<div class="modal fade" id="qrpatientModal" tabindex="-1" aria-labelledby="qrpatientModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="qrpatientModalLabel">Generate QR Code (VTM)</h5>
				<button type="button" class="btn-close btn-secondary" data-bs-dismiss="modal"
					aria-label="Close">X</button>
			</div>

			<div class="modal-body">
				<div class="modal-content">

					<form id="form-qrpatientModal" method="POST" action="{{route('qrpatient-store')}}" enctype="multipart/form-data">
						@csrf
						<div class="form-group row mb-1">
							<label class="col-lg-12 col-form-label pb-1">Branch</label>
							<div class="col-lg-12">
							<select name="branch_id" 
								class="form-control">
								@foreach ($branchs as $branch)
								<option value="{{$branch->id}}">{{$branch->b_name}}</option>
								@endforeach
							</select>
							@error('branch_id') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
						</div>
                        <div class="form-group row mb-1">
                            <label class="col-lg-12 col-form-label pb-1">Generate Total</label>
                            <div class="col-lg-12">
                                <input type="number" value="1" id="generatetotal" class="form-control">
                            </div>
                        </div>
				
						<div class="modal-footer">
							<a href="javascript:void(0)" class="btn btn-block btn-md btn-primary" iconCls="icon-ok" onclick="simpan()" style="width:90px">Save</a>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
@push('scripts')

@endpush