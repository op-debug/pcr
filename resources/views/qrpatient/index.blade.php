@extends('app')

@section('content')
<section id="qrpatient">
	<div class="row match-height">
		<div class="col-xl-12 col-md-12 col-12">
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Opps Something went wrong</strong>
                <button type="button" class="btn-close btn-secondary" data-bs-dismiss="alert" aria-label="Close">X</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
              </div>
            @endif
            
            @include('qrpatient.table')
            
        </div>
    </div>
</section>

<style>
    .select2-selection__arrow b{
        display:none !important;
    }
</style>

@include('qrpatient.create')
@include('qrpatient.printqr')

@endsection

@push('scripts')
<script>
    $(function(){
        var dg = $('#qrpatient-table').datagrid();
        dg.datagrid({
            toolbar: "#qrpatient-table-toolbar",
            pageList: [5, 10, 15, 20, 25],			
			remoteSort:false,
            pageSize: 10,
            striped: true,
            pagination: true,
            fit: true,
            fitColumns: false,
            rownumbers: true,
            singleSelect: true,
            selectOnCheck: false,
            checkOnSelect: false,
            method: "POST",
            columns:[[
                {field:'id_patient',title:'Patient Number', align:'center', sortable:false, width:200},
                {field:'print',title:'Print', align:'center', sortable:false, width:100},
                {field:'print_by',title:'Print By', align:'center', sortable:false, width:100},
                {field:'print_time',title:'Print Time', align:'center', sortable:false, width:100},
                {field:'use',title:'Use', align:'center', sortable:false, width:100},
                {field:'use_by',title:'Use By', align:'center', sortable:false, width:100},
                {field:'use_time',title:'Use Time', align:'center', sortable:false, width:100},
                {field:'created_by',title:'Create By', align:'center', sortable:false, width:120},
                {field:'created_time',title:'Create Time', align:'center', sortable:false, width:180,
                    formatter: function(value,row,index){
                        return moment(value).format("DD-MM-YYYY h:mm:ss"); 
                    }
                },
            ]],
            
        });
    });

    function simpan(){
		$.ajax({
            type: 'POST',
			data:$('#form-qrpatientModal').serialize(),
            url: "{{route('qrpatient-store')}}",
            success: function(res) {
				if (res.success) {
					Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
					$('#qrpatientModal').modal('hide');
					$("#qrpatient-table").datagrid('reload');
                    $("#qrpatient-table").datagrid('clearSelections');
                    $("#qrpatient-table").datagrid('clearChecked');
				}
			},
            error:function(err){
				if (err.status == 422) { // when status code is 422, it's a validation issue
					console.log(err.responseJSON);
					$('#success_message').fadeIn().html(err.responseJSON.message);
					
					// you can loop through the errors object and show it to the user
					console.warn(err.responseJSON.errors);
					// display errors on each form field
					$.each(err.responseJSON.errors, function (i, error) {
						var el = $(document).find('[name="'+i+'"]');
						el.after($('<span style="color: red;">'+error[0]+'</span>'));
					});
				}
            }
        })
	}

    function qrCode(){
        $('#qrpatientModal').trigger('reset');
        $('#qrpatientModal').modal('show');    
    }

    function printqr(){
        $('#printqrModal').trigger('reset');
        $('#printqrModal').modal('show');    
    }
</script>

@endpush