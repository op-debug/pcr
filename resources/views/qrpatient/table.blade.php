<div class="row">
    <div class="col-12">
        <table id="qrpatient-table" title="QR Patient No" class="easyui-datagrid" url="{{route('qrpatient-table')}}" toolbar="#toolbar"
            pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" style="width:100%;height:550px;">
        </table>
        
        <div id="qrpatient-table-toolbar" class="p-1">
            <div class="row">
                
                <div class="col-12"> 
                    <div style="float: left">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="qrCode()"><i
                            class="fa fa-plus fa-fw fa-lg" style="color:green"></i>&nbsp;Generate New Number</a>
                        <a href="#" class="easyui-linkbutton" data-options="plain:true"
                            onclick="printqr()"><i class="fa fa-file-excel-o fa-fw fa-lg"
                                style="color:green"></i>&nbsp;Print</a>
                    </div>

                    
                        
                </div>
               
            </div>
        </div>

        
        
    </div>
</div>