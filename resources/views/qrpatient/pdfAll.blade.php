<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <link rel="shortcut icon" type="image/x-icon" href="{{ get_favicon()}}">
    <link rel="apple-touch-icon" href="{{ get_favicon()}}">
</head>

<body>
<?php

foreach ($result as $key => $value) {
    echo '<table>
                <tr><td><img src="data:image/png;base64, '. $value['qrpatient'] .'"></td>
                <td width="100px" align="center">'.$value['id_patient'].'</td></tr>';
    echo '<tr><td align="center">'.$value['id'].'</td></tr></table>';
}
?>

</body>

</html>