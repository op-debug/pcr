<div class="modal fade" id="printqrModal" tabindex="-1" aria-labelledby="printqrModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="printqrModalLabel">Print QR Code (VTM)</h5>
				<button type="button" class="btn-close btn-secondary" data-bs-dismiss="modal"
					aria-label="Close">X</button>
			</div>

			<div class="modal-body">
				<div class="modal-content">

					<form id="form-printqrModal" method="GET" target="_blank" action="{{route('qr-pdfAll')}}" enctype="multipart/form-data">
						@csrf
						<div class="form-group row mb-1">
							<label class="col-lg-12 col-form-label pb-1">Clinic Branch</label>
							<div class="col-lg-12">
							<select name="branch_id"
								class="form-control">
								@foreach ($branchs as $branch)
								<option value="{{$branch->id}}">{{$branch->b_name}}</option>
								@endforeach
							</select>
							@error('branch_id') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
						</div>
						<div class="form-group row mb-1">
							<label class="col-lg-12 col-form-label pb-1">Print Mode</label>
							<div class="col-lg-12">
								<select name="print_mode" 
									class="form-control">
									<option value="1">Not-Print & Not-Use</option>
									<option value="2">Not-Use</option>
								</select>
							</div>
						</div>
						<div class="form-group row mb-1">
							<label class="col-lg-12 col-form-label pb-1">Label Size</label>
							<div class="col-lg-12">
								<select name="label_size" 
									class="form-control">
									<option value="1">Label TJ 103</option>
								</select>
							</div>
						</div>
                        <div class="form-group row mb-1">
                            <label class="col-lg-12 col-form-label pb-1">Print Total</label>
                            <div class="col-lg-12">
                                <input type="number" name="jlm" value="1" id="generatetotal" class="form-control">
                            </div>
                        </div>
				
						<div class="modal-footer">
							<button href="javascript:void(0)" class="btn btn-block btn-md btn-primary"
								type="submit">Simpan</button>

						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
@push('scripts')

@endpush