@extends('app')

@section('content')
<section id="setting_user">
  <div class="card">
    <div class="card-body">
      <h1>Setting User</h1>
      @if(Session::has('error'))
          <div class="alert alert-danger text-center">
              <strong>{{ session('error') }}</strong>
          </div>
      @endif
      <form method="post" action="{{route('setting_user_save')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        
        <div class="form-group">
          <label>Nama</label>
          <input type="text" name="name" value="{{ $data['name'] }}" class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" value="{{ $data['email'] }}" class="form-control" required="true">
        </div>

        <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" value="{{ $data['pass'] }}" class="form-control" required="true">
        </div>

        <div class="form-group">
          <img src="public/img/{{ $data['gambar'] }}" width="200px" height="200px">
          <br/>
          <label>gambar</label>
          <input type="file" name="gambar" class="form-control">
        </div>

        <div class="modal-footer">
          <button href="javascript:void(0)" class="btn btn-block btn-md btn-primary"  style="width:100px;"
            type="submit">Save</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection

