@extends('app')

@section('content')
<section id="dashboard-ecommerce">
  <center><h1 class="h3 mb-0 text-gray-800">Selamat Datang di <span style="color:#7367F0">{{  get_name()}}</span><h1></center>
  @if (get_data_login()->role=="admin" || get_data_login()->role=="labpcr" || get_data_login()->role=="labekstraksi")
    <div class="row match-height">
      <!-- Statistics Card -->
      <div class="col-xl-12 col-md-6 col-12">
        <div class="card card-statistics">
          <div class="card-body statistics-body">
            <div class="row">
              <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                <div class="media">
                  <div class="avatar bg-light-primary mr-2">
                    <div class="avatar-content">
                      <i data-feather="user" class="avatar-icon"></i>
                    </div>
                  </div>
                  <div class="media-body my-auto">
                    <h4 class="font-weight-bolder mb-0">{{ $total_patient[0]->jumlah }}</h4>
                    <p class="card-text font-small-3 mb-0">TOTAL PATIENT SUBMITED</p>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                <div class="media">
                  <div class="avatar bg-light-info mr-2">
                    <div class="avatar-content">
                      <i data-feather="user" class="avatar-icon"></i>
                    </div>
                  </div>
                  <div class="media-body my-auto">
                    <h4 class="font-weight-bolder mb-0">{{ $total_process[0]->jumlah }}</h4>
                    <p class="card-text font-small-3 mb-0">TOTAL SAMPLE PROCESS</p>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                <div class="media">
                  <div class="avatar bg-light-success mr-2">
                    <div class="avatar-content">
                      <i data-feather="box" class="avatar-icon"></i>
                    </div>
                  </div>
                  <div class="media-body my-auto">
                    <h4 class="font-weight-bolder mb-0">{{ $total_negative[0]->jumlah }}</h4>
                    <p class="card-text font-small-3 mb-0">TOTAL NEGATIVE</p>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 col-12">
                <div class="media">
                  <div class="avatar bg-light-danger mr-2">
                    <div class="avatar-content">
                      <i data-feather="box" class="avatar-icon"></i>
                    </div>
                  </div>
                  <div class="media-body my-auto">
                    <h4 class="font-weight-bolder mb-0">{{ $total_positive[0]->jumlah }}</h4>
                    <p class="card-text font-small-3 mb-0">TOTAL POSITIVE</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/ Statistics Card -->
      <div class="col-xl-12 col-md-6 col-12">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" >
                <canvas id="myChart" width="200" height="80"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card-body" style="height: 10px">
                    <canvas id="chart-line" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body" style="height: 10px">
                    <canvas id="chart-line2" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>
    </div>
  @endif
</section>

@endsection

@push('scripts')
<script src="{{ asset('public/chart.min.js')}}"></script>
<script>
      window.onload = function () {
        //Yearly Income Vs Expense
        var link = "{{ url('api/dashbord/json_grafik')}}";
        $.ajax({
            url: link,
            success: function (data) {
                var json = JSON.parse(data);
                var ctx = document.getElementById("myChart").getContext('2d');
                var myChart = new Chart(ctx, {
                  type: 'bar',
                  data: {
                      labels: json['Months'],
                      datasets: [{
                          label: 'Positive',
                          data: json['positive'],
                          fill: false,
                          backgroundColor:'#EA5455',
                          borderColor: '#EA5455',
                          tension: 0.1
                          },
                          {
                          label: 'Negative',
                          data: json['negative'],
                          fill: false,
                          backgroundColor:'#28C76F',
                          borderColor: '#28C76F',
                          tension: 0.1
                          },
                  ]
                  },
                  options: {
                      scales: {
                          yAxes: [{
                              ticks: {
                                  beginAtZero:true
                              }
                          }]
                      }
                  }
                });

              }
        });

      }
   
</script>

@endpush