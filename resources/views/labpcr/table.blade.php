<div class="row">
    <div class="col-12">
        <table id="pcr-table" title="Data Finish Ekstraksi" class="easyui-datagrid" url="{{url('labpcr-table')}}" toolbar="#toolbar"
            pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" style="width:100%;height:550px;">
        </table>
        
        <div id="pcr-table-toolbar" class="p-1">
            <div class="row">
                
                <div class="col-12"> 
                    <div style="float: left">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-undo'" onclick="_jf_102_grid02_send_ekstraksi()">&nbsp;Send Ekstraksi</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-redo'" onclick="_jf_102_grid02_send_analize()">&nbsp;Sample To Analize PCR</a>
                    </div>

                    <div style="float:right">
                        <select name="mitra_id" id="filter_mitra_owner" style="width: 150px; height: 25px;">
                            <option value="">All Mitra</option>
                            @foreach ($mitras as $mitra)
                                <option value="{{$mitra->id}}">{{$mitra->m_name}}</option>
                            @endforeach
                        </select>
                        <select name="branch_id" id="filter_branch_owner" style="width: 150px; height: 25px;">
                            <option value="">All Branch</option>
                        </select>
                        <select name="desc_nar_status" id="filter_desc_nar_status" style="width: 120px; height: 25px;">
                            <option value="">All Status NAR</option>
                            <option value="1">Valid</option>
                            <option value="0">Not Valid</option>
                        </select>

                        <select style="width: 120px; height: 25px;">
                            <option value="">All</option>
                            {{-- <option value="1">Valid</option>
                            <option value="0">Not Valid</option> --}}
                        </select>
                        <select name="filer_order" id="filter_order" style="width: 120px; height: 25px;">
                            <option value="">All Data</option>
                            <option value="1">Last Data</option>
                            <option value="2">New Data</option>
                        </select>
                        <div class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-filter'" onclick="filterOpen()">Terapkan Filter</div>
                    </div>
                        
                </div>
               
            </div>
        </div>

        
        
    </div>
</div>