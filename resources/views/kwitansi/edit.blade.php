<div class="modal fade" id="editKwitansi" tabindex="-1" aria-labelledby="editKwitansiLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="editKwitansiLabel">Edit Kwitansi</h5>
				<button type="button" class="btn-close btn-secondary" data-bs-dismiss="modal"
					aria-label="Close">X</button>
			</div>

			<div class="modal-body">
				<div class="modal-content">

					<form id="form-editKwitansi" enctype="multipart/form-data">
						@csrf
						<input type="hidden" id="id_kwitansi" name="id">
						<div class="pt-2 pb-2 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Nomor Recipt</label>
								<div class="col-lg-12">
									<input type="text" name="no_recipt" value="{{old('no_recipt')}}" id="no_recipt"
										class="form-control {{$errors->has('no_recipt') ? 'validatebox-invalid' : '' }}"
										placeholder="no recipt">
									@error('no_recipt') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Booking Recipt No</label>
								<div class="col-lg-12">
									<input type="text" name="booking_recipt_no" value="{{old('booking_recipt_no')}}" id="booking_recipt_no"
										class="form-control {{$errors->has('booking_recipt_no') ? 'validatebox-invalid' : '' }}"
										placeholder="Booking Recipt No">
									@error('booking_recipt_no') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Payment</label>
								<div class="col-lg-12">
									<input type="text" name="payment" value="{{old('payment')}}" id="payment"
										class="form-control {{$errors->has('payment') ? 'validatebox-invalid' : '' }}"
										placeholder="Payment">
									@error('payment') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Name</label>
								<div class="col-lg-12">
									<input type="text" name="name" value="{{old('name')}}" id="name"
										class="form-control {{$errors->has('name') ? 'validatebox-invalid' : '' }}"
										placeholder="Name">
									@error('payment') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Phone</label>
								<div class="col-lg-12">
									<input type="text" name="phone" value="{{old('phone')}}" id="phone"
										class="form-control {{$errors->has('phone') ? 'validatebox-invalid' : '' }}"
										placeholder="Phone">
									@error('payment') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Pay</label>
								<div class="col-lg-12">
									<input type="number" name="price" value="{{old('pay')}}" id="pay"
										class="form-control {{$errors->has('pay') ? 'validatebox-invalid' : '' }}"
										placeholder="pay">
									@error('payment') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Description</label>
								<div class="col-lg-12">
									<textarea name="description" id="description"
										class="form-control {{$errors->has('description') ? 'validatebox-invalid' : '' }}"
										autocomplete="off" tabindex="" placeholder="description..."
										style="vertical-align: top;">{{old('description')}}
										</textarea>
									@error('description') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>
						
						<div class="modal-footer">
							<a href="javascript:void(0)" class="btn btn-block btn-md btn-primary"   onclick="editkwit()" >Edit</a>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
