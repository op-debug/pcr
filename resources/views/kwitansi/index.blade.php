@extends('app')

@section('content')
<section id="kwitansi">
	<div class="row match-height">
		<div class="col-xl-12 col-md-12 col-12">
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Opps Something went wrong</strong>
                <button type="button" class="btn-close btn-secondary" data-bs-dismiss="alert" aria-label="Close">X</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
              </div>
            @endif
            
            @include('kwitansi.table')
        </div>
    </div>
</section>

<style>
    .select2-selection__arrow b{
        display:none !important;
    }
</style>

@include('kwitansi.create')
@include('kwitansi.edit')

@endsection

@push('scripts')
<script>

$(function(){
        var dg = $('#test-table').datagrid();
        
        dg.datagrid({
            toolbar: "#test-table-toolbar",
            pageList: [5, 10, 15, 20, 25],			
			remoteSort:false,
            pageSize: 10,
            striped: true,
            pagination: true,
            fit: true,
            fitColumns: false,
            rownumbers: true,
            checkbox: true,
            singleSelect: true,
            selectOnCheck: false,
            checkOnSelect: false,
            nowrap: false,
            method: "POST",
            frozenColumns: [[
                {field: "ck",checkbox:true},
            ]],
            columns:[[
                
                {field:'test',title:'Test', align:'center', sortable:false,width: 85},
               
                {field:'created_at',title:'Date', align:'center', sortable:false,width: 85,
                    formatter: function(value,row,index){
                        return moment(value).format("DD MMM YYYY"); 
                    }
                 },
                {field:'pasien_no',title:'Patient No', align:'center', sortable:false,width: 185 },
                {field:'fullname',title:'Fullname', align:'center', sortable:false,width: 200 },
                {field:'dob',title:'Date Of Birth', align:'center', sortable:false,
                    formatter: function(value,row,index){
                        return moment(value).format("DD MMM YYYY"); 
                    }
                },
                {field:'gender',title:'Gender', align:'center', sortable:false,width: 100 },
                {field:'nik',title:'Identity No', align:'center', sortable:false,width: 200  },
                {field:'no_phone',title:'Phone/Tlpn', align:'center', sortable:false,width: 150  },
                {field:'email',title:'Email', align:'center', sortable:false,width: 150  },
                {field:'penjamin',title:'Penjamin', align:'center', sortable:false,width: 200  },
                {field:'swabber_date',title:'Swab Date', align:'center', sortable:false, width: 250,
                    formatter: function(value,row,index){
                        return moment(value).format("YYYY-MM-DD h:mm:ss"); 
                    }
                },
                {field:'swabber_name',title:'Swabber Name', align:'center', sortable:false, width: 250 },
                {field:'r_status',title:'Status', align:'center', sortable:false, width: 150,
                    formatter: function(value,row,index){
                        if(value == ''){
                            return '<span style="color: #000;">Entry</span>';
                        }else if(value == 'dikirim'){
                            return '<span style="color: skyblue;">Dikirim</span>';
                        }else if(value == 'diterima'){
                            return '<span style="color: blue;">Diterima</span>';
                        }else if(value == "ekstraksi"){
                            return '<span style="color: red;">Prosess Ekstraksi</span>';
                        }else if(value=="selesaiekstraksi"){
                            return '<span style="color: green;">Finish Ekstraksi</span>';
                        }else if(value=="analize"){
                            return '<span style="color: red;">Analize PCR</span>';
                        }
                        else{
                            return '-';
                        }
                        
                    }
                },
                {field:'price',title:'Price', align:'center', sortable:false, width: 250 },
            ]],
            
        });
    });


    $(function(){
        var dg = $('#kwitansi-table').datagrid();
        
        dg.datagrid({
            toolbar: "#kwitansi-table-toolbar",
            pageList: [5, 10, 15, 20, 25],			
			remoteSort:false,
            pageSize: 10,
            striped: true,
            pagination: true,
            fit: true,
            fitColumns: false,
            rownumbers: true,
            singleSelect: true,
            selectOnCheck: false,
            checkOnSelect: false,
            nowrap: false,
            pageSize: 10,
            method: "POST",
            columns:[[
                
                {field:'year',title:'Year', align:'center', sortable:false,width: 250 },
                {field:'date',title:'Date', align:'center', sortable:false,width: 250  },
                {field:'no_recipt',title:'No. Receipt', align:'center', sortable:false,width: 250  },
                {field:'payment',title:'Payment', align:'center', sortable:false,width: 250  },
                {field:'sampling_point',title:'Sampling Point', align:'center', sortable:false,width: 250  },
                {field:'description',title:'Description', align:'center', sortable:false,width: 250 },
                {field:'br_year',title:'BR Year', align:'center', sortable:false, width: 250},
                {field:'br_date',title:'BR Date', align:'center', sortable:false, width: 250},
                {field:'booking_recipt_no',title:'Booking Receipt No.', align:'center', sortable:false,width: 250 },
                {field:'service',title:'Service', align:'center', sortable:false,width: 250 },
                {field:'name',title:'Name', align:'center', sortable:false,width: 250 },
                {field:'phone',title:'Phone', align:'center', sortable:false,width: 250 },
            ]],
            
        });
    });

    function _jf_102_grid02_create_kwitansi(){
        var check = $("#test-table").datagrid('getChecked');
        var sel = $("#test-table").datagrid('getSelected');
        if (sel){
            var patientIdList = [];
            patientIdList.push(sel.id);
            var message_confirm = "Add Selected data to Kwitansi ?";
            execute_addkwitansi(patientIdList,message_confirm);
        }
        else if(check.length>0){
            var patientIdList = [];
            check.forEach(function(value, index){
                patientIdList.push(value.id);
            });        
            var message_confirm = "Add Selected data to Kwitansi ?";
            execute_addkwitansi(patientIdList,message_confirm);

        }
        else{
            $.messager.alert("Add Kwitansi","Checklist Sample Patient To Prosess.");
        }
    }


    function execute_addkwitansi(patientIdList="", message_confirm=""){
        if(patientIdList!=""){
        $.messager.confirm("Sample Patient", message_confirm,function(r){
            if(r){ 
            $.ajax({
                type: 'POST',
                dataType: "JSON",
                url: "{{ route('kwitansi-store') }}",
                data:{
                    _token: "{{ csrf_token() }}",
                    _method: 'POST',
                    id: patientIdList
                },
                success: function(res) {
                if (res.success) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
                    $("#kwitansi-table").datagrid('reload');
                    $("#kwitansi-table").datagrid('clearSelections');
                    $("#kwitansi-table").datagrid('clearChecked');
                }
            },
                error:function(){
                    $.messager.alert("Add Sampple Patient", "Request error, please try again.");
                }
            })
            }
        })
        }else{
            alert("Please select or checklist data!");
        }
    }


    function _jf_102_grid02_show_reset(){
        $("#test-table").datagrid('reload');
    }
    function editKwitansi(){
        $('#editKwitansi').trigger('reset');
        var data = $("#kwitansi-table").datagrid('getSelected');
        // console.log(data.has_kwitansi);
        if(data){
            $('#editKwitansi').modal('show');
            $('#form-editKwitansi').form('load',data);
        }else{
            $.messager.alert("Edit Kwitansi","Select Data first.");
        }
    }

    function deleteKwitansi(){
        var sel = $("#kwitansi-table").datagrid('getSelected');
        if(sel){
            var patientIdList = [];
            patientIdList.push(sel.id);
            var message_confirm = "Delete Selected data ?";
            execute_delete(patientIdList,message_confirm); 
        } else {
            $.messager.alert("Delete Data Kwitansi","Select Data first.");
        }
    }


    function _jf_102_grid50_print_kwitansi(){
        var sel = $("#kwitansi-table").datagrid('getSelected');
        if(sel){
            window.open("{{ route('kwitansi-pdfAll')}}?id="+sel.id, '_blank'); 
        } else {
            $.messager.alert("Print Kwitansi","Select Data first.");
        }
    }
    function addmore(){
        var sel = $("#kwitansi-table").datagrid('getSelected');
        if(sel){
            $.ajax({
                type: 'POST',
                url: "{{ route('kwitansi-addmore') }}",
                data:{
                    id: sel.id
                },
                success: function(res) {
                if (res.success) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
                    $("#kwitansi-table").datagrid('reload');
                    $("#kwitansi-table").datagrid('clearSelections');
                    $("#kwitansi-table").datagrid('clearChecked');
                }
                }
            });
        }
        else {
            $.messager.alert("Add Data Kwitansi","Select Data Kwitansi first.");
        }
    }
    function editkwit(){
		$.ajax({
            type: 'POST',
			data:$('#form-editKwitansi').serialize(),
            url: "{{route('kwitansi-update')}}",
            success: function(res) {
				if (res.success) {
					Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
					$('#editKwitansi').modal('hide');
					$("#kwitansi-table").datagrid('reload');
                    $("#kwitansi-table").datagrid('clearSelections');
                    $("#kwitansi-table").datagrid('clearChecked');
				}
			},
            error:function(err){
				if (err.status == 422) { // when status code is 422, it's a validation issue
					console.log(err.responseJSON);
					$('#success_message').fadeIn().html(err.responseJSON.message);
					
					// you can loop through the errors object and show it to the user
					console.warn(err.responseJSON.errors);
					// display errors on each form field
					$.each(err.responseJSON.errors, function (i, error) {
						var el = $(document).find('[name="'+i+'"]');
						el.after($('<span style="color: red;">'+error[0]+'</span>'));
					});
				}
            }
        })
	}


    function execute_delete(patientIdList="", message_confirm=""){
        if(patientIdList!=""){
            // console.log(patientIdList);
        $.messager.confirm("Delete Patient", message_confirm,function(r){
            // console.log(r)
            if(r){
             
            $.ajax({
                type: 'DELETE',
                dataType: "JSON",
                url: "{{ route('kwitansi-destroy') }}",
                data:{
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE',
                    id: patientIdList
                },
                success: function(res) {
                // $.messager.alert("Delete Patient", r.message);
                if (res.success) {
                    // console.log(res)
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
                    $("#kwitansi-table").datagrid('reload');
                    $("#kwitansi-table").datagrid('clearSelections');
                    $("#kwitansi-table").datagrid('clearChecked');
                }
            },
                error:function(){
                    $.messager.alert("Delete Patient", "Request error, please try again.");
                }
            })
            }
        })
        }else{
            alert("Please select or checklist data!");
        }
    }

    $(document).ready(function(){
        var msg = '{{Session::get('sukses')}}';
        var exist = '{{Session::has('sukses')}}';
        if(exist){
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: msg,
                showConfirmButton: false,
                timer: 2500
            })
        }

    })
</script>

@endpush