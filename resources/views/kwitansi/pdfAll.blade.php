<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<style>
    .divider{
        border-bottom: 5px solid gray;
        margin-top: -20px;
    }
    .divider-1{
        border-bottom: 5px solid gray;
       
    }
    .title p{
        font-size: 14px;
        font-weight: bold;
        text-align: center;
        padding-top: 20px;
    }
    td{
        font-size: 15px;
        line-height: 1.1;
    }
    .table-wrapper-1{
        margin-top: 50px;
    }
    .flex-1{
        margin-bottom: 50px;
    }
    .con{
        padding:5px;
    }
</style>
<body>
    <div class="con">
        <center>
            <h6><b><span style="margin-left:75%;position:fixed;margin-top:-30px">SOW {{ $qrpatient['no_recipt'] }}</span></b></h6>
            <h6><b>BUKTI PEMBAYARAN</b></h6>
            <h6><b>DRIVE THRU RAPID TEST</b></h6>
        </center>
        <p style="font-size:13px">
        NAMA : {{ $qrpatient['name'] }}
        <br>
        <span style="margin-left:70%;position:fixed;margin-top:-20px">PT ............................................</span>
        NO. TELP : 081328675727
        </p>
            <table border="1" width="100%">
                <tr>
                    <td width="50%" class="text-center">ORT Antibodi (igG/igM)</td>
                    <td width="30%"  class="text-center">ORT Swab Antigen</td>
                    <td width="20%" class="text-center">OPCR</td>
                </tr>
                <tr>
                    <td align="left">{{ $qrpatient['payment'] }}</td>
                    <td align="left">Quantity : 1</td>
                    <td align="left">Rp {{ number_format($qrpatient['price']) }}</td>
                </tr>
                <tr>
                    <td align="left" height="15px"> </td>
                    <td align="left"></td>
                    <td align="left"></td>
                </tr>
                <tr>
                    <td align="left" height="15px"> </td>
                    <td align="left"></td>
                    <td align="left"></td>
                </tr>
                <tr>
                    <td align="left" height="15px"></td>
                    <td align="left"></td>
                    <td align="left"></td>
                </tr>
                <tr>
                    <td align="left" height="15px"></td>
                    <td align="left"></td>
                    <td align="left"></td>
                </tr>
                <tr>
                    <td align="left" height="15px"></td>
                    <td align="left"></td>
                    <td align="left"></td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td width="50%"></td>
                    <td width="30%"></td>
                    <td width="20%" class="text-center">Kasir
                    <br/>
                    <br/>
                    <br/>
                    ..............................................
                    </td>
                </tr>
            </table>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>

</html>