<div class="row">
    <div class="col-12">
        <table id="test-table" title="Test" class="easyui-datagrid" url="{{url('pcr-table')}}" toolbar="#toolbar"
            pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" style="width:100%;height:300px;">
        </table>
        <div id="test-table-toolbar" class="p-1">
            <div class="row">
                
                <div class="col-12"> 
                    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="_jf_102_grid02_show_reset()">Show-All</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="_jf_102_grid02_create_kwitansi()">Create-Kwitansi</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="padding-top:20px">
    <div class="col-12">
        <table id="kwitansi-table" title="Kwitansi" class="easyui-datagrid" url="{{route('kwitansi-table')}}" toolbar="#toolbar"
            pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" style="width:100%;height:300px;">
        </table>
        
        <div id="kwitansi-table-toolbar" class="p-1">
            <div class="row">
                
                <div class="col-12"> 
                    <div style="float: left">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true" onclick="_jf_102_grid50_print_kwitansi()">Print-Kwitansi</a>              
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="editKwitansi()"><i class="fa fa-edit fa-fw" style="color:blue;"></i> Edit</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="deleteKwitansi()"><i class="fa fa-trash" style="color:red;"></i> Delete</a>
                        &nbsp;|&nbsp; 
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add', plain:true" onclick="addmore()"> Add-More</a>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
