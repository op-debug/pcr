<div class="modal fade" id="addKwitansi" tabindex="-1" aria-labelledby="addKwitansiLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<h5 class="modal-title" id="addKwitansiLabel">Add Kwitansi</h5>
				<button type="button" class="btn-close btn-secondary" data-bs-dismiss="modal" aria-label="Close">X</button>
			</div>

			<div class="modal-body">
				<div class="modal-content">

					<form id="form-addKwitansi" method="POST" action="{{route('kwitansi-store')}}" enctype="multipart/form-data">
						@csrf
						<div class="pt-2 pb-2 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-6"><b>Patient data</b></span>
								</div>
							</div>
							@if(isset($pcrs))
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Pilih Patient</label>
								<select name="patient_id" id="patient_id"
									class="form-control {{$errors->has('patient_id') ? 'validatebox-invalid' : '' }}">
									<option value="">-Pilih-</option>
									@foreach ($pcrs as $patient)
									<option value="{{$patient->id}}" {{ old('patient_id')==$patient->id ?
										'selected="selected"': '' }}>{{$patient->fullname}}</option>
									@endforeach
								</select>
								<input type="hidden" id="patient_name" name="patient_name">
								@error('patient_id') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							@endif
                            <div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Nomor Recipt</label>
								<div class="col-lg-12">
									<input type="text" name="no_recipt" value="{{old('no_recipt')}}"
										class="form-control {{$errors->has('no_recipt') ? 'validatebox-invalid' : '' }}"
										placeholder="no recipt">
									@error('no_recipt') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
                            <div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Description</label>
								<div class="col-lg-12">
									<textarea name="description"
										class="form-control {{$errors->has('description') ? 'validatebox-invalid' : '' }}"
										autocomplete="off" tabindex="" placeholder="description..."
										style="vertical-align: top;">{{old('description')}}
										</textarea>
									@error('description') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>		
						<div class="modal-footer">
							<button href="javascript:void(0)" class="btn btn-block btn-md btn-primary" id="btnSubmit"
								type="submit">Save</button>
						</div>
					</form>
					{{-- <div class="alert alert-danger" style="display:none"></div> --}}
				</div>
			</div>
			{{-- <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div> --}}
		</div>
	</div>
</div>
@push('scripts')

<script>
	

	$(document).ready(function(){
		$('#patient_id').change(function() {
			$('#patient_name').val($('option:selected', this).text());
		});
	

	});
</script>
@endpush