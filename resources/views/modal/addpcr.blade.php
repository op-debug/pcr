<div id="my_modal_frame"><div class="modal fade show" id="_102_grid02_dialog" aria-modal="true" style="display: block;">
<div class="modal-dialog ">
  <div class="modal-content">
    <div class="modal-header my_bg_inatti text-white"><h5 class="modal-title">Add Patient</h5><button type="button" class="close" onclick="my_modal_close('_102_grid02_dialog')"><span aria-hidden="true">×</span></button></div><div class="modal-body">
<div class="modal-content" id="_102_grid02_form_content">
  <form id="_102_grid02_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="SGf26USQOrvEPgBbeAPLH61mKE1AhY8i4BpwIZCu">
    <div class="pt-2 pb-2 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-6"><b>Branch Data</b></span>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_branch_id_error" class="form_error"></div>
        <label for="_102_f_branch_id" class="col-lg-12 col-form-label pb-1">Branch</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_branch_id" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_branch_id" comboname="f_branch_id"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input86" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_branch_id" value="20"></span>
        </div>
      </div>

    </div>

    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-6"><b>Profile</b></span>
          <div class="col-lg-6">
            <div class="float-right" id="label_use_nar_biodata">
              <label class="textbox-label textbox-label-before" for="_easyui_checkbox_7" style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use Profile From SISLAB?</b></label><input class="easyui-checkbox checkbox-f" value="1" id="_102_valid_biodata" label="<b>Use Profile From SISLAB?</b>" checkboxname="f_valid_biodata" style="display: none;"><span class="checkbox inputbox" style="width: 20px; height: 20px;"><span class="checkbox-inner" style="display: none;"><svg xml:space="preserve" focusable="false" version="1.1" viewBox="0 0 24 24"><path d="M4.1,12.7 9,17.6 20.3,6.3" fill="none" stroke="white"></path></svg></span><input type="checkbox" class="checkbox-value" name="f_valid_biodata" id="_easyui_checkbox_7" value="1"></span>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_id_type_error" class="form_error"></div>
        <label for="_102_f_id_type" class="col-lg-12 col-form-label pb-1">Identity Type</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_id_type" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_id_type" comboname="f_id_type"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input89" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_id_type" value="0"></span>
        </div>
      </div>
      
              <div class="form-group row mb-1">
          <div id="f_nik_error" class="form_error"></div>
          <label id="label_identitas" for="_102_f_id_no" class="col-lg-12 col-form-label pb-1">NIK</label>
          <div class="col-lg-3 mb-1" id="nationality_code">
            <input type="text" id="_102_f_nationality_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_nationality_code" comboname="f_nationality_code"><span class="textbox easyui-fluid combo" style="width: 74px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input91" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="Nationality Code" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 46px;"><input type="hidden" class="textbox-value" name="f_nationality_code" value="ID"></span>
          </div>
          <div class="mb-1 col-lg-6" id="field_nik">
            <input type="text" id="_102_f_id_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_nik"><span class="textbox easyui-fluid textbox-invalid" style="width: 173px;"><input id="_easyui_textbox_input93" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="NIK" maxlength="20" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 171px;"><input type="hidden" class="textbox-value" name="f_nik" value=""></span>
          </div>
          
        </div>
            <div class="form-group row mb-1" id="identity_2" style="display: none;">
        <div id="f_identitas_2_error" class="form_error"></div>
        <label for="_102_f_identitas_2" class="col-lg-12 col-form-label pb-1">PASPORT</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_identitas_2" style="width: 100%; display: none;" class="textbox-f" textboxname="f_identitas_2"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input90" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="PASPORT" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_identitas_2" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_name_error" class="form_error"></div>
        <label for="_102_f_name" class="col-lg-12 col-form-label pb-1">Fullname</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_name" style="width: 100%; display: none;" class="textbox-f" textboxname="f_name"><span class="textbox textbox-invalid easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input104" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Patient Fullname" maxlength="100" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_name" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_dob_ddmmyyyy_error" class="form_error"></div>
        <label for="_102_f_dob" class="col-lg-12 col-form-label pb-1">DOB</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_dob" style="width: 100%; display: none;" class="datebox-f combo-f textbox-f" textboxname="f_dob_ddmmyyyy" comboname="f_dob_ddmmyyyy"><span class="textbox textbox-invalid easyui-fluid combo datebox" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input105" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="dd/mm/yyyy" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_dob_ddmmyyyy" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_gender_error" class="form_error"></div>
        <label for="_102_f_gender" class="col-lg-12 col-form-label pb-1">Gender</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_gender" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_gender" comboname="f_gender"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input106" type="text" class="textbox-text validatebox-text validatebox-readonly validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" readonly="" placeholder="Select Gender" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_gender" value=""></span>
        </div>
      </div>

    </div>


    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-6"><b>Domicile address</b></span>
          <div class="col-lg-6">
            <div class="float-right" id="label_use_nar_address">
              <label class="textbox-label textbox-label-before" for="_easyui_checkbox_6" style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use NAR Data?</b></label><input class="easyui-checkbox checkbox-f" value="1" id="_102_valid_address" label="<b>Use NAR Data?</b>" checkboxname="f_valid_address" style="display: none;"><span class="checkbox inputbox" style="width: 20px; height: 20px;"><span class="checkbox-inner" style="display: none;"><svg xml:space="preserve" focusable="false" version="1.1" viewBox="0 0 24 24"><path d="M4.1,12.7 9,17.6 20.3,6.3" fill="none" stroke="white"></path></svg></span><input type="checkbox" class="checkbox-value" name="f_valid_address" id="_easyui_checkbox_6" value="1"></span>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_provinsi_code_error" class="form_error"></div>
        <label for="_102_f_provinsi_code" class="col-lg-12 col-form-label pb-1">Provinsi</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_provinsi_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_provinsi_code" comboname="f_provinsi_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input114" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Provinsi" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_provinsi_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_kota_code_error" class="form_error"></div>
        <label for="_102_f_kota_code" class="col-lg-12 col-form-label pb-1">Kabupaten/Kota</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_kota_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_kota_code" comboname="f_kota_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input115" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Kabupaten/Kota" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_kota_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_kecamatan_code_error" class="form_error"></div>
        <label for="_102_f_kecamatan_code" class="col-lg-12 col-form-label pb-1">Kecamatan</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_kecamatan_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_kecamatan_code" comboname="f_kecamatan_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input116" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Kecamatan" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_kecamatan_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_kelurahan_code_error" class="form_error"></div>
        <label for="_102_f_kelurahan_code" class="col-lg-12 col-form-label pb-1">Kelurahan</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_kelurahan_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_kelurahan_code" comboname="f_kelurahan_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input117" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Kelurahan" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_kelurahan_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div class="col-lg-6">
          <div class="row">
            <div id="f_rt_error" class="form_error"></div>
            <label for="_102_f_rt" class="col-lg-12 col-form-label pb-1">RT</label>
            <div class="col-lg-12">
              <input type="text" id="_102_f_rt" style="width: 100%; display: none;" class="textbox-f" textboxname="f_rt"><span class="textbox easyui-fluid" style="width: 173px;"><input id="_easyui_textbox_input101" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="RT" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 171px;"><input type="hidden" class="textbox-value" name="f_rt" value=""></span>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="row">
            <div id="f_rw_error" class="form_error"></div>
            <label for="_102_f_rw" class="col-lg-12 col-form-label pb-1">RW</label>
            <div class="col-lg-12">
              <input type="text" id="_102_f_rw" style="width: 100%; display: none;" class="textbox-f" textboxname="f_rw"><span class="textbox easyui-fluid" style="width: 173px;"><input id="_easyui_textbox_input102" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="RW" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 171px;"><input type="hidden" class="textbox-value" name="f_rw" value=""></span>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_address_error" class="form_error"></div>
        <label for="_102_f_address" class="col-lg-12 col-form-label pb-1">Alamat</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_address" style="width: 100%; display: none;" class="textbox-f" textboxname="f_address"><span class="textbox textbox-invalid easyui-fluid" style="width: 370px; height: 60px;"><textarea id="_easyui_textbox_input103" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Alamat..." style="margin: 0px; height: 58px; width: 368px;"></textarea><input type="hidden" class="textbox-value" name="f_address" value=""></span>
        </div>
      </div>

    </div>

    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-12"><b>More</b></span>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_phone_no_error" class="form_error"></div>
        <label for="_102_f_phone_no" class="col-lg-12 col-form-label pb-1">Handphone No.</label>
        <div class="col-lg-12">
          <input type="number" id="_102_f_phone_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_phone_no"><span class="textbox textbox-invalid easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input107" type="number" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="62XXXXXXXXXXX" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_phone_no" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_email_error" class="form_error"></div>
        <label for="_102_f_email" class="col-lg-12 col-form-label pb-1">Email</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_email" style="width: 100%; display: none;" class="textbox-f" textboxname="f_email"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input108" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Email" maxlength="50" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_email" value=""></span>
        </div>
      </div>

      <div class="form-group row mb-1" id="group_nationality" style="display: none;">
        <div id="f_nationality_group_error" class="form_error"></div>
        <label for="_102_f_nationality_group" class="col-lg-12 col-form-label pb-1">Nationality</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_nationality_group" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_nationality_group" comboname="f_nationality_group"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input95" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Nationality" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_nationality_group" value="1"></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="others_nationality" style="display: none;">
        <div id="f_nationality_error" class="form_error"></div>
        <label for="_102_f_nationality" class="col-lg-12 col-form-label pb-1">Other Nationality</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_nationality" style="width: 100%; display: none;" class="textbox-f" textboxname="f_nationality"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input94" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="Nationality" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_nationality" value="Indonesia"></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_corporate_name_error" class="form_error"></div>
        <label for="_102_f_corporate_name" class="col-lg-12 col-form-label pb-1">Corporate Name</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_corporate_name" style="width: 100%; display: none;" class="textbox-f" textboxname="f_corporate_name"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input109" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Corporate Name" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_corporate_name" value=""></span>
        </div>
      </div>
    </div>

    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-12"><b>Detail Test</b></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_pemeriksaan_tujuan_id_error" class="form_error"></div>
        <label for="_102_f_pemeriksaan_tujuan_id" class="col-lg-12 col-form-label pb-1">Inspection purpose</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_pemeriksaan_tujuan_id" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_pemeriksaan_tujuan_id" comboname="f_pemeriksaan_tujuan_id"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input112" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Choose Inspection purpose" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_pemeriksaan_tujuan_id" value="3"></span>
        </div>
      </div>
    
      <div id="form_detail" class="d-none d-block">
      <div class="form-group row mb-1">        
        <div id="f_detail_tujuan_id_error" class="form_error"></div>
        <label for="_102_f_detail_tujuan_id" class="col-lg-12 col-form-label pb-1">Detail</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_detail_tujuan_id" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_detail_tujuan_id" comboname="f_detail_tujuan_id"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input118" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Choose detail" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_detail_tujuan_id" value="1"></span>
        </div>
      </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_status_error" class="form_error"></div>
        <label for="_102_f_status" class="col-lg-12 col-form-label pb-1">Status</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_status" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_status" comboname="f_status"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input110" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="Choose Status" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_status" value="sSwab"></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="swabber_name">
        <div id="f_swabber_name_error" class="form_error"></div>
        <label for="_102_f_swabber_name" class="col-lg-12 col-form-label pb-1">Swabber Name</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_swabber_name" style="width: 100%; display: none;" class="textbox-f" textboxname="f_swabber_name"><span class="textbox easyui-fluid textbox-invalid" style="width: 370px;"><input id="_easyui_textbox_input119" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Swabber name" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_swabber_name" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="swab_dt">
        <div id="f_swab_dt_error" class="form_error"></div>
        <label for="_102_f_swab_dt" class="col-lg-12 col-form-label pb-1">Swab Date</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_swab_dt" style="width: 100%; display: none;" class="combo-f textbox-f datetimebox-f" textboxname="f_swab_dt" comboname="f_swab_dt"><span class="textbox easyui-fluid combo datebox" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input96" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Y-M-D" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_swab_dt" value="2022-04-04 11:23:48"></span>
        </div>
      </div>
      
      
      <div class="form-group row mb-1" id="patient_no">
        <div id="f_patient_no_error" class="form_error"></div>
        <label for="_102_f_patient_no" class="col-lg-12 col-form-label pb-1">Pasien No.</label>
        <div class="col-lg-9 col-9">
          <input type="text" id="_102_f_patient_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_patient_no"><span class="textbox easyui-fluid" style="width: 271px;"><input id="_easyui_textbox_input98" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="No. Pasien" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 269px;"><input type="hidden" class="textbox-value" name="f_patient_no" value="00213273.00007437"></span>
        </div>
        <div class="col-lg-3 col-3">
          <div class="btn btn-sm btn-block btn-info" onclick="_jf_102_grid01_generate()">Get No.</div>
        </div>
      </div>
      <div class="form-group row mb-1" id="lab_no" style="display: none;">
        <div id="f_klab_patient_no_error" class="form_error"></div>
        <label for="_102_f_klab_patient_no" class="col-lg-12 col-form-label pb-1">Lab. No.</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_klab_patient_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_klab_patient_no"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input99" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Lab. No." style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_klab_patient_no" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="no_external">
        <div id="f_patient_no_external_error" class="form_error"></div>
        <label for="_102_f_patient_no_external" class="col-lg-12 col-form-label pb-1">External No.</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_patient_no_external" style="width: 100%; display: none;" class="textbox-f" textboxname="f_patient_no_external"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input100" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Ext. No." style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_patient_no_external" value=""></span>
        </div>
      </div>
    </div>
  </form>

  

</div>
</div><div class="modal-footer">
      <a href="javascript:void(0)" class="btn btn-block btn-md btn-primary" onclick="_jf_102_grid02_create()">Save</a>
  </div></div></div></div></div>