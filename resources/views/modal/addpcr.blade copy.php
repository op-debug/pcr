<div id="my_modal_frame">
  <div class="modal fade show" id="_102_grid02_dialog" aria-modal="true" style="display: block;">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header my_bg_inatti text-white">
          <h5 class="modal-title">Add Patient</h5>
          <button type="button" class="close" onclick="my_modal_close('_102_grid02_dialog')"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body"><style>
  .textbox-readonly .textbox-text{
      background: rgb(228, 228, 228);
      color: rgb(133, 133, 133);
  }
  label{
    font-size: 8pt !important;
  }
  #_102_grid02_dialog .modal-body{
    background: rgb(244 250 255);
  }
</style>
<div class="container mt-3 mb-2" id="_102_grid02_form_content">
  <form id="_102_grid02_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="SGf26USQOrvEPgBbeAPLH61mKE1AhY8i4BpwIZCu">        
    <div class="pt-2 pb-2 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-6"><b>Branch Data</b></span>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_branch_id_error" class="form_error"></div>
        <label for="_102_f_branch_id" class="col-lg-12 col-form-label pb-1">Branch</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_branch_id" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_branch_id" comboname="f_branch_id"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input86" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_branch_id" value="20"></span>
        </div>
      </div>

    </div>

    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-6"><b>Profile</b></span>
          <div class="col-lg-6">
            <div class="float-right" id="label_use_nar_biodata">
              <label class="textbox-label textbox-label-before" for="_easyui_checkbox_7" style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use Profile From SISLAB?</b></label><input class="easyui-checkbox checkbox-f" value="1" id="_102_valid_biodata" label="<b>Use Profile From SISLAB?</b>" checkboxname="f_valid_biodata" style="display: none;"><span class="checkbox inputbox" style="width: 20px; height: 20px;"><span class="checkbox-inner" style="display: none;"><svg xml:space="preserve" focusable="false" version="1.1" viewBox="0 0 24 24"><path d="M4.1,12.7 9,17.6 20.3,6.3" fill="none" stroke="white"></path></svg></span><input type="checkbox" class="checkbox-value" name="f_valid_biodata" id="_easyui_checkbox_7" value="1"></span>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_id_type_error" class="form_error"></div>
        <label for="_102_f_id_type" class="col-lg-12 col-form-label pb-1">Identity Type</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_id_type" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_id_type" comboname="f_id_type"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input89" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_id_type" value="0"></span>
        </div>
      </div>
      
              <div class="form-group row mb-1">
          <div id="f_nik_error" class="form_error"></div>
          <label id="label_identitas" for="_102_f_id_no" class="col-lg-12 col-form-label pb-1">NIK</label>
          <div class="col-lg-3 mb-1" id="nationality_code">
            <input type="text" id="_102_f_nationality_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_nationality_code" comboname="f_nationality_code"><span class="textbox easyui-fluid combo" style="width: 74px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input91" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="Nationality Code" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 46px;"><input type="hidden" class="textbox-value" name="f_nationality_code" value="ID"></span>
          </div>
          <div class="mb-1 col-lg-6" id="field_nik">
            <input type="text" id="_102_f_id_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_nik"><span class="textbox easyui-fluid textbox-invalid" style="width: 173px;"><input id="_easyui_textbox_input93" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="NIK" maxlength="20" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 171px;"><input type="hidden" class="textbox-value" name="f_nik" value=""></span>
          </div>
          
        </div>
            <div class="form-group row mb-1" id="identity_2" style="display: none;">
        <div id="f_identitas_2_error" class="form_error"></div>
        <label for="_102_f_identitas_2" class="col-lg-12 col-form-label pb-1">PASPORT</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_identitas_2" style="width: 100%; display: none;" class="textbox-f" textboxname="f_identitas_2"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input90" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="PASPORT" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_identitas_2" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_name_error" class="form_error"></div>
        <label for="_102_f_name" class="col-lg-12 col-form-label pb-1">Fullname</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_name" style="width: 100%; display: none;" class="textbox-f" textboxname="f_name"><span class="textbox textbox-invalid easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input104" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Patient Fullname" maxlength="100" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_name" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_dob_ddmmyyyy_error" class="form_error"></div>
        <label for="_102_f_dob" class="col-lg-12 col-form-label pb-1">DOB</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_dob" style="width: 100%; display: none;" class="datebox-f combo-f textbox-f" textboxname="f_dob_ddmmyyyy" comboname="f_dob_ddmmyyyy"><span class="textbox textbox-invalid easyui-fluid combo datebox" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input105" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="dd/mm/yyyy" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_dob_ddmmyyyy" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_gender_error" class="form_error"></div>
        <label for="_102_f_gender" class="col-lg-12 col-form-label pb-1">Gender</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_gender" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_gender" comboname="f_gender"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input106" type="text" class="textbox-text validatebox-text validatebox-readonly validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" readonly="" placeholder="Select Gender" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_gender" value=""></span>
        </div>
      </div>

    </div>


    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-6"><b>Domicile address</b></span>
          <div class="col-lg-6">
            <div class="float-right" id="label_use_nar_address">
              <label class="textbox-label textbox-label-before" for="_easyui_checkbox_6" style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use NAR Data?</b></label><input class="easyui-checkbox checkbox-f" value="1" id="_102_valid_address" label="<b>Use NAR Data?</b>" checkboxname="f_valid_address" style="display: none;"><span class="checkbox inputbox" style="width: 20px; height: 20px;"><span class="checkbox-inner" style="display: none;"><svg xml:space="preserve" focusable="false" version="1.1" viewBox="0 0 24 24"><path d="M4.1,12.7 9,17.6 20.3,6.3" fill="none" stroke="white"></path></svg></span><input type="checkbox" class="checkbox-value" name="f_valid_address" id="_easyui_checkbox_6" value="1"></span>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_provinsi_code_error" class="form_error"></div>
        <label for="_102_f_provinsi_code" class="col-lg-12 col-form-label pb-1">Provinsi</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_provinsi_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_provinsi_code" comboname="f_provinsi_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input114" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Provinsi" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_provinsi_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_kota_code_error" class="form_error"></div>
        <label for="_102_f_kota_code" class="col-lg-12 col-form-label pb-1">Kabupaten/Kota</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_kota_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_kota_code" comboname="f_kota_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input115" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Kabupaten/Kota" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_kota_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_kecamatan_code_error" class="form_error"></div>
        <label for="_102_f_kecamatan_code" class="col-lg-12 col-form-label pb-1">Kecamatan</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_kecamatan_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_kecamatan_code" comboname="f_kecamatan_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input116" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Kecamatan" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_kecamatan_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_kelurahan_code_error" class="form_error"></div>
        <label for="_102_f_kelurahan_code" class="col-lg-12 col-form-label pb-1">Kelurahan</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_kelurahan_code" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_kelurahan_code" comboname="f_kelurahan_code"><span class="textbox textbox-invalid easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input117" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Kelurahan" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_kelurahan_code" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div class="col-lg-6">
          <div class="row">
            <div id="f_rt_error" class="form_error"></div>
            <label for="_102_f_rt" class="col-lg-12 col-form-label pb-1">RT</label>
            <div class="col-lg-12">
              <input type="text" id="_102_f_rt" style="width: 100%; display: none;" class="textbox-f" textboxname="f_rt"><span class="textbox easyui-fluid" style="width: 173px;"><input id="_easyui_textbox_input101" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="RT" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 171px;"><input type="hidden" class="textbox-value" name="f_rt" value=""></span>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="row">
            <div id="f_rw_error" class="form_error"></div>
            <label for="_102_f_rw" class="col-lg-12 col-form-label pb-1">RW</label>
            <div class="col-lg-12">
              <input type="text" id="_102_f_rw" style="width: 100%; display: none;" class="textbox-f" textboxname="f_rw"><span class="textbox easyui-fluid" style="width: 173px;"><input id="_easyui_textbox_input102" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="RW" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 171px;"><input type="hidden" class="textbox-value" name="f_rw" value=""></span>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_address_error" class="form_error"></div>
        <label for="_102_f_address" class="col-lg-12 col-form-label pb-1">Alamat</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_address" style="width: 100%; display: none;" class="textbox-f" textboxname="f_address"><span class="textbox textbox-invalid easyui-fluid" style="width: 370px; height: 60px;"><textarea id="_easyui_textbox_input103" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Alamat..." style="margin: 0px; height: 58px; width: 368px;"></textarea><input type="hidden" class="textbox-value" name="f_address" value=""></span>
        </div>
      </div>

    </div>

    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-12"><b>More</b></span>
        </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_phone_no_error" class="form_error"></div>
        <label for="_102_f_phone_no" class="col-lg-12 col-form-label pb-1">Handphone No.</label>
        <div class="col-lg-12">
          <input type="number" id="_102_f_phone_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_phone_no"><span class="textbox textbox-invalid easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input107" type="number" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="62XXXXXXXXXXX" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_phone_no" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_email_error" class="form_error"></div>
        <label for="_102_f_email" class="col-lg-12 col-form-label pb-1">Email</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_email" style="width: 100%; display: none;" class="textbox-f" textboxname="f_email"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input108" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Email" maxlength="50" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_email" value=""></span>
        </div>
      </div>

      <div class="form-group row mb-1" id="group_nationality" style="display: none;">
        <div id="f_nationality_group_error" class="form_error"></div>
        <label for="_102_f_nationality_group" class="col-lg-12 col-form-label pb-1">Nationality</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_nationality_group" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_nationality_group" comboname="f_nationality_group"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input95" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Nationality" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_nationality_group" value="1"></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="others_nationality" style="display: none;">
        <div id="f_nationality_error" class="form_error"></div>
        <label for="_102_f_nationality" class="col-lg-12 col-form-label pb-1">Other Nationality</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_nationality" style="width: 100%; display: none;" class="textbox-f" textboxname="f_nationality"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input94" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="Nationality" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_nationality" value="Indonesia"></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_corporate_name_error" class="form_error"></div>
        <label for="_102_f_corporate_name" class="col-lg-12 col-form-label pb-1">Corporate Name</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_corporate_name" style="width: 100%; display: none;" class="textbox-f" textboxname="f_corporate_name"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input109" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Corporate Name" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_corporate_name" value=""></span>
        </div>
      </div>
    </div>

    <div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
      <div class="form-group">
        <div class="row">
          <span class="col-lg-12"><b>Detail Test</b></span>
        </div>
      </div>
      <div class="form-group row mb-1">
        <div id="f_pemeriksaan_tujuan_id_error" class="form_error"></div>
        <label for="_102_f_pemeriksaan_tujuan_id" class="col-lg-12 col-form-label pb-1">Inspection purpose</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_pemeriksaan_tujuan_id" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_pemeriksaan_tujuan_id" comboname="f_pemeriksaan_tujuan_id"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input112" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Choose Inspection purpose" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_pemeriksaan_tujuan_id" value="3"></span>
        </div>
      </div>
    
      <div id="form_detail" class="d-none d-block">
      <div class="form-group row mb-1">        
        <div id="f_detail_tujuan_id_error" class="form_error"></div>
        <label for="_102_f_detail_tujuan_id" class="col-lg-12 col-form-label pb-1">Detail</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_detail_tujuan_id" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_detail_tujuan_id" comboname="f_detail_tujuan_id"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input118" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Choose detail" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_detail_tujuan_id" value="1"></span>
        </div>
      </div>
      </div>

      <div class="form-group row mb-1">
        <div id="f_status_error" class="form_error"></div>
        <label for="_102_f_status" class="col-lg-12 col-form-label pb-1">Status</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_status" style="width: 100%; display: none;" class="combobox-f combo-f textbox-f" textboxname="f_status" comboname="f_status"><span class="textbox easyui-fluid combo" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input110" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="Choose Status" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_status" value="sSwab"></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="swabber_name">
        <div id="f_swabber_name_error" class="form_error"></div>
        <label for="_102_f_swabber_name" class="col-lg-12 col-form-label pb-1">Swabber Name</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_swabber_name" style="width: 100%; display: none;" class="textbox-f" textboxname="f_swabber_name"><span class="textbox easyui-fluid textbox-invalid" style="width: 370px;"><input id="_easyui_textbox_input119" type="text" class="textbox-text validatebox-text validatebox-invalid textbox-prompt" autocomplete="off" tabindex="" placeholder="Swabber name" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_swabber_name" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="swab_dt">
        <div id="f_swab_dt_error" class="form_error"></div>
        <label for="_102_f_swab_dt" class="col-lg-12 col-form-label pb-1">Swab Date</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_swab_dt" style="width: 100%; display: none;" class="combo-f textbox-f datetimebox-f" textboxname="f_swab_dt" comboname="f_swab_dt"><span class="textbox easyui-fluid combo datebox" style="width: 370px;"><span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;"><a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a></span><input id="_easyui_textbox_input96" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Y-M-D" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 342px;"><input type="hidden" class="textbox-value" name="f_swab_dt" value="2022-04-04 11:23:48"></span>
        </div>
      </div>
      
      
      <div class="form-group row mb-1" id="patient_no">
        <div id="f_patient_no_error" class="form_error"></div>
        <label for="_102_f_patient_no" class="col-lg-12 col-form-label pb-1">Pasien No.</label>
        <div class="col-lg-9 col-9">
          <input type="text" id="_102_f_patient_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_patient_no"><span class="textbox easyui-fluid" style="width: 271px;"><input id="_easyui_textbox_input98" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="No. Pasien" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 269px;"><input type="hidden" class="textbox-value" name="f_patient_no" value="00213273.00007437"></span>
        </div>
        <div class="col-lg-3 col-3">
          <div class="btn btn-sm btn-block btn-info" onclick="_jf_102_grid01_generate()">Get No.</div>
        </div>
      </div>
      <div class="form-group row mb-1" id="lab_no" style="display: none;">
        <div id="f_klab_patient_no_error" class="form_error"></div>
        <label for="_102_f_klab_patient_no" class="col-lg-12 col-form-label pb-1">Lab. No.</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_klab_patient_no" style="width: 100%; display: none;" class="textbox-f" textboxname="f_klab_patient_no"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input99" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Lab. No." style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_klab_patient_no" value=""></span>
        </div>
      </div>
      <div class="form-group row mb-1" id="no_external">
        <div id="f_patient_no_external_error" class="form_error"></div>
        <label for="_102_f_patient_no_external" class="col-lg-12 col-form-label pb-1">External No.</label>
        <div class="col-lg-12">
          <input type="text" id="_102_f_patient_no_external" style="width: 100%; display: none;" class="textbox-f" textboxname="f_patient_no_external"><span class="textbox easyui-fluid" style="width: 370px;"><input id="_easyui_textbox_input100" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Ext. No." style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 368px;"><input type="hidden" class="textbox-value" name="f_patient_no_external" value=""></span>
        </div>
      </div>
    </div>
  </form>

  

</div>

<script type="text/javascript">
var loop = 0;


  $(function(){
    // $("#_102_f_plato_id").textbox({
    //   prompt:"Plato ID",
    //   required:false,
    // }).textbox('textbox').attr('maxlength',20)
    $("#_102_f_branch_id").combobox({
      required:true,
      editable:false,
      valueField:"f_sub_branch_id",
      textField:"f_branch_name_custom",
      panelHeight:200,
      method: "GET",
      data:[{"f_branch_ref_id":43,"f_branch_id":20,"f_sub_branch_id":20,"f_start_dt":null,"f_end_dt":null,"f_delete":0,"f_entry_by":null,"f_entry_dt":null,"f_modify_by":null,"f_modify_dt":null,"f_branch_owner_id":1,"f_branch_name":"Taishan Alkes Indonesia","f_branch_link":0,"f_branch_name_custom":"Taishan Alkes Indonesia","selected":true,"f_swabber_name_custom":"Taishan Alkes Indonesia"}],

        onChange: function(index, row) {
            var sel = $("#_102_f_branch_id").combobox("getValue");
            $("#_102_f_branch_product_id").combogrid({
                url: "{{url('pcr/getbranch')}}",
            })
        },
    })

    $("#_102_valid_address").checkbox({
      labelPosition:'before',
      labelWidth:100,
      checked:false,
      onChange: function(newValue, oldValue){
        readonly_valid_address(newValue);
      }
    })

    $("#_102_valid_biodata").checkbox({
      labelPosition:'before',
      labelWidth:100,
      checked:false,
      onChange: function(newValue, oldValue){
        readonly_valid_biodata(newValue);
      }
    })

    $("#_102_f_identitas_2").textbox({
      prompt:"PASPORT",
      required:false,
    })

    $("#_102_f_nationality_code").combobox({
      prompt:"Nationality Code",
      required:true,
      editable:true,
      valueField:"f_value",
      textField:"f_text",
      panelHeight:150,
      panelWidth:200,
      method: "GET",
      url : "{{url('public/get_nationality.json')}}",
      onSelect: function(rec){
        $("#_102_f_nationality").textbox('setValue',rec.f_nationality_name);

        var group = 3;
        if(rec.f_nationality_name=="Indonesia"){
          group = 1;
        }else if(rec.f_nationality_name=="South Korea"){
          group = 2;
        }

        $("#_102_f_nationality_group").combobox('setValue',group);
      },
    })

    $("#_102_f_id_type").combobox({
      required:true,
      editable:false,
      valueField:"f_value",
      textField:"f_text",
      panelHeight:100,
      data:[
        {"f_text":"NIK","f_value":"0","selected":true},
        {"f_text":"PASPORT","f_value":"2"},
        {"f_text":"NIK & PASPORT","f_value":"3"},
      ],
      onLoadSuccess:function(){
        // $("#identity_2").hide();
      },
      onChange:function(newValue, oldValue){
        // $("#identity_2").hide();
        $("#_102_f_identitas_2").textbox({
          prompt:"PASPORT",
          required:false,
        })
        
        $("#_102_f_identitas_2").textbox("setValue","");

        if(newValue!=2){
          $("#field_nik").removeClass("col-lg-9");
          $("#field_nik").addClass("col-lg-6");
          // $("#nationality_code").hide();
          $("#_102_f_nationality_code").combobox({required:false}).combobox("setValue","ID");
        }else{
          $("#field_nik").removeClass("col-lg-9");
          $("#field_nik").addClass("col-lg-6");

          $("#nationality_code").show();
          $("#_102_f_nationality_code").combobox({required:true}).combobox("setValue","");
        }


        if(newValue==0){
          $("#_102_f_id_no").textbox({
            prompt:"NIK"
          });

          $("#label_identitas").html("NIK");
          $("#identity_2").hide();
        }else if(newValue==2){
          $("#_102_f_id_no").textbox({
            prompt:"PASPORT"
          });

          $("#label_identitas").html("PASPORT");
          $("#identity_2").hide();
        }else if(newValue==3){
          $("#_102_f_id_no").textbox({
            prompt:"NIK"
          });

          $("#label_identitas").html("NIK");
          $("#identity_2").show();

          $("#_102_f_identitas_2").textbox({
            prompt:"PASPORT",
            required:true,
          })
        }
      }
    })

    $("#_102_f_id_no").textbox({
      prompt:"NIK",
      required:true,

      onChange : function(newValue,oldValue){
        if(loop>0){
          // cek_nik(index);
          var identity_type = $("#_102_f_id_type").combobox("getValue");

          if(identity_type!=2 && newValue.length!=16){
            alert("NIK length must be 16 digits");
          }

        }
        loop++;
      }
    }).textbox('textbox').attr('maxlength',20)

    $("#_102_f_nationality").textbox({
      prompt:"Nationality",
    })

    $("#_102_f_nationality_group").combobox({
      prompt:"Nationality",
      required:true,
      editable:false,
      valueField:"f_nationality_group",
      textField:"f_text",
      panelHeight:150,
      data:[
          {f_text:"Indonesia",f_nationality_group:"1"},
          {f_text:"Republic of Korea",f_nationality_group:"2"},
          {f_text:"Others",f_nationality_group:"3"},
      ],
      onChange: function(newVal, oldVal) {
        // if(newVal==3){
        //   $("#others_nationality").show();
        //   $("#_102_f_nationality").textbox({required:true});
        // }else{
        //   $("#others_nationality").hide();
        //   $("#_102_f_nationality").textbox({required:false});
        // }
      },
    }).combobox("setValue",1);

    $("#others_nationality").hide();

    $("#_102_f_swab_dt").datetimebox({
      prompt:"Y-M-D",
      required:true,
      editable:false,
      onChange:function(newValue, oldValue){
          var min_dt = Date.parse(newValue)+57600000;
          var max_dt = Date.parse(newValue)+86400000;

          var date_min = new Date(min_dt).toLocaleString('sv-SE', { timeZone: "Asia/Jakarta" });
          var date_max = new Date(max_dt).toLocaleString('sv-SE', { timeZone: "Asia/Jakarta" });

          var requirment = " ( Min."+date_min+"  |  Max."+date_max+" )";

          $("#range_date").html(requirment);
          // alert(d.setTime(tes));
      }
    })

    $("#_102_f_custom_result_dt").datetimebox({
      prompt:"Y-M-D h:i:s",
      editable:false,
      onChange:function(newValue, oldValue){
        diff_date_result(newValue);
      }
    })

    $("#_102_f_custom").combobox({
      prompt:"Choose custom result date",
      required:false,
      editable:false,
      valueField:"f_value",
      textField:"f_text",
      panelHeight:100,
      data:[
          {f_text:"Default result date",f_value:"0", selected:"selected"},
          {f_text:"Custom result date",f_value:"1"}
      ],
      onChange: function(newVal, oldVal) {
        if(newVal==1){
          $("#custom_result_dt").show();
          $("#_102_f_custom_result_dt").datetimebox({required:true});
        }else{
          $("#custom_result_dt").hide();
          $("#_102_f_custom_result_dt").datetimebox({required:false}).datetimebox("setValue","0000-00-00 00:00:00");
        }
      },
    })

    $("#custom_result_dt").hide();


    $("#_102_f_patient_no").textbox({
      prompt:"No. Pasien",
      required:false,
    })

    $("#_102_f_klab_patient_no").textbox({
      prompt:"Lab. No.",
      required:false,
    })

    $("#_102_f_patient_no_external").textbox({
      prompt:"Ext. No.",
      required:false,
    })

    $("#_102_f_product_price").numberbox({
      prompt:"Price",
      required:true,
    })


    $("#_102_f_branch_product_id").combogrid({
      disabled: false,
      editable: true,
      required: true,
      pagination: true,
      pageList: [5, 10, 20, 25, 50, 100],
      pageSize: 100,
      panelWidth : 400,
      // fit:true,
      prompt: "Product Branch",
      fitColumns: false,
      rownumbers: true,
      singleSelect: true,
      method: "GET",
      idField: "f_branch_product_id",
      textField: 'f_product_name',
      // url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_data_branch_product",
      // nowrap:false,
      mode: 'remote',
      delay: 500,
      columns: [
          [
              {field:"f_product_name",title:"<b>Product</b>",align:"center",width:125, sortable:true},
              {field:"f_product_price",title:"<b>Price</b>",align:"center",width:175, sortable:true},              
          ]
      ],

      onChange: function(index, row) {
          var sel = $("#_102_f_branch_product_id").combogrid('grid').datagrid("getSelected");
          var date = $("#_102_f_pcr_dt").datetimebox("getValue");
          // alert(JSON.stringify(sel));

          if (sel) {
              var id = sel.f_branch_product_id;
              var price = sel.f_product_price;
              $("#_102_f_shift_id").combogrid({
                  url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_data_shift?q="+id+"&date="+date,
              });
              $("#_102_f_product_price").numberbox("setValue",price);
          } else {
              $("#_102_f_shift_id").combogrid({
                  url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_data_shift?q=0",
              })
          }
      },

      // onChange: function(index, row) {
      //     get_shift(index);
      // },
    });

    $("#_102_f_shift_id").combogrid({
      disabled: false,
      editable: true,
      required: true,
      pagination: true,
      pageList: [5, 10, 20, 25, 50, 100],
      pageSize: 100,
      panelWidth : 400,
      // fit:true,
      prompt: "Shift",
      fitColumns: false,
      rownumbers: true,
      singleSelect: true,
      method: "GET",
      idField: "f_shift_id",
      textField: 'f_shift_name',
      // url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_data_shift",
      // nowrap:false,
      mode: 'remote',
      delay: 500,
      columns: [
          [
              {field:"f_shift_name",title:"<b>SHIFT</b>",align:"center",width:125, sortable:true},
              {field:"f_shift_day",title:"<b>SHIFT DAY</b>",align:"center",width:175, sortable:true},
              {field:"f_shift_from", title: "<b>Shift From</b>", align: "left", width: 150,sortable: true},
              {field:"f_shift_to", title: "<b>Shift To</b>", align: "left", width: 150,sortable: true},
          ]
      ],

      // onChange: function(index, row) {
      //     get_shift(index);
      // },
    });

    $("#_102_f_rt").textbox({
      prompt:"RT",
      required:false,
    });
    $("#_102_f_rw").textbox({
      prompt:"RW",
      required:false,
    });

    $("#_102_f_address").textbox({
      prompt:"Alamat...",
      required:true,
      multiline:true,
      height: 60
    });
    $("#_102_f_name").textbox({
      prompt:"Patient Fullname",
      required:true,
    }).textbox('textbox').attr('maxlength',100)
    $("#_102_f_dob").datebox({
      formatter:datebox_formatter_ddmmyyyy,
      parser:datebox_parser_ddmmyyyy,
      prompt:"dd/mm/yyyy",
      required:true,
      editable:true,
    })
    $("#_102_f_gender").combobox({
      prompt:"Select Gender",
      required:true,
      editable:false,
      valueField:"f_value",
      textField:"f_text",
      panelHeight:'auto',
      method: "GET",
      data:[
          {f_value:"M", f_text:'Male'},
          {f_value:"F", f_text:'Female'},
      ]
    })
    $("#_102_f_relation").combobox({
      prompt:"Select Relation",
      required:false,
      editable:false,
      valueField:"f_value",
      textField:"f_text",
      panelHeight:'auto',
      method: "GET",
      data:[
          {f_value:1, f_text:'Owner'},
          {f_value:2, f_text:'Child'},
      ]
    })
    $("#_102_f_duration").combobox({
      prompt:"Select Duration",
      required:false,
      editable:false,
      valueField:"f_value",
      textField:"f_text",
      panelHeight:'auto',
      method: "GET",
      data:[
          // {f_value:6, f_text:'6'},
          // {f_value:12, f_text:'12'},
          {f_value:24, f_text:'24'},
      ]
    })
    $("#_102_f_pcr_dt").datetimebox({
      prompt:"Y-M-D",
      required:false,
      editable:false,

      onChange: function(index, row) {
          var sel = $("#_102_f_branch_product_id").combobox("getValue");
          var date = $("#_102_f_pcr_dt").datetimebox("getValue");
          // alert(sel);

          if (sel) {
              $("#_102_f_shift_id").combogrid({
                  url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_data_shift?q="+sel+"&date="+date,
              });
              // $("#_102_f_product").textbox("setValue",r.data.f_ni)
          } else {
              $("#_102_f_shift_id").combogrid({
                  url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_data_shift?q=0",
              })
          }
      },
    })
    // $("#_102_f_swabber_name").textbox({
    //   prompt:"Swabber Name",
    //   required:false,
    // }).textbox('textbox').attr('maxlength',100)
    $("#_102_f_phone_no").textbox({
        prompt:"62XXXXXXXXXXX",
        required:true,
    })
    $("#_102_f_email").textbox({
      prompt:"Email",
      required:false,
    }).textbox('textbox').attr('maxlength',50)
    $("#_102_f_corporate_name").textbox({
      prompt:"Corporate Name",
      required:false,
    })
    $("#_102_f_specimen").combobox({
      prompt:"Speciment",
      required:false,
      editable:false,
      valueField:"f_value",
      textField:"f_text",
      panelHeight:'auto',
      method: "GET",
      data:[
          {f_value:'Nasofaring', f_text:'Nasofaring'},
          {f_value:'Orofaring', f_text:'Orofaring'},
          {f_value:'Naso-Orofaring', f_text:'Naso-Orofaring'},
      ]
    })

    $("#_102_f_status").combobox({
      prompt:"Choose Status",
      required:true,
      editable:true,
      valueField:"f_value",
      textField:"f_name",
      panelHeight: 'auto',
      method: "GET",
      url : "{{url('public/get_status.json')}}",
        onLoadSuccess: function(){
          $("#_102_f_status").combobox("setValue","sSwab");
        },
        onChange: function(index, row) {
            var sel = $("#_102_f_status").combobox("getValue");

            if (sel == 'sPresent') {
                // $("#swab_dt").addClass('d-none');
                // $("#swabber_name").addClass('d-none');
                // $("#patient_no").addClass('d-none');

                $("#swab_dt").hide();
                $("#swabber_name").hide();
                $("#patient_no").hide();

                $("#_102_f_swabber_name").textbox({
                  required:false,
                })
            }else if(sel == 'sSwab') {
                // $("#swab_dt").removeClass('d-none');
                // $("#swabber_name").removeClass('d-none');
                // $("#patient_no").removeClass('d-none');
                
                $("#swab_dt").show();
                $("#swabber_name").show();
                $("#patient_no").show();

                $("#_102_f_swabber_name").textbox({
                  required:true,
                })
            }else{

            }
        },
    })

    $("#_102_f_detail_tujuan_id").combobox({
      prompt:"Choose detail",
      required:false,
      editable:false,
      valueField:"f_value",
      textField:"f_name",
      panelHeight: 'auto',
      method: "GET",
      // url : "https://klab-ref.sislabplus.com/pcr-test-v3/get_detail",
    })

    $("#form_detail").addClass('d-none');


    $("#_102_f_pemeriksaan_tujuan_id").combobox({
      prompt:"Choose Inspection purpose",
      required:true,
      editable:false,
      valueField:"f_value",
      textField:"f_name",
      panelHeight: 'auto',
      method: "GET",
      url : "{{url('public/get_tujuan.json')}}",
      onChange: function(newData, oldData) {
        if(newData=="3"){
          $("#form_detail").addClass('d-block');

          $("#_102_f_detail_tujuan_id").combobox({
            url : "{{url('public/get_detail.json')}}",
          }).combobox('setValue', '1');
        }else{
          $("#form_detail").removeClass('d-block');
          $("#form_detail").addClass('d-none');
        }
      },
    })

    $("#_102_f_swabber_name").textbox({
      prompt:"Swabber name",
      required:false,
      // editable:true,
      // valueField:"f_swabber_name_custom",
      // textField:"f_swabber_name_custom",
      // panelHeight: 'auto',
      // method: "GET",
      // data:[{"f_branch_ref_id":43,"f_branch_id":20,"f_sub_branch_id":20,"f_start_dt":null,"f_end_dt":null,"f_delete":0,"f_entry_by":null,"f_entry_dt":null,"f_modify_by":null,"f_modify_dt":null,"f_branch_owner_id":1,"f_branch_name":"Taishan Alkes Indonesia","f_branch_link":0,"f_branch_name_custom":"Taishan Alkes Indonesia","selected":true,"f_swabber_name_custom":"Taishan Alkes Indonesia"}],

    })

    $("#_102_f_provinsi_code").combobox({
      prompt:"Provinsi",
      required:true,
      editable:true,
      valueField:"f_value",
      textField:"f_name",
      panelHeight:200,
      method: "GET",
      url : "{{url('public/get_province.json')}}",

        onChange: function(index, row) {
            var sel = $("#_102_f_provinsi_code").combobox("getValue");

            if (sel) {
                $("#_102_f_kota_code").combobox({
                    url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kabupaten?id_prov="+sel,
                    method: "GET",
                });
            } else {
                $("#_102_f_kota_code").combobox({
                    url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kabupaten?id_prov=0",
                    method: "GET",
                })
            }
            readonly_valid_address(false);
        },
    })

    $("#_102_f_kota_code").combobox({
      prompt:"Kabupaten/Kota",
      required:true,
      editable:true,
      valueField:"f_value",
      textField:"f_name",
      panelHeight:200,
      method: "GET",
      // url : "https://klab-ref.sislabplus.com/pcr-test-v3/get_kabupaten",

        onChange: function(index, row) {
            var sel = $("#_102_f_kota_code").combobox("getValue");

            if (sel) {
                $("#_102_f_kecamatan_code").combobox({
                    url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kecamatan?q="+sel,
                    method: "GET",
                });
            } else {
                $("#_102_f_kecamatan_code").combobox({
                    url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kecamatan?q=0",
                    method: "GET",
                })
            }
            readonly_valid_address(false);
        },
    })
    $("#_102_f_kecamatan_code").combobox({
      prompt:"Kecamatan",
      required:true,
      editable:true,
      valueField:"f_value",
      textField:"f_name",
      panelHeight:200,
      method: "GET",
      // url : "https://klab-ref.sislabplus.com/pcr-test-v3/get_kabupaten",

        onChange: function(index, row) {
            var sel = $("#_102_f_kecamatan_code").combobox("getValue");

            if (sel) {
                $("#_102_f_kelurahan_code").combobox({
                    url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kelurahan?id_kec="+sel,
                    method: "GET",
                });
            } else {
                $("#_102_f_kelurahan_code").combobox({
                    url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kelurahan?id_kec=0",
                    method: "GET",
                })
            }
            readonly_valid_address(false);
        },
    })
    $("#_102_f_kelurahan_code").combobox({
      prompt:"Kelurahan",
      required:true,
      editable:true,
      valueField:"f_value",
      textField:"f_name",
      panelHeight:200,
      method: "GET",
      // url : "https://klab-ref.sislabplus.com/pcr-test-v3/get_kabupaten",

      onChange: function(index, row) {
          // var sel = $("#_102_f_kecamatan_code").combobox("getValue");

          // if (sel) {
          //     $("#_102_f_kelurahan_code").combogrid({
          //         url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kelurahan?q="+sel,
          //         method: "GET",
          //     });
          // } else {
          //     $("#_102_f_kelurahan_code").combogrid({
          //         url: "https://klab-ref.sislabplus.com/pcr-test-v3/get_kelurahan?q=0",
          //         method: "GET",
          //     })
          // }
          readonly_valid_address(false);
      },
    })

    $("#group_nationality").hide();
    $("#others_nationality").hide();
    
  });

  function diff_date_result(newDate){
    if(newDate!=""){
      var swab_dt = $("#_102_f_swab_dt").datetimebox("getValue");
      // alert(swab_dt);
      var swab_sec = Date.parse(swab_dt) / 1000;
      var result_sec = Date.parse(newDate) / 1000;

      var diff = result_sec-swab_sec;
      var result_min = 57600;//(60*60)*16
      var result_max = 86400;//(60*60)*24

      if(diff<result_min || diff>=result_max){
        alert("Custom result of at least 16 hours and maximal 24 hours from swab date!");
        $("#_102_f_custom_result_dt").datetimebox("setValue","");
      }
    }

  }

  function readonly_valid_address(param=false){
    if(param==true){
      $("#_102_f_provinsi_code").combobox('readonly',true);
      $("#_102_f_kota_code").combobox('readonly',true);
      $("#_102_f_kecamatan_code").combobox('readonly',true);
      $("#_102_f_kelurahan_code").combobox('readonly',true);
      $("#_102_f_address").combobox('readonly',true);
      $("#_102_f_rt").textbox('readonly',true);
      $("#_102_f_rw").textbox('readonly',true);
    }else{
      $("#_102_f_provinsi_code").combobox('readonly',false);
      $("#_102_f_kota_code").combobox('readonly',false);
      $("#_102_f_kecamatan_code").combobox('readonly',false);
      $("#_102_f_kelurahan_code").combobox('readonly',false);
      $("#_102_f_address").combobox('readonly',false);
      $("#_102_f_rt").textbox('readonly',false);
      $("#_102_f_rw").textbox('readonly',false);
    }
  }

  function readonly_valid_biodata(param){
    if(param){
      $("#_102_f_name").textbox('readonly',true);
      $("#_102_f_dob").datebox('readonly',true);
      $("#_102_f_gender").combobox('readonly',true);
    }else{
      $("#_102_f_name").textbox({
        required:true
      }).textbox('readonly',false);
      $("#_102_f_dob").datebox('readonly',false);
      $("#_102_f_gender").combobox('readonly',false);
    }
  }

  function cek_nik_detail(){
    var nik = $("#_102_f_id_no").textbox("getValue");
    var nationality_code = $("#_102_f_nationality_code").combobox("getValue");
    var id_type = $("#_102_f_id_type").combobox("getValue");
    var my_modal_id = "_102_grid01_dialogg";
    var my_modal_size = "";
    var my_modal_url = "https://klab-ref.sislabplus.com/pcr-test-v3/cek_nik_detail?my_modal_id=_102_grid01_dialog&f_nik="+nik+"&f_nationality_code="+nationality_code+"&f_id_type="+id_type;
    var my_modal_title = "Check NIK / PASPOR";
    my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
    $('#'+my_modal_id).modal({
      show:true,
      backdrop:'static'
    });
  }

  function _jf_102_grid01_generate(generate){
    // alert(generate_number);
    preloader_block();
    $.ajax({
      type: 'GET',
      // data: JSON.stringify(generate),
      dataType: "JSON",
      url:"{{url('pcr/generate_code')}}",
      success: function(r) {
          // $.messager.alert("Swab pasien", r.message);
          if (r.success) {
            $("#_102_f_patient_no").textbox('setValue',r.data);
          }
          preloader_none();
      },
      error:function(){
          $.messager.alert("Generate No Patient", "Request error, please try again.");
          preloader_none();
      }
    })
  }

function cek_nik(data){

// if(tipe_identitas==0){
  
  $.ajax({
    type: 'GET',
    dataType: "JSON",
    // url:"https://klab-ref.sislabplus.com/pcr-test-v3/get_nik/"+data.value,
    url:"https://klab-ref.sislabplus.com/pcr-test-v3/nik_check/"+data,
    success: function(r) {
      if (r.success) {

        var dataNik = r.dataNik;
        // alert(JSON.stringify(dataNik));
        if(dataNik.dataNar){
          $("#_102_f_name").textbox('setValue',dataNik.f_name);
        }
        $("#_102_f_provinsi_code").combobox('setValue', dataNik.f_provinsi_code);
        $("#_102_f_kota_code").combobox('setValue', dataNik.f_kota_code);
        $("#_102_f_kecamatan_code").combobox('setValue', dataNik.f_kecamatan_code);
        $("#_102_f_kelurahan_code").combobox('setValue', dataNik.f_kelurahan_code);

        $("#_102_f_gender").combobox('setValue',dataNik.f_gender);
        // $("#_102_f_gender").val(dataNik.f_gender);
        
        $("#_102_f_dob").datebox('setValue',dataNik.f_dob);
        // $("#_102_f_dob").val(dataNik.f_dob);

        // $.messager.alert("NIK",r.message);

      }else{
        // $.messager.alert("NIK",r.message);
      }
    }
  })
  
// }

}

  function get_shift($f_branch_product_id){
    // alert($f_branch_product_id);
    $.ajax({
      type: 'GET',
      dataType: "JSON",
      url:"https://klab-ref.sislabplus.com/pcr-test-v3/get_data_shift?q="+$f_branch_product_id,
      success: function(r) {
        if (r.success) {
          // alert(JSON.stringify(r.data));
          // $("#_102_f_id_no").textbox("setValue",r.data.f_nik)
          // $("#_102_f_name").textbox("setValue",r.data.f_name)
          // $("#_102_f_dob").datebox("setValue",r.data.f_dob)
          // $("#_102_f_gender").combobox("setValue",r.data.f_gender)
          // $("#_102_f_phone_no").numberbox("setValue",r.data.f_phone_no)
          // $("#_102_f_relation").combobox("setValue",r.data.f_relation)
        }
      },
      // error:function(){
      //   $.messager.alert("Delete Patient", "Request error, please try again.");
      //   preloader_none();
      // }
    })
  }

  function _jf_102_grid02_create(){
    preloader_block();
    $("#_102_grid02_form").form("submit", {
      url: "https://klab-ref.sislabplus.com/pcr-test-v3/grid02_create",
      onSubmit: function() {
        if(!$(this).form('validate')){
            preloader_none();
            return $(this).form('validate');
        }
      },
      success: function(res) {
          var r = JSON.parse(res);

          $.messager.alert("Handover Batch", r.message);
          $(".form_error").html("");

          if (r.success) {
              // $("#_102_grid02_dialog").dialog('close');
              $("#_102_grid02_dialog").modal('hide');
              $("#_102_grid01").datagrid('reload');
              $("#_102_grid02").datagrid('reload');
              $("#_102_grid02").datagrid('clearSelections');
              $("#_102_grid02").datagrid('clearChecked');
          } else {
              if (r.form_error) {
                  var form_error_array = r.form_error_array;
                  for (key in form_error_array) {
                    for (key2 in form_error_array[key]) {
                        if(key2 == 0){
                            $("#" + key + "_error").append(form_error_array[key][key2]);
                        }else{
                            $("#" + key + "_error").append("<br>"+form_error_array[key][key2]);
                        }
                    }
                  }
              }
          }
          preloader_none();
      },
      error:function(){
        $.messager.alert("Form Input", "Process Failed..");
        preloader_none();
      }
    })
  }
</script>

<script>
    $(function(){
                    loop++;
            // no action
            // $("#swab_dt").addClass('d-none');
            // $("#patient_no").addClass('d-none');
            // $("#swabber_name").addClass('d-none');

            // $("#swab_dt").hide();
            // $("#patient_no").hide();
            // $("#swabber_name").hide();

            var currentdate = new Date(); 
            var datetime = currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

            // alert(datetime);

            $("#_102_f_swab_dt").datetimebox("setValue", datetime);
            $("#_102_f_pemeriksaan_tujuan_id").combobox("setValue",3);
            $("#_102_f_detail_tujuan_id").combobox("setValue",1);

            $("#lab_no").hide();

            _jf_102_grid01_generate();
            })
</script>
</div><div class="modal-footer">
      <a href="javascript:void(0)" class="btn btn-block btn-md btn-primary" onclick="_jf_102_grid02_create()">Save</a>
  </div></div></div></div></div>