    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mr-auto "><a class="navbar-brand" href="{{ route('dashboard')}}">
            <span class="brand-logo">
              <img src="{{ get_logo() }}">
             </span>
              <h2 class="brand-text">{{ get_name()}}</h2></a></li>
          <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
      </div>
      <div class="shadow-bottom"></div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class=" nav-item {{ (request()->segment(1) == 'dashboard') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('dashboard')}}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboards</span></a>
          </li>
          @if (get_data_login()->role=="mitra" || get_data_login()->role=="branch" || get_data_login()->role=="admin") 
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Menu Mitra</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ (request()->is('pcr')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pcr') }}"><i data-feather="credit-card"></i><span class="menu-title text-truncate" data-i18n="Card">Swab PCR Test</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('qrpatient')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{  route('qrpatient')}}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Todo">QR Patient (VTM)</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('kwitansi')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{  route('kwitansi')}}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">Kwitansi</span></a>
            </li>
          @endif
          @if (get_data_login()->role=="labekstraksi" || get_data_login()->role=="admin")          
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Lab Ekstraksi</span><i data-feather="more-horizontal"></i>
            </li>
            <li class=" nav-item {{ (request()->is('pcrdikirim')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pcrdikirim') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">List Sample Dikirim</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('pcrditerima')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pcrditerima') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">List Sample Diterima</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('pcrprosessekstraksi')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pcrprosessekstraksi') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">List Sample Prosess Ekstraksi</span></a>
            </li>
          @endif
          @if (get_data_login()->role=="labpcr" || get_data_login()->role=="admin")
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Lab PCR</span><i data-feather="more-horizontal"></i>
            </li>
            <li class=" nav-item {{ (request()->is('listpcr')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('listpcr') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">List Finish Ekstraksi</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('analizepcr')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('analizepcr') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">List Analize PCR</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('pcr_result')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pcr_result') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">PCR Result Machine</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('interpretasi')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('interpretasi') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Calendar">Interpretasi PCR</span></a>
            </li>
          @endif
          @if (get_data_login()->role=="admin")
            <li class=" navigation-header"><span data-i18n="User Interface">Pengaturan</span><i data-feather="more-horizontal"></i>
            </li>
            <li class=" nav-item {{ (request()->is('setting')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('setting')}}"><i data-feather="settings"></i><span class="menu-title text-truncate" data-i18n="Typography">Setting Program</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('mitra')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('mitra')}}"><i data-feather="settings"></i><span class="menu-title text-truncate" data-i18n="Colors">Setting Mitra</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('branch')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('branch')}}"><i data-feather="settings"></i><span class="menu-title text-truncate" data-i18n="Typography">Branch</span></a>
            </li>
            <li class=" nav-item {{ (request()->is('user')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('user')}}"><i data-feather="settings"></i><span class="menu-title text-truncate" data-i18n="Typography">Setting User</span></a>
            </li>
          @endif
        </ul>
      </div>
    </div>
    <!-- END: Main Menu-->
