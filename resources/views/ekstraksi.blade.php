@extends('app')

@section('content')
<section id="ekstraksi">
  <div class="row match-height">
      <div class="col-xl-12 col-md-6 col-12">
        <div class="easyui-layout" data-options="fit:true" style="width: 1237px; height: 304px;">
          <div data-options="region:'center', border:false, split:true">
            <div class="easyui-layout" data-options="fit:true">
              <div data-options="region:'center', split:true, title:'List PCR Result'" >
                <table id="_ekstraksi_grid"
                url="{{url('api/ekstraksi/getdata')}}",>
                </table>
                  <div id="_ekstraksi_grid_toolbar" class="p-1">
                      <div class="row">
                        <div class="col-12">
                          
                          <div style="float: left">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_ekstraksi_grid_import()"><i class="fa fa-plus fa-fw fa-lg" style="color:green"></i>&nbsp;Import Data</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_ekstraksi_grid_edit()"><i class="fa fa-edit fa-fw fa-lg" style="color:green"></i>&nbsp;Edit</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_ekstraksi_grid_delete()"><i class="fa fa-trash fa-fw fa-lg" style="color:green"></i>&nbsp;Delete</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_prosess_validasi()"><i class="fa fa-refresh fa-fw fa-lg" style="color:green"></i>&nbsp;Prosess Validasi</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-redo'" onclick="_jf_prosess_interpretasi_hasil_sampel()">&nbsp;Interpretasi Hasil Sampel</a>
                          </div>
                        </div>
                      </div>
              </div>
          </div>
        </div>
    </div>
  </div>

    <div id="_ekstraksi_grid_dialog" class="easyui-dialog" style="width:20%;height:220px;padding:10px 25px"
          closed="true" buttons="#dlg-buttons">
          <form method="post" action="{{url('/pcr_result/import_excel')}}" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <label>Pilih file excel</label>
                <input type="file" name="file" required="required">
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </form>
    </div>


    <div id="_ekstraksi_edit_dialog" class="easyui-dialog" style="width:20%;height:220px;padding:10px 25px"
        closed="true" buttons="#dlg-buttons">
      <form id="fme" method="post"  novalidate>
          <div class="form-group">
              <label>CT</label>
              <input type="text" name="ct" class="form-control" required="true">
          </div>
      </form>
    </div>
    <div id="dlg-buttons">
      <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveekstraksi()" style="width:90px">Save</a>
  </div>

</section>
<script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script>

<script type="text/javascript">
  var url;
  $(function(){

    $("#_ekstraksi_grid").datagrid({
      toolbar: "#_ekstraksi_grid_toolbar",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "id",
      frozenColumns: [[
        {field: "check",checkbox:true},
      ]],
      columns: [[
        {field: "id_prosess_excel",title: "<b>Ekstraksi ID</b>",align: "left",width: 150},
        {field: "hole",title: "<b>Hole</b>",align: "left",width: 50},
        {field: "channel",title: "<b>Channel</b>",align: "left",width: 85},
        {field: "ct",title: "<b>CT</b>",align: "left",width: 85},
        {field: "type",title: "<b>Type</b>",align: "left",width: 85},
        {field: "Purpose",title: "<b>Purpose</b>",align: "left",width: 85},
        {field: "group",title: "<b>Group</b>",align: "left",width: 85},
        {field: "sample",title: "<b>Sample</b>",align: "left",width: 85},
        {field: "target",title: "<b>Target</b>",align: "left",width: 85},
        {field: "gene",title: "<b>Gene</b>",align: "left",width: 95},
        {field: "posvalid",title: "<b>POS VALID</b>",align: "left",width: 145,
          formatter: function(value,row,index){
              if(value == 'false'){
                  return '<div class="bg-danger px-5 text-white" style="height:100%; width:100%;">INVALID</div>';
              }else if(value == 'true'){
                  return '<div class="bg-success px-5 text-white" style="height:100%; width:100%;">VALID</div>';
              }
              else{
                return'';
              }
          }
        },
        {field: "ntcvalid",title: "<b>NTC VALID</b>",align: "left",width: 145,
          formatter: function(value,row,index){
              if(value == 'false'){
                  return '<div class="bg-danger px-5 text-white" style="height:100%; width:100%;">INVALID</div>';
              }else if(value == 'true'){
                  return '<div class="bg-success px-5 text-white" style="height:100%; width:100%;">VALID</div>';
              }
              else{
                return'';
              }
          }
        },
      ]],
    })
  })


  function _jf_ekstraksi_grid_import(){
    $('#_ekstraksi_grid_dialog').dialog('open').dialog('setTitle','Import Ekstraksi');
    $('#fm').form('clear');
    url= "{{url('api/ekstraksi/store')}}";
  }
   
  
  function _jf_ekstraksi_grid_edit(){
    var row = $('#_ekstraksi_grid').datagrid('getSelected');
		if (row){
      $('#_ekstraksi_edit_dialog').dialog('open').dialog('setTitle','Edit Ekstraksi');
      $('#fme').form('load',row);
      url = "{{url('api/ekstraksi/update')}}/"+row.id;
    }
    else{
      $.messager.alert("Edit Ekstraksi","Select Data first.");
    }
  }

  function saveekstraksi(){
    $.ajax({
        type: "POST",
        url: url,
        data:$('#fme').serialize(),
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#_ekstraksi_edit_dialog').dialog('close'); 
                $('#_ekstraksi_grid').datagrid('reload');   
            }
        }
    });
  }

  function _jf_ekstraksi_grid_delete(){
    var sel = $("#_ekstraksi_grid").datagrid('getSelected');
    var check = $("#_ekstraksi_grid").datagrid('getChecked');
    if(sel){

      var patientIdList = [];
      patientIdList.push(sel.id);
      var message_confirm = "Delete Selected data ?";
      execute_delete(patientIdList,message_confirm);

    }else if(check.length>0){
      var patientIdList = [];
      check.forEach(function(value, index){
        patientIdList.push(value.id);
      });        
      var message_confirm = "Delete Checklist data ?";
      execute_delete(patientIdList,message_confirm);
    }else{
      alert("Please select or checklist data!");
    }
	}
 

  function execute_delete(patientIdList="", message_confirm=""){
    if(patientIdList!=""){
      $.messager.confirm("Delete Patient", message_confirm,function(r){
        if(r){
          preloader_block();
          $.ajax({
            type: 'DELETE',
            dataType: "JSON",
            url:"{{url('api/ekstraksi/delete?id=')}}"+JSON.stringify(patientIdList),
            success: function(r) {
              $.messager.alert("Delete", r.message);
              if (r.success) {
                $("#_ekstraksi_grid").datagrid('reload');
                $("#_ekstraksi_grid").datagrid('clearSelections');
                $("#_ekstraksi_grid").datagrid('clearChecked');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Delete Patient", "Request error, please try again.");
              preloader_none();
            }
          })
        }
      })
    }else{
      alert("Please select or checklist data!");
    }
  }
  function _jf_prosess_validasi(){
    var message_confirm = "Validasi Data data ?";
    $.messager.confirm("Validasi Data?", message_confirm,function(r){
      if(r){
        preloader_block();
          $.ajax({
            type: 'GET',
            dataType: "JSON",
            url:"{{url('api/ekstraksi/validasi')}}",
            success: function(r) {
              $.messager.alert("Validasi", r.message);
              if (r.success) {
                $("#_ekstraksi_grid").datagrid('reload');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Validasi", "Request error, please try again.");
              preloader_none();
            }
          })
      }
    })
  }

  function _jf_prosess_interpretasi_hasil_sampel(){
    var message_confirm = "Interpretasi Data data ?";
    $.messager.confirm("Interpretasi Data?", message_confirm,function(r){
      if(r){
        preloader_block();
          $.ajax({
            type: 'GET',
            dataType: "JSON",
            url:"{{url('api/ekstraksi/interpretasi')}}",
            success: function(r) {
              $.messager.alert("Validasi", r.message);
              if (r.success) {
                $("#_ekstraksi_grid").datagrid('reload');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Validasi", "Request error, please try again.");
              preloader_none();
            }
          })
      }
    })
  }
</script>
@endsection

