@extends('app')

@section('content')
<section id="antigent">
  <div class="row match-height">
      <div class="col-xl-12 col-md-6 col-12">
        <div class="easyui-layout" data-options="fit:true" style="width: 100%; height: 497px;">
            <div data-options="region:'center', border:false, split:true">
              <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'center', split:true, title:'Swab PCR Test'" >
                  <table id="_antigent_grid01"
                  url="",>
                  </table> 
            
                  <div id="_antigent_grid01_toolbar" class="p-1">
                    <div class="row">
                      <div class="col-12">
                        <div style="float:left">
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="iconCls:'icon-add',plain:true" onclick="_jf_102_grid02_add()" group="" id=""><span class="l-btn-left l-btn-icon-right"><span class="l-btn-text">Add</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="iconCls:'icon-edit',plain:true" onclick="_jf_102_grid02_edit()" group="" id=""><span class="l-btn-left l-btn-icon-right"><span class="l-btn-text">Edit</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="iconCls:'icon-remove',plain:true" onclick="_jf_102_grid02_delete()" group="" id=""><span class="l-btn-left l-btn-icon-right"><span class="l-btn-text">Delete</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="plain:true" onclick="_jf_antigent_grid01_add_exist()" group="" id=""><span class="l-btn-left"><span class="l-btn-text"><i class="fas fa-plus fa-fw fa-lg" style="color:orange"></i>&nbsp;Add-Exist</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="plain:true" onclick="_jf_antigent_grid01_confirm()" group="" id=""><span class="l-btn-left"><span class="l-btn-text"><i class="fas fa-check fa-lg fa-fw" style="color:green"></i>&nbsp;Confirm</span></span></a>
                          <a href="#" class="easyui-menubutton l-btn l-btn-small l-btn-plain m-btn m-btn-small" data-options="menu:'#_grid02_template_membership', plain:true, showEvent:'click'" group="" id=""><span class="l-btn-left"><span class="l-btn-text">Import Data</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="plain:true" onclick="_jf_102_grid03_export()" group="" id=""><span class="l-btn-left"><span class="l-btn-text"><i class="fas fa-file-excel fa-fw fa-lg" style="color:green"></i>&nbsp;Export Data</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="plain:true" onclick="_jf_102_grid03_export_fin()" group="" id=""><span class="l-btn-left"><span class="l-btn-text"><i class="fas fa-file-excel fa-fw fa-lg" style="color:green"></i>&nbsp;Export-Data-Finance</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="plain:true" onclick="_jf_102_grid02_antigene_result()" group="" id=""><span class="l-btn-left"><span class="l-btn-text"><i class="fas fa-edit fa-fw fa-lg" style="color:#15b3c2"></i>&nbsp;Update Result</span></span></a>
                          <a href="#" class="easyui-menubutton l-btn l-btn-small l-btn-plain m-btn m-btn-small" data-options="menu:'#_antigent_grid01_action', plain:true, showEvent:'click'" group="" id=""><span class="l-btn-left"><span class="l-btn-text">Print-Result</span><span class="m-btn-downarrow"></span><span class="m-btn-line"></span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="plain:true" onclick="_jf_102_grid02_search_scan_qr()" group="" id=""><span class="l-btn-left"><span class="l-btn-text"><i class="fas fa-qrcode fa-fw" style="color:#3572bd;"></i> Search-QR</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="iconCls:'icon-print',plain:true" onclick="_jf_102_grid02_print_label('tj103')" group="" id=""><span class="l-btn-right l-btn-icon-right"><span class="l-btn-text">Print-Label</span></span></a>
                          <a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" data-options="iconCls:'icon-print',plain:true" onclick="_jf_102_grid02_print_label_v2('label50x25mm')" group="" id=""><span class="l-btn-right l-btn-icon-right"><span class="l-btn-text">Label-50x25mm</span></span></a>
                        </div>

                        <div style="float:right">
                          <input type="text" id="_antigent_grid01_filter_status_code_front" class="combobox-f combo-f textbox-f" style="display: none;">
                          <span class="textbox combo" style="width: 161px;">
                              <span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;">
                                <a href="javascript:;" class="textbox-icon combo-arrow" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a>
                              </span>
                              <input id="_easyui_textbox_input1" type="text" class="textbox-text validatebox-text validatebox-readonly" autocomplete="off" tabindex="" readonly="" placeholder="Status" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 133px;"><input type="hidden" class="textbox-value" name="" value="sAllActive"></span>
                              <input type="text" id="_antigent_grid01_filter_srcevt_front" class="textbox-f" style="display: none;">
                              <span class="textbox" style="width: 161px;">
                                  <input id="_easyui_textbox_input2" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="Search All" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 159px;">
                                  <input type="hidden" class="textbox-value" value="">
                              </span>
                          </span>
                          <a href="#" class="easyui-menubutton l-btn l-btn-small l-btn-plain m-btn m-btn-small" data-options="menu:'#_antigent_grid01_filter_mb', iconCls:'icon-filter', plain:true" onclick="_jf_antigent_grid01_filter_o()" group="" id="">
                            <span class="l-btn-left l-btn-icon-left">
                              <span class="l-btn-text">Filter</span>
                              <span class="l-btn-icon icon-filter">&nbsp;</span>
                              <span class="m-btn-downarrow"></span>
                              <span class="m-btn-line"></span>
                            </span>
                          </a>
                          <a href="#" class="easyui-menubutton l-btn l-btn-small l-btn-plain m-btn m-btn-small" data-options="menu:'#_grid01_legend', plain:true" group="" id="">
                            <span class="l-btn-left">
                              <span class="l-btn-text">Status</span>
                              <span class="m-btn-downarrow"></span>
                              <span class="m-btn-line"></span>
                            </span>
                          </a>                 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>
</section>
<script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="{{ asset('public/modal.js')}}"></script>
<script src="{{ asset('public/script.js')}}"></script>
<script src="{{ asset('public/select2.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/html5-qrcode.min.js')}}"></script>
<script>
    $(function(){
      $.fn.datebox.defaults.formatter = function(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        var format_y = y;
        var format_m = (m<10?('0'+m):m);
        var format_d = (d<10?('0'+d):d);
        return format_y+"-"+format_m+"-"+format_d;
      };
      $.fn.datebox.defaults.parser = function(s){
        if(!s) return new Date();
        var ss = (s.split('-'));
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        var d = parseInt(ss[2],10);
        if(!isNaN(y) && !isNaN(m) && !isNaN(d)){
          return new Date(y,m-1,d);
        }else{
          return new Date();
        }
      };
      $.fn.datetimebox.defaults.formatter = function(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        var format_y = y;
        var format_m = (m<10?('0'+m):m);
        var format_d = (d<10?('0'+d):d);

        var h = date.getHours();
        var i = date.getMinutes();
        var s = date.getSeconds();
        var format_h = (h<10?('0'+h):h);
        var format_i = (i<10?('0'+i):i);
        var format_s = (s<10?('0'+s):s);
        // var s2 = [date.getHours(),date.getMinutes(),date.getSeconds()].join(':');
        var s2 = [h,i,s].join(':');

        return format_y+"-"+format_m+"-"+format_d+" "+s2;
      };
      $.fn.datetimebox.defaults.parser = function(s){
        if(!s) return new Date();

        var dt = s.split(' ');
        var ss = dt[0].split('-');
        var ss2 = dt[1].split(':');
        // var ss = (s.split('-'));
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        var d = parseInt(ss[2],10);
        if(!isNaN(y) && !isNaN(m) && !isNaN(d)){
          var date = new Date(y,m-1,d);
          date.setHours(ss2[0]);
          date.setMinutes(ss2[1]);
          date.setSeconds(ss2[2]);
          return date;
        }else{
          return new Date();
        }
      };
    })

    function datebox_formatter_ddmmyyyy(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
    }
    function datebox_parser_ddmmyyyy(s){
        if (!s) return new Date();
        var ss = (s.split('/'));
        var y = parseInt(ss[2],10);
        var m = parseInt(ss[1],10);
        var d = parseInt(ss[0],10);
        if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
            return new Date(y,m-1,d);
        } else {
            return new Date();
        }
    }

    function _jf_set_dialogButtons(id){
      var _dialogButtons = $("#_dialogButtons").html();
      $("#_dialogButtons").remove();
      $("#"+id).next().html(_dialogButtons);
    }
    function _jf_dialog_create(id, href, title = "Dialog Title", width = 500, height = 300, closable = true){
      var dialog = "<div id='"+id+"' class='easyui-dialog'></div>";
      $("#_dialog_section").append(dialog);

      $("#"+id).dialog({
        cls:"class_for_"+id,
        closed:true,
        modal:true,
        cache: false,
        title:title,
        width:width,
        height:height,
        closable: closable,
        href: href,
        buttons: [],
        onLoad:function(){
          var _dialogButtons = $("#_dialogButtons").html();
          $("#_dialogButtons").remove();
          $("#"+id).next().html(_dialogButtons);
        },
        onOpen:function(){
          var _dialogDisplay = $(".class_for_"+id).first().css('display');
          if(_dialogDisplay == 'block'){
            $("body").css("overflow","hidden");
          }else{
            $("body").css("overflow","");
          }
        },
        onClose:function(){
          $("#"+id).dialog('destroy');
          var _dialogDisplay = $(".class_for_"+id).first().css('display');
          if(_dialogDisplay == 'block'){
            $("body").css("overflow","hidden");
          }else{
            $("body").css("overflow","");
          }
        }
      });
    }

    function number_format(number, decimals, dec_point, thousands_sep) {
      number = (number + '').replace(',', '').replace(' ', '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
        };
        
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    }

    function preloader_block(){
      $(".preloader").css("display","block");
    }
    function preloader_none(){
      $(".preloader").css("display","none");
    }
    function preloader2_block(){
      $(".preloader2").css("display","block");
    }
    function preloader2_none(){
      $(".preloader2").css("display","none");
    }

    $(function(){
      $("#my_modal").modal({
        show:false,
        backdrop:'static'
      })
      my_modal_set();
    })
    
    function my_modal_set(modalSize = "modal-md", modalTitle = "Modal Title", modalBody = "No Content" ){
      var md = $("#my_modal .modal-dialog");
      md.removeClass();
      md.addClass("modal-dialog "+modalSize);

      $("#my_modal .modal-title").text(modalTitle);
      $("#my_modal .modal-body").text("");

      preloader_block()
      $("#my_modal .modal-body").load(modalBody,function(){
        preloader_none();
      });
    }
  </script>


<script type="text/javascript">
  var searchScanQR = 0;
  var searchScanQRValue = "";

  $(function(){
    setTimeout(function(){
      $("#_102_layout03").layout('collapse','west');
    },500)

    _jf_antigent_grid01_filter_i();

    $("#_antigent_grid01_filter_status_code_front").combobox({
        prompt:"Status",
        editable:false,
        panelHeight:"auto",
        valueField:"f_status_code",
        textField:"f_status_name",
        method: "GET",
        url:"-v2/status_data/filter",
        onChange:function(newValue, oldValue){
            $("#_antigent_grid01_filter_status_code").combobox('setValue',newValue);
            _jf_antigent_grid01_filter_p();
        }
    })

    $("#_antigent_grid01_filter_srcevt_front").textbox({
    prompt:'Search All',
    required:false,
    onChange:function(newValue, oldValue){
        $("#_antigent_grid01_filter_srcevt").textbox('setValue',newValue);
        $("#_antigent_grid01_filter_status_code_front").combobox('setValue','sAll');
        _jf_antigent_grid01_filter_p();
    }
    })

    $("#_antigent_grid01").datagrid({
      toolbar: "#_antigent_grid01_toolbar",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "f_patient_id",
      frozenColumns: [[
        {field: "check",checkbox:true},
      ]],
      columns: [[
        {field: "f_result_name",title: "<b>Result</b>",align: "center",width: 65,
          styler : function(value,row,index){
            return {class:'some-class', style:'color:white;background-color:'+row.f_result_color+';'}
          }
        },
        {field: "f_payment_status",title: "<b>Trans. Stat.</b>",align: "center",width: 80},
        {field: "f_handover_date_ddmmmyyyy",title: "<b>Date</b>",align: "center",width: 85},
        {field: "f_id_type_mod",title: "<b>Type</b>",align: "center",width: 65},
        {field: "f_nationality_code",title: "<b>Nationality</b>",align: "center",width: 65},
        {field: "f_nik",title: "<b>NIK</b>",align: "center",width: 140},
        {field: "f_identitas_2",title: "<b>Other Identity</b>",align: "center",width: 140},
        {field: "f_name",title: "<b>Fullname</b>",align: "left",width: 200},
        {field: "f_dob_ddmmmyyyy",title: "<b>Date Of Birth</b>",align: "center",width: 100},
        {field: "f_gender",title: "<b>Gender</b>",align: "center",width: 60},
        {field: "f_patient_no",title: "<b>Patient No.</b>",align: "center",width: 150},
        {field: "f_reg_type_name",title: "<b>Reg. Type</b>",align: "center",width: 100},
        {field: "f_corporate_name",title: "<b>Corporate</b>",align: "center",width: 250},
        {field: "f_product_name",title: "<b>Package</b>",align: "center",width: 100},
        {field: "f_payment_service",title: "<b>Payment Service</b>",align: "center",width: 100},
        {field: "f_payment_no",title: "<b>Payment No.</b>",align: "center",width: 100},
        {field: "f_phone_no",title: "<b>Phone/Telp</b>",align: "center",width: 150},
        {field: "f_email",title: "<b>Email</b>",align: "center",width: 150},
        {field: "f_address",title: "<b>Address / Alamat</b>",align: "center",width: 250},
        {field: "f_pemeriksaan_tujuan_name",title: "<b>Pemeriksaan</b>",align: "center",width: 150},
        {field: "f_detail_tujuan_name",title: "<b>Detail</b>",align: "center",width: 150},
        {field: "f_destination_country",title: "<b>Destination</b>",align: "center",width: 150},  
        {field: "f_pcr_dt",title: "<b>Booking</b>",align: "center",width: 100},  
        {field: "f_swabber_name",title: "<b>Swabber Name</b>",align: "center",width: 150},
        {field: "f_swab_dt",title: "<b>Swab Datetime</b>",align: "center",width: 100},
        {field: "f_result_dt",title: "<b>Result Datetime</b>",align: "center",width: 150}, 
        {field: "f_status_name",title: "<b>Status</b>",align: "center",width: 120},
        {field: "f_nar_status_desc",title: "<b>Desc NAR Status</b>",align: "center",width: 350},
        {field: "f_result_email",title: "<b>Send Email</b>",align: "center",width: 80},
        {field: "f_print",title: "<b>Print</b>",align: "center",width: 50},
        {field: "f_patient_id",title: "<b>#PID</b>",align: "center",width: 100},
      ]],
      rowStyler:function(index,row){
        if (row.f_status_color != null){
          return 'color:'+row.f_status_color;
        }
      },
      onLoadSuccess:function(){
        $("#_antigent_grid01").datagrid('showColumn', 'f_result_wa_mod');

        $("#_antigent_grid01").datagrid('clearSelections');
        $("#_antigent_grid01").datagrid('clearChecked');
        _jf_antigent_grid01_status_load();
        
        $("#_102_grid02").datagrid('reload');
        $("#_102_grid02").datagrid('clearSelections');
        $("#_102_grid02").datagrid('clearChecked');
        // _jf_grid02_termFinanceDoc_load();

        if(searchScanQR == 1){
          var getData = $("#_antigent_grid01").datagrid('getData');
          if(getData.rows.length == 0){
            $.messager.alert("Search with scan qr","Patient No "+searchScanQRValue+" Not Found.")
          }

          $("#_antigent_grid01").datagrid('selectRow',0);
          $("#_antigent_grid01").datagrid('checkRow',0);
          setTimeout(function(){ 
            searchScanQR = 0;
          }, 5000);
        }
      },
      onSelect:function(){
        $("#_antigent_grid01").datagrid('clearChecked');
        // _jf_antigent_grid01_status_load();
        _jf_102_grid02_load()
        // _jf_grid02_termFinanceDoc_load();
      },
      onCheck:function(){
        $("#_antigent_grid01").datagrid('clearSelections');
      },
      onCheckAll:function(){
        $("#_antigent_grid01").datagrid('clearSelections');
      }
    })


  })
    // function _jf_antigent_grid01_export(){
    //     var filterParams = _jf_antigent_grid01_filter_g();
    //     window.open("-v2/membership_batch_export/?"+filterParams);
    // }

    function _jf_antigent_grid01_filter_i(){
        $("#_antigent_grid01_filter_d").load("-v2/grid01_filter/?_dialog=_antigent_grid01_filter_d", function(){
        _jf_set_dialogButtons("_antigent_grid01_filter_d");
        setTimeout(function(){ 
            $("#_antigent_grid01_filter_d").dialog('close');
        }, 100)
        });
    }
    function _jf_antigent_grid01_filter_o(){
        $("#_antigent_grid01_filter_d").dialog("open");
    }
    function _jf_antigent_grid01_filter_g(){
        var srcevt = $("#_antigent_grid01_filter_srcevt").textbox('getValue');
        $("#_antigent_grid01_filter_srcevt_front").textbox('setValue',srcevt);
        var statusCode = $("#_antigent_grid01_filter_status_code").combobox('getValue');
        $("#_antigent_grid01_filter_status_code_front").combobox('setValue',statusCode);

        var filterParams = $("#_antigent_grid01_filter_f").serialize();
        // alert(filterParams);
        // filterParams += "";
        return filterParams;
    }
    function _jf_antigent_grid01_filter_p(){
        var filterParams = _jf_antigent_grid01_filter_g();
        $("#_antigent_grid01").datagrid("load", "-v2/grid01_read/?"+filterParams);
        $("#_antigent_grid01_filter_d").dialog("close");
    }
    function _jf_antigent_grid01_filter_r(){
        $("#_antigent_grid01_filter_f").form('clear');
        $("#_antigent_grid01_filter_status_code").combobox('setValue','sAll');
        _jf_antigent_grid01_filter_p();
    }

function _jf_102_grid02_import(){
  // $.messager.alert("Import Result","on developing.");
  _jf_dialog_create(
    id = "_102_grid02_dialog",
    href = "-v2/grid02_import/?_dialog=_102_grid02_dialog",
    title = "Import Patient",
    width = 400,
    height = 300,
    closable = true
  );
  $("#_102_grid02_dialog").dialog('open');
}
function _jf_102_grid02_import_taishan(){
  // $.messager.alert("Import Result","on developing.");
  _jf_dialog_create(
    id = "_102_grid02_dialog",
    href = "-v2/grid02_import_taishan/?_dialog=_102_grid02_dialog",
    title = "Import Patient",
    width = 400,
    height = 250,
    closable = true
  );
  $("#_102_grid02_dialog").dialog('open');
}

  function _jf_102_grid02_wa_result(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];

    if(sel){
      dataPatient.push(sel.f_patient_id);
    }
    if(check){
      check.forEach(function(value, index){
        dataPatient.push(value.f_patient_id);
      });
    }

    if (dataPatient.length>0) {
      $.messager.confirm("Result (WhatsApp)","Send "+dataPatient.length+" result to WhatsApp ?", function(r){
        if(r){
          preloader_block();
          $.ajax({
            type: 'GET',
            dataType: "JSON",
            url:"-v2/wa_result/"+JSON.stringify(dataPatient),
            success: function(r) {
              $.messager.alert("Result (WhatsApp)", r.message);
              if (r.success) {
                $("#_antigent_grid01").datagrid('reload');
                $("#_antigent_grid01").datagrid('clearSelections');
                $("#_antigent_grid01").datagrid('clearChecked');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Result (WhatsApp)", "Request error, please try again.");
              preloader_none();
            }
          })
        }
      })
    }else{
      $.messager.alert("Result (WhatsApp)", "Checklist Patient!");
    }
  }

  function _jf_102_grid02_email_result(){
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];
      check.forEach(function(value, index){
          dataPatient.push(value.f_patient_id);
          // alert(JSON.stringify(dataPatient.push(value.f_patient_id)));
      });

      if (dataPatient.length>0) {
        var my_modal_id = "102_grid01_form_modal";
        var my_modal_size = "";
        var my_modal_url = "-v2/grid03_form_mail/"+JSON.stringify(dataPatient);
        var my_modal_title = "Mail Form";
        my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
        $('#'+my_modal_id).modal({
          show:true,
          backdrop:'static'
        });
      }else{
        $.messager.alert("Sent Result", "Checklist Patient!");
      }
  }

function _jf_102_grid02_sentmail_result(){
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];
      check.forEach(function(value, index){
          dataPatient.push(value.f_patient_id);
          // alert(JSON.stringify(dataPatient.push(value.f_patient_id)));
      });

      if (dataPatient.length>0) {
        var my_modal_id = "102_grid01_form_modal";
        var my_modal_size = "";
        var my_modal_url = "-v2/grid03_form_mail/"+JSON.stringify(dataPatient);
        var my_modal_title = "Mail Form";
        my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
        $('#'+my_modal_id).modal({
          show:true,
          backdrop:'static'
        });
      }else{
        $.messager.alert("Sent Result", "Checklist Patient!");
      }
  }

function _jf_102_grid02_add(){

    var my_modal_id = "_102_grid02_dialog";
    var my_modal_size = "";
    var my_modal_url = "-v2/grid02_add/?_dialog=_102_grid02_dialog";
    var my_modal_title = "Add Patient";
    my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
    $('#'+my_modal_id).modal({
      show:true,
      backdrop:'static'
    });
}
function _jf_102_grid02_edit(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      var mId = sel.f_patient_id;

      var my_modal_id = "_102_grid02_dialog";
      var my_modal_size = "";
      var my_modal_url = "-v2/grid02_edit/"+mId+"?_dialog=_102_grid02_dialog";
      var my_modal_title = "Edit Patient";
      my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
      $('#'+my_modal_id).modal({
        show:true,
        backdrop:'static'
      });

     }else{
      $.messager.alert("Edit Patient","Select Patient first.");
    }
  }
  function _jf_102_grid02_edit_super(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      var mId = sel.f_patient_id;

      var my_modal_id = "_102_grid02_dialog";
      var my_modal_size = "";
      var my_modal_url = "-v2/grid02_edit_super/"+mId+"?_dialog=_102_grid02_dialog";
      var my_modal_title = "Edit Patient (SUPER)";
      my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
      $('#'+my_modal_id).modal({
        show:true,
        backdrop:'static'
      });

      // _jf_dialog_create(
      //   id = "_102_grid02_dialog",
      //   href = "-v2/grid02_edit/"+mId+"?_dialog=_102_grid02_dialog",
      //   title = "Edit Patient",
      //   width = 400,
      //   height = 500,
      //   closable = true
      // );
      // $("#_102_grid02_dialog").dialog('open');
    }else{
      $.messager.alert("Edit Patient","Select Patient first.");
    }
  // var sel = $("#_antigent_grid01").datagrid('getSelected');
  // if(sel){
  //   var mBatchId = sel.f_handover_batch_id;
  // }else{
  //   $.messager.alert("Edit Patient","Select batch first.");
  // }
  }


  function _jf_102_grid02_result_send_wa(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    var check = $("#_antigent_grid01").datagrid('getChecked');

    if(sel){
      var patientIdList = [];
      patientIdList.push(sel.f_patient_id);

      var message_confirm = "Send Result By WA 1 data ?";

      execute_result_send_wa(patientIdList,message_confirm);
    }else if(check.length>0){
      var patientIdList = [];

      check.forEach(function(value, index){
        patientIdList.push(value.f_patient_id);
      });        

      var message_confirm = "Send Result By WA "+check.length+" data ?";
      execute_result_send_wa(patientIdList,message_confirm);
    }else{
      alert("Please select or checklist data!");
    }
  }
  function execute_result_send_wa(patientIdList="", message_confirm=""){
    if(patientIdList!=""){
      $.messager.confirm("Send Result By WA",message_confirm,function(r){
        if(r){
          preloader_block();
          $.ajax({
            type: 'POST',
            dataType: "JSON",
            data:{
              idpatient:JSON.stringify(patientIdList)
            },
            url:"-v2/grid02_result_send_wa",
            success: function(r) {
              $.messager.alert("Send Result By WA", r.message);
              if (r.success) {
                $("#_antigent_grid01").datagrid('reload');
                $("#_antigent_grid01").datagrid('clearSelections');
                $("#_antigent_grid01").datagrid('clearChecked');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Send Result By WA", "Request error, please try again.");
              preloader_none();
            }
          })
        }
      })
    }else{
      alert("Please select or checklist data!");
    }
  }


  function _jf_102_grid02_delete(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    var check = $("#_antigent_grid01").datagrid('getChecked');

    if(sel){

      var patientIdList = [];
      patientIdList.push(sel.f_patient_id);

      var message_confirm = "Delete Selected data ?";

      execute_delete(patientIdList,message_confirm);
    }else if(check.length>0){
      var patientIdList = [];

      check.forEach(function(value, index){
        patientIdList.push(value.f_patient_id);
      });        

      var message_confirm = "Delete Checklist data ?";
      execute_delete(patientIdList,message_confirm);
    }else{
      alert("Please select or checklist data!");
    }
  }

  function _jf_antigent_grid01_add_exist(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      var mId = sel.f_patient_id;
      // filterParams = "&handover_batch_in="+sel.f_handover_batch_id;
      filterParams = "&handover_batch_in=";

      var my_modal_id = "_102_grid02_dialog";
      var my_modal_size = "";
      var my_modal_url = "-v2/grid02_add_exist/"+mId+"?_dialog=_102_grid02_dialog"+filterParams;
      var my_modal_title = "Add By Exist Data";
      my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
      $('#'+my_modal_id).modal({
        show:true,
        backdrop:'static'
      });

    }else{
      $.messager.alert("Add By Exist Data","Select Patient first.");
    }
  }
  
  function _jf_antigent_grid01_confirm(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      var mId = sel.f_patient_id;
      // filterParams = "&handover_batch_in="+sel.f_handover_batch_id;
      filterParams = "&handover_batch_in=";

      var my_modal_id = "_102_grid02_dialog";
      var my_modal_size = "";
      var my_modal_url = "-v2/grid02_confirm/"+mId+"?_dialog=_102_grid02_dialog"+filterParams;
      var my_modal_title = "Confirm Data";
      my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
      $('#'+my_modal_id).modal({
        show:true,
        backdrop:'static'
      });

    }else{
      $.messager.alert("Confirm Data","Select Patient first.");
    }
  }

  function execute_delete(patientIdList="", message_confirm=""){
    if(patientIdList!=""){
      $.messager.confirm("Delete Patient",message_confirm,function(r){
        if(r){
          preloader_block();
          $.ajax({
            type: 'DELETE',
            dataType: "JSON",
            url:"-v2/grid02_delete?patient="+JSON.stringify(patientIdList),
            success: function(r) {
              $.messager.alert("Delete Patient", r.message);
              if (r.success) {
                $("#_antigent_grid01").datagrid('reload');
                $("#_antigent_grid01").datagrid('clearSelections');
                $("#_antigent_grid01").datagrid('clearChecked');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Delete Patient", "Request error, please try again.");
              preloader_none();
            }
          })
        }
      })
    }else{
      alert("Please select or checklist data!");
    }
  }

  function _jf_102_grid02_delete_super(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    var check = $("#_antigent_grid01").datagrid('getChecked');

    if(sel){

      var patientIdList = [];
      patientIdList.push(sel.f_patient_id);

      var message_confirm = "Delete Selected data ?";

      execute_delete_super(patientIdList,message_confirm);
    }else if(check.length>0){
      var patientIdList = [];

      check.forEach(function(value, index){
        patientIdList.push(value.f_patient_id);
      });        

      var message_confirm = "Delete Checklist data ?";
      execute_delete_super(patientIdList,message_confirm);
    }else{
      alert("Please select or checklist data!");
    }
  }
  function execute_delete_super(patientIdList="", message_confirm=""){
    if(patientIdList!=""){
      $.messager.confirm("Delete Patient (SUPER)",message_confirm,function(r){
        if(r){
          preloader_block();
          $.ajax({
            type: 'DELETE',
            dataType: "JSON",
            url:"-v2/grid02_delete_super?patient="+JSON.stringify(patientIdList),
            success: function(r) {
              $.messager.alert("Delete Patient", r.message);
              if (r.success) {
                $("#_antigent_grid01").datagrid('reload');
                $("#_antigent_grid01").datagrid('clearSelections');
                $("#_antigent_grid01").datagrid('clearChecked');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Delete Patient", "Request error, please try again.");
              preloader_none();
            }
          })
        }
      })
    }else{
      alert("Please select or checklist data!");
    }
  }

  function _jf_102_grid02_receive(){
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];
    check.forEach(function(value, index){
        dataPatient.push(value.f_patient_id);
    });
        if (dataPatient.length>0) {
            $.messager.confirm("Receive Patient","Receive selected data ?",function(r){
                if(r){
                preloader_block();
                $.ajax({
                    type: 'GET',
                    dataType: "JSON",
                    url:"-v2/grid02_receive/?idpatient="+JSON.stringify(dataPatient),
                    success: function(r) {
                    $.messager.alert("Receive Patient", r.message);
                    if (r.success) {
                        $("#_antigent_grid01").datagrid('reload');
                        $("#_102_grid02").datagrid('reload');
                        $("#_102_grid02").datagrid('clearSelections');
                        $("#_102_grid02").datagrid('clearChecked');

                    }
                    preloader_none();
                    },
                    error:function(){
                    $.messager.alert("Receive Patient", "Request error, please try again.");
                    preloader_none();
                    }
                })
                }
            })
        }else{
          $.messager.alert("Receive Patient", "Checklist Patient to Receive");
        }
    }

  function _jf_102_grid03_sentRegister(){
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];
      check.forEach(function(value, index){
          dataPatient.push(value.f_patient_id);
      });

      if (dataPatient.length>0) {
        _jf_dialog_create(
          id = "_102_grid03_dialog",
          href = "-v2/grid03_sent_patient/?_dialog=_102_grid03_dialog",
          title = "Sent Patient to Pool Point",
          width = 500,
          height = 180,
          closable = true
        );
        $("#_102_grid03_dialog").dialog('open');
      }else{
        $.messager.alert("Sent Patient to Lab", "Checklist Patient!");
      }
  }

    function _jf_102_grid03_export() {
      var filterParams = _jf_antigent_grid01_filter_g();
      window.open("-v2/grid01_export/?"+filterParams);
    }

    function _jf_102_grid03_export_fin() {
      var filterParams = _jf_antigent_grid01_filter_g();
      
      window.open("-v2/grid01_export_plato/?"+filterParams);
      
    }
  
  function _jf_antigent_grid01_import(){
    $.messager.alert("Import Result","on developing.");
  }

  function _jf_antigent_grid01_edit(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
    var mId = sel.f_patient_id;
    _jf_dialog_create(
        id = "_antigent_grid01_dialog",
        href = "-v2/grid01_edit/"+mId+"?_dialog=_antigent_grid01_dialog",
        title = "Edit Patient",
        width = 500,
        height = 550,
        closable = true
    );
    $("#_antigent_grid01_dialog").dialog('open');
    }else{
    $.messager.alert("Edit Patient","Select Patient first.");
    }
  }
  function _jf_antigent_grid01_delete(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      var mBatchId = sel.f_handover_batch_id;
      var sel = $("#_antigent_grid01").datagrid('getSelected');
      if(sel){
        var mId = sel.f_patient_id;
        $.messager.confirm("Delete Patient","Delete selected data ?",function(r){
          if(r){
            preloader_block();
            $.ajax({
              type: 'DELETE',
              dataType: "JSON",
              url:"-v2/grid01_delete/"+mBatchId+"/"+mId,
              success: function(r) {
                $.messager.alert("Delete Patient", r.message);
                if (r.success) {
                  $("#_antigent_grid01").datagrid('reload');
                  $("#_antigent_grid01").datagrid('clearSelections');
                  $("#_antigent_grid01").datagrid('clearChecked');
                }
                preloader_none();
              },
              error:function(){
                $.messager.alert("Delete Patient", "Request error, please try again.");
                preloader_none();
              }
            })
          }
        })
      }else{
        $.messager.alert("Delete Patient","Select Patient to delete.");
      }
    }else{
      $.messager.alert("Delete Patient","Select batch first.");
    }
  }

  function _jf_antigent_grid01_send_to_lab(){
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var patientIdList = [];
    check.forEach(function(value, index){
        patientIdList.push(value.f_patient_id);
        // alert(JSON.stringify(patientIdList.push(value.f_patient_id)));
    });
    // alert(patientIdList);
    if (patientIdList.length>0) {
      _jf_dialog_create(
        id = "_antigent_grid01_dialog_send_to_lab",
        href = "-v2/grid01_send_to_lab/?_dialog=_antigent_grid01_dialog_send_to_lab",
        title = "Send Sample To Lab",
        width = 500,
        height = 180,
        closable = true
      );
      $("#_antigent_grid01_dialog_send_to_lab").dialog('open');
    }else{
      $.messager.alert("Send Sample To Lab", "Checklist Sample Patient To Process.")
    }
  }


  function _jf_102_grid02_antigene_result(mode = 0, lang = 'id'){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];

    if(sel){
      dataPatient.push(sel.f_patient_id);
    }
    if(check){
      check.forEach(function(value, index){
        dataPatient.push(value.f_patient_id);
      });
    }
    if (dataPatient.length>0) {
      // window.open("-v2/grid02_antigene_result");
      _jf_dialog_create(
        id = "_102_grid02_dialog_antigene_result",
        href = "-v2/grid02_antigene_result/?_dialog=_102_grid02_dialog_antigene_result&patientIdList="+JSON.stringify(dataPatient),
        title = "Update Result For "+dataPatient.length+" Data",
        width = 350,
        height = 250,
      );
      $("#_102_grid02_dialog_antigene_result").dialog('open');
    }else{
      $.messager.alert("Update Result","Check list data to update result.");
    }
  }
  function _jf_102_grid02_antigene_result_super(mode = 0, lang = 'id'){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];

    if(sel){
      dataPatient.push(sel.f_patient_id);
    }
    if(check){
      check.forEach(function(value, index){
        dataPatient.push(value.f_patient_id);
      });
    }

    if (dataPatient.length>0) {
      // window.open("-v2/grid02_antigene_result");
      _jf_dialog_create(
        id = "_102_grid02_dialog_antigene_result_super",
        href = "-v2/grid02_antigene_result_super/?_dialog=_102_grid02_dialog_antigene_result_super&patientIdList="+JSON.stringify(dataPatient),
        title = "Update Result For "+dataPatient.length+" Data (SUPER)",
        width = 350,
        height = 250,
      );
      $("#_102_grid02_dialog_antigene_result_super").dialog('open');
    }else{
      $.messager.alert("Update Result","Check list data to update result.");
    }
  }
  function _jf_antigent_grid01_print_pdf(mode = 0, lang = 'id', stamp = 0){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];

    if(sel){
      dataPatient.push(sel.f_patient_id);
    }
    if(check){
      check.forEach(function(value, index){
        dataPatient.push(value.f_patient_id);
      });
    }

    if (dataPatient.length>0) {
      window.open("-v2/grid01_print_pdf/?mode="+mode+"&lang="+lang+"&idpatient="+JSON.stringify(dataPatient)+"&stamp="+stamp);
    }else{
      $.messager.alert("Print Result","Please Select or Check-list data to print.");
    }
  }

  function _jf_102_grid02_print_label(mode = ""){
    var check = $("#_antigent_grid01").datagrid('getChecked');
    var dataPatient = [];
    check.forEach(function(value, index){
        dataPatient.push(value.f_patient_id);
    });
    if (dataPatient.length>0) {
      window.open("-v2/grid02_print_label/?mode="+mode+"&idpatient="+JSON.stringify(dataPatient));
    }else{
      $.messager.alert("Print Label","Check list data to print.");
    }
  }

  function _jf_102_grid02_print_label_v2(mode = ""){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if (sel) {
      window.open("-v2/grid02_print_label_v2/?mode="+mode+"&idpatient="+sel.f_patient_id);
    }else{
      $.messager.alert("Label 50x25mm","Please select data.");
    }
  }


  function _jf_102_grid02_search_scan_qr(){
    searchScanQR = 0;
    searchScanQRValue = "";

    var my_modal_id = "102_grid02_search_scan_qr";
    var my_modal_size = "";
    var my_modal_url = "-v2/grid02_search_scan_qr/";
    var my_modal_title = "Search With Scan QR";
    my_modal_create(my_modal_id, my_modal_size, my_modal_url, my_modal_title);
    $('#'+my_modal_id).modal({
      show:true,
      backdrop:'static'
    });
  }


  function _jf_1022(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      $.messager.confirm("Get Result","Get result for patient "+sel.f_name+" ?",function(r){
        if(r){
          preloader_block();
          $.ajax({
            type: 'PUT',
            dataType: "JSON",
            url:"-v2/grid02_get_result/"+mBatchId+"/"+mId,
            success: function(r) {
              $.messager.alert("Delete Patient", r.message);
              if (r.success) {
                $("#_antigent_grid01").datagrid('reload');
                $("#_antigent_grid01").datagrid('clearSelections');
                $("#_antigent_grid01").datagrid('clearChecked');
              }
              preloader_none();
            },
            error:function(){
              $.messager.alert("Delete Patient", "Request error, please try again.");
              preloader_none();
            }
          })
        }
      })
    }else{
      $.messager.alert("Get Result","Select patient first.");
    }
  }


    function _jf_antigent_grid01_gResultAll() {
      // var sel = $("#_antigent_grid01").datagrid('getSelected');
      // if (sel) {
        $.messager.confirm("Get All Result","Get All Result Data ?",function(r){
          if(r){
            preloader_block();
            $.ajax({
              type: 'GET',
              dataType: "JSON",
              url:"-v2/grid01_gresult_all/",
              success: function(r) {
              $.messager.alert("Get All Result", r.message);
              if (r.success) {
                  $("#_antigent_grid01").datagrid('reload');
                  $("#_antigent_grid01").datagrid('clearSelections');
                  $("#_antigent_grid01").datagrid('clearChecked');
              }
                preloader_none();
              },
              error:function(){
                $.messager.alert("Get All Result", "Request error, please try again.");
                preloader_none();
              }
            })
          }
        })
    }

    function _jf_antigent_grid01_gResultAll_v2() {
        $.messager.confirm("Get All Result","Get All Result Data ?",function(r){
          if(r){
            preloader_block();
            $.ajax({
              type: 'GET',
              dataType: "JSON",
              url:"-v2/grid01_gresult_all/",
              success: function(r) {
              $.messager.alert("Get All Result", r.message);
              if (r.success) {
                  $("#_antigent_grid01").datagrid('reload');
                  $("#_antigent_grid01").datagrid('clearSelections');
                  $("#_antigent_grid01").datagrid('clearChecked');
              }
                preloader_none();
              },
              error:function(){
                $.messager.alert("Get All Result", "Request error, please try again.");
                preloader_none();
              }
            })
          }
        })
    }


</script>

<script type="text/javascript">
  $(function(){
    
    $("#_antigent_grid01_status").datagrid({
      border:false,
      striped: true,
      pagination: false,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 25, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "f_id",
      // url: "-v2/grid01_status_progress_read",
      columns: [[
        {field: "f_status_name",title: "<b>Status</b>",align: "center",width: 120},
        {field: "f_status_desc",title: "<b>Status Desc</b>",align: "left",width: 150},
        {field: "f_entry_by_name",title: "<b>Status By</b>",align: "center",width: 100},
        {field: "f_entry_dt",title: "<b>Status Datetime</b>",align: "center",width: 140},
      ]],
      rowStyler:function(index,row){
        if (row.f_status_color != null){
          return 'color:'+row.f_status_color;
        }
      },
    })
  })

  function _jf_antigent_grid01_status_load(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      var id = sel.f_patient_id;
      $("#_antigent_grid01_status").datagrid({
        url: "-v2/grid01_status_progress_read/"+id,
      })
    }else{
      $("#_antigent_grid01_status").datagrid({
        url: "-v2/grid01_status_progress_read/0",
      })
    }
  }
</script>


<script type="text/javascript">
  $(function(){
    $("#_102_grid02_status").datagrid({
      border:false,
      striped: true,
      pagination: false,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 25, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "f_id",
      // url: "-v2/grid02_status_progress_read",
      columns: [[
        {field: "f_status_name",title: "<b>Status</b>",align: "center",width: 120},
        {field: "f_status_desc",title: "<b>Status Desc</b>",align: "left",width: 150},
        {field: "f_entry_by_name",title: "<b>Status By</b>",align: "center",width: 100},
        {field: "f_entry_dt",title: "<b>Status Datetime</b>",align: "center",width: 140},
      ]],
      rowStyler:function(index,row){
        if (row.f_status_color != null){
          return 'color:'+row.f_status_color;
        }
      },
    })
  })
  function _jf_102_grid02_status_load(){
    var sel = $("#_102_grid02").datagrid('getSelected');
    if(sel){
      var id = sel.f_patient_id;
      $("#_102_grid02_status").datagrid({
        url: "-v2/grid02_status_progress_read/"+id,
      })
    }else{
      $("#_102_grid02_status").datagrid({
        url: "-v2/grid02_status_progress_read/0",
      })
    }
  }
</script>

<script>
     $(function(){
    $("#_102_grid02").datagrid({
      toolbar: "#_102_grid02_toolbar",
      // title:"Daftar User",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: true,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      // pageList: [5, 10, 20, 25, 50, 100, 200, 300, 500, 1000],
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "f_pcr_id",
      frozenColumns: [[
      ]],
      columns: [[
        {field: "f_status_name",title: "<b>Result</b>",align: "center",width: 80},
        {field: "f_entry_dt",title: "<b>Entry Datetime</b>",align: "center",width: 100},
      ]],
      rowStyler:function(index,row){
        if (row.f_status_color != null){
          return 'color:'+row.f_status_color;
        }
      },
      onLoadSuccess:function(){
        $("#_102_grid02").datagrid('clearSelections');
        $("#_102_grid02").datagrid('clearChecked');
        // _jf_102_grid02_status_load();
        // _jf_grid02_termFinanceDoc_load();
        var getData = $("#_102_grid02").datagrid('getData');
        var total = getData.total;
        if(total > 0){
          $("#_102_grid02").datagrid('selectRow',0);
        }else{
          _jf_102_grid03_load();
        }
      },
      onSelect:function(){
        _jf_102_grid02_status_load();
        _jf_102_grid03_load();
        // _jf_grid02_termFinanceDoc_load();
      }
    })

  })

  function _jf_102_grid02_load(){
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if(sel){
      var id = sel.f_patient_id;
      $("#_102_grid02").datagrid({
        url: "-v2/grid02_read/"+id,
      })
    }else{
      $("#_102_grid02").datagrid({
        url: "-v2/grid02_read",
      })
    }
  }

  function _jf_102_grid02_status_load(){
    var sel = $("#_102_grid03").datagrid('getSelected');
    if(sel){
      var id = sel.f_patient_id;
      $("#_102_grid02_status").datagrid({
        url: "-v2/grid02_status_progress_read/"+id,
      })
    }else{
      $("#_102_grid02_status").datagrid({
        url: "-v2/grid02_status_progress_read/0",
      })
    }
  }

  function _jf_102_grid02_gresult() {
    var sel = $("#_antigent_grid01").datagrid('getSelected');
    if (sel) {
      $.messager.confirm("Result Test","Result data ?",function(r){
        if(r){
        preloader_block();
        $.ajax({
            type: 'GET',
            dataType: "JSON",
            url:"-v2/grid02_gresult/"+sel.f_patient_id,
            success: function(r) {
            $.messager.alert("Result Patient", r.message);
            if (r.success) {
                $("#_102_grid02").datagrid('reload');
                $("#_102_grid02").datagrid('clearSelections');
                $("#_102_grid02").datagrid('clearChecked');
            }
            preloader_none();
            },
            error:function(){
            $.messager.alert("Result Patient", "Request error, please try again.");
            preloader_none();
            }
        })
        }
    })
    }else{
      $.messager.alert("Get Result","Select Patient to get Result.");
    }
  }
</script>

<script>
     $(function(){
    $("#_102_grid03").datagrid({
      toolbar: "#_102_grid03_toolbar",
      // title:"Daftar User",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      // pageList: [5, 10, 20, 25, 50, 100, 200, 300, 500, 1000],
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "f_result_ct_d",
      frozenColumns: [[
        {field: "f_target_name",title: "<b>Target Name</b>",align: "left",width: 150},
      ]],
      columns: [[
        {field: "f_ct",title: "<b>CT</b>",align: "center",width: 80},
        {field: "f_ct_mean",title: "<b>CT Mean</b>",align: "center",width: 80},
        {field: "f_ct_sd",title: "<b>CT SD</b>",align: "center",width: 80},
        {field: "f_ct_treshold",title: "<b>CT Treshold</b>",align: "center",width: 80},
        {field: "f_cq_conf",title: "<b>CT Conf.</b>",align: "center",width: 80},
        {field: "f_entry_by_name",title: "<b>Entry By</b>",align: "center",width: 100},
        {field: "f_entry_dt",title: "<b>Entry Datetime</b>",align: "center",width: 100},
      ]],
      rowStyler:function(index,row){
        if (row.f_status_color != null){
          return 'color:'+row.f_status_color;
        }
      },
      onLoadSuccess:function(){
        $("#_102_grid03").datagrid('clearSelections');
        $("#_102_grid03").datagrid('clearChecked');
      },
      onSelect:function(){
        _jf_102_grid02_status_load();
      }
    })

  })

  function _jf_102_grid03_load(){
    var sel = $("#_102_grid02").datagrid('getSelected');
    if(sel){
      var id = sel.f_result_id;
      $("#_102_grid03").datagrid({
        url: "-v2/grid03_read/"+id,
      })
    }else{
      $("#_102_grid03").datagrid({
        url: "-v2/grid03_read",
      })
    }
  }

  function _jf_102_grid02_status_load(){
    var sel = $("#_102_grid03").datagrid('getSelected');
    if(sel){
      var id = sel.f_patient_id;
      $("#_102_grid02_status").datagrid({
        url: "-v2/grid02_status_progress_read/"+id,
      })
    }else{
      $("#_102_grid02_status").datagrid({
        url: "-v2/grid02_status_progress_read/0",
      })
    }
  }
</script>
@endsection

