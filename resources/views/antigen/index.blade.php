@extends('app')

@section('content')
<section id="antigen">
	<div class="row match-height">
		<div class="col-xl-12 col-md-12 col-12">
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Opps Something went wrong</strong>
                <button type="button" class="btn-close btn-secondary" data-bs-dismiss="alert" aria-label="Close">X</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
              </div>
            @endif
            
            @include('antigen.table')
        </div>
    </div>
</section>

<style>
    .select2-selection__arrow b{
        display:none !important;
    }
</style>

@include('antigen.create')
@include('antigen.edit')
@endsection

@push('scripts')
<script>
     $(function(){
        var dg = $('#antigen-table').datagrid();
        
        dg.datagrid({
            toolbar: "#antigen-table-toolbar",
            pageList: [5, 10, 15, 20, 25],			
			remoteSort:false,
            pageSize: 10,
            method: "POST",
            frozenColumns: [[
                {field: "ck",checkbox:true},
            ]],
            // url:'{{url("antigen-table")}}',
            columns:[[
                // {field:'ck',title:'', align:'center',checkbox:true},
                {field:'result',title:'Result', align:'center', sortable:true,},
                {field:'trans_stat',title:'Trans Status', align:'center', sortable:true,
                    formatter: function(value,row,index){
                        if(value == 1){
                            return '<div class="bg-success text-white" style="height:100%; width:100%;">Paid</div>';
                        }else{
                            return '<div class="bg-danger text-white" style="height:100%; width:100%;">Not Paid</div>';
                        }
                    }
                },
                {field:'created_at',title:'Date', align:'center', sortable:true,
                    formatter: function(value,row,index){
                        return moment(value).format("DD MMM YYYY"); 
                    }
                },
                {field:'identitas_type_id',title:'Type', align:'center', sortable:true,
                    formatter : function(value,row,index)
                    {
                        if (row.has_identitas_type)
                        {
                            return row.has_identitas_type.name;
                        } else {
                            return value;
                        }
                        
                    }
                },
                {field:'nik',title:'NIK.', align:'center', sortable:true,},
                {field:'nik_type',title:'Nationality', align:'center', sortable:true,},
                {field:'identitas_2',title:'Other Identity.', align:'center', sortable:true,},
                {field:'fullname',title:'Fullname.', align:'center', sortable:true,},
                {field:'dob',title:'Date Of Birth', align:'center', sortable:true,
                    formatter: function(value,row,index){
                        return moment(value).format("DD MMM YYYY"); 
                    }
                },
                {field:'gender',title:'Gender', align:'center', sortable:true,},
                {field:'pasien_no',title:'Patient No.', align:'center', sortable:true,},
                {field:'external_no',title:'Lab Patient No.', align:'center', sortable:true,},
                {field:'no_phone',title:'Phone / Tlpn', align:'center', sortable:true,},
                {field:'email',title:'Email', align:'center', sortable:true,},
                {field:'penjamin',title:'Penjaminan', align:'center', sortable:true,},
                {field:'inspeksi_id',title:'Pemeriksaan', align:'center', sortable:true,
                    formatter : function(value,row,index)
                    {
                        if (row.has_inspeksi)
                        {
                            return row.has_inspeksi.name;
                        } else {
                            return value;
                        }
                    }
                },
                {field:'detail_inspeksi_id',title:'Detail', align:'center', sortable:true,
                    formatter : function(value,row,index)
                    {
                        if (row.has_detail_inspeksi)
                        {
                            return row.has_detail_inspeksi.name;
                        } else {
                            return value;
                        }
                    }
                },
                {field:'ct_1',title:'CT 1', align:'center', sortable:true,},
                {field:'ct_2',title:'CT 2', align:'center', sortable:true,},
                {field:'swabber_date',title:'Swab Datetime', align:'center', sortable:true,
                    formatter: function(value,row,index){
                        return moment(value).format("DD MMM YYYY, h:mm:ss a"); 
                    }
                },
                {field:'swabber_name',title:'Swab Name', align:'center', sortable:true,},
                {field:'receive_date',title:'Receive Datetime', align:'center', sortable:true,},
                {field:'analisis_date',title:'Analiysis Datetime', align:'center', sortable:true,},
                {field:'result_date',title:'Result Datetime', align:'center', sortable:true,},
                {field:'status_print',title:'Status', align:'center', sortable:true,
                    formatter: function(value,row,index){
                        if(value == 1){
                            return '<span style="color: #000;">Printed</span>';
                        }else if(value == 2){
                            return '<span style="color: skyblue;">Result</span>';
                        }else if(value == 3){
                            return '<span style="color: blue;">Proccesing</span>';
                        }else if(value == 4){
                            return '<span style="color: red;">Entry</span>';
                        }else{
                            return '-';
                        }
                        
                    }
                },
                {field:'branch_id',title:'Owner', align:'center', sortable:true,
                    formatter : function(value,row,index)
                    {
                        if (row.has_branch.owner)
                        {
                            return row.has_branch.owner;
                        } else {
                            return value;
                        }
                    }
                },
                {field:'branch_name',title:'Branch', align:'center', sortable:true,},
                {field:'desc_nar_status',title:'Desc NAR Status', align:'center', sortable:true,
                    formatter: function(value,row,index){
                        if(value == 1){
                            return '<span>Desc NAR VALID</span>';
                        }else{
                            return '<span>Desc NAR NOT VALID</span>';
                        }
                    }
                },
                {field:'jml_print',title:'Print', align:'center', sortable:true,},
            ]],
            
        });
    });

    function addAntigen(){
		$('#addAntigen').modal('show');
        $('#addAntigen').on('hidden.bs.modal', function(e) {
            $(this).find('form').trigger('reset');
        })
	}

    function editAntigen(){
        $('#editAntigen').trigger('reset');
        var data = $("#antigen-table").datagrid('getSelected');
        // console.log(data);
        if(data){
            $('#editAntigen').modal('show');
            $(".modal-body #patient_id").val( data.id );
            // $(".modal-body #branch_id").val( data.branch_id );
            $(".modal-body #identitas_type_id").val( data.identitas_type_id );
            $(".modal-body #nik_type").val( data.nik_type );
            $(".modal-body #nik").val( data.nik );
            $(".modal-body #passport").val( data.passport );
            $(".modal-body #fullname").val( data.fullname );
            $(".modal-body #dob").val( data.dob );
            $(".modal-body #gender").val( data.gender );
            // $(".modal-body #provinsi-edit").val( data.provinsi_id );
            // $(".modal-body #provinsi-edit option:selected").val( data.provinsi_id );
            $(".modal-body #kota").val( data.kab_kota_id );
            $(".modal-body #rt").val( data.rt );
            $(".modal-body #rw").val( data.rw );
            $(".modal-body #alamat").val( data.alamat );
            $(".modal-body #no_phone").val( data.no_phone );
            $(".modal-body #email").val( data.email );
            $(".modal-body #registrasi_type").val( data.registrasi_type );
            $(".modal-body #perusahaan_name").val( data.perusahaan_name );
            $(".modal-body #inspeksi_id").val( data.inspeksi_id );
            $(".modal-body #detail_inspeksi_id").val( data.detail_inspeksi_id );
            $(".modal-body #destination_country").val( data.destination_country );
            $(".modal-body #status_swab_id").val( data.status_swab_id );
            $(".modal-body #swabber_name").val( data.swabber_name );
            $(".modal-body #swabber_date").val( data.swabber_date );
            $(".modal-body #pasien_no_edit").val( data.pasien_no );
            $(".modal-body #external_no").val( data.external_no );
            $(".modal-body #payment_service").val( data.payment_service );
            $(".modal-body #payment_ref").val( data.payment_ref );
            $(".modal-body #payment_card_no").val( data.payment_card_no );
            $(".modal-body #payment_name").val( data.payment_name );
            $(".modal-body #payment_date").val( data.payment_date );
            $(".modal-body #product_price").val( data.product_price );
            $(".modal-body #coupon").val( data.coupon );
            $(".modal-body #price").val( data.price );
            
        }else{
            $.messager.alert("Edit Patient","Select Patient first.");
        }
    }

    function deleteAntigen(){
        var sel = $("#antigen-table").datagrid('getSelected');
        var check = $("#antigen-table").datagrid('getChecked');
        
        if(sel){
            var patientIdList = [];
            patientIdList.push(sel.id);

            var message_confirm = "Delete Selected data ?";

            execute_delete(patientIdList,message_confirm);

            } else if(check.length>0) {
                var patientIdList = [];

                check.forEach(function(value, index){
                    patientIdList.push(value.id);
            });        

            var message_confirm = "Delete Checklist data ?";

            execute_delete(patientIdList,message_confirm);

            } else {
                alert("Please select or checklist data!");
            }
    }

    function execute_delete(patientIdList="", message_confirm=""){
        if(patientIdList!=""){
            // console.log(patientIdList);
        $.messager.confirm("Delete Patient", message_confirm,function(r){
            // console.log(r)
            if(r){
             
            $.ajax({
                type: 'DELETE',
                dataType: "JSON",
                url: "{{ route('antigen-destroy') }}",
                data:{
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE',
                    id: patientIdList
                },
                success: function(res) {
                // $.messager.alert("Delete Patient", r.message);
                if (res.success) {
                    // console.log(res)
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
                    $("#antigen-table").datagrid('reload');
                    $("#antigen-table").datagrid('clearSelections');
                    $("#antigen-table").datagrid('clearChecked');
                }
            },
                error:function(){
                    $.messager.alert("Delete Patient", "Request error, please try again.");
                }
            })
            }
        })
        }else{
            alert("Please select or checklist data!");
        }
    }

    function doSearch(val){
       
       var dg = $('#antigen-table').datagrid({
           pageSize: 5,
           pageList: [5, 10, 15, 20, 25],
           remoteSort:false,
       });
       $.ajax({
           url: '{{ route("antigen-table") }}',
           type: 'POST',
           data: {
               _token : "{{ csrf_token() }}",
               cari : val,

           },
           success: function(data){
               // console.log(data);
               // console.log(val);
               // prevQueryParams = dg.datagrid('options')['queryParams'];
               // newQueryParams = $.extend(prevQueryParams, { cari: val} );

               // dg.datagrid('loadData', newQueryParams);
               dg.datagrid('loadData', data);
           }
       });
    }

    $(document).ready(function(){
        var msg = '{{Session::get('sukses')}}';
        var exist = '{{Session::has('sukses')}}';
        if(exist){
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: msg,
                showConfirmButton: false,
                timer: 2500
            })
        }

    })
</script>

@endpush