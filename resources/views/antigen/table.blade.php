<div class="row">
    <div class="col-12">
        <table id="antigen-table" title="Swab Antigen Test" class="easyui-datagrid" url="{{route('antigen-table')}}" toolbar="#toolbar"
            pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" style="width:100%;height:550px;">
        </table>
        
        <div id="antigen-table-toolbar" class="p-1">
            <div class="row">
                
                <div class="col-12"> 
                    <div style="float: left">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="addAntigen()"><i
                                class="fa fa-plus fa-fw fa-lg" style="color:green"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                            onclick="editAntigen()"><i class="fa fa-edit fa-fw fa-lg" style="color:green"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                            onclick="deleteAntigen()"><i class="fa fa-trash fa-fw fa-lg"
                                style="color:green"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                            onclick="_jf_102_grid02_add_exist()"><i class="fa fa-plus fa-fw fa-lg"
                                style="color:green"></i>&nbsp;Add-Exist</a>
                        <a href="#" class="easyui-menubutton"
                            data-options="menu:'#_grid02_template_membership_new', plain:true, showEvent:'click'">Import</a>
                        <div id="_grid02_template_membership_new" class="menu-content" style="width:300px;">
                            <div>
                                <small class="p-2">
                                    <a href='' target='_blank'><i class='fa fa-cloud-download fa-fw fa-lg'></i> Download
                                        Template (New)</a>
                                </small>
                            </div>
                            <div>
                                <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                                    onclick="_jf_102_grid02_import_new()"><i class="fa fa-file-excel-o fa-fw fa-lg"
                                        style="color:green"></i>&nbsp;Import (Format Sislab)</a>
                            </div>
                        </div>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                            onclick="_jf_102_grid02_check_validation()"><i class="fa fa-refresh fa-fw fa-lg"
                                style="color:green"></i>&nbsp;Check Validation</a>
                        <a href="{{route('antigen-excelAll')}}" class="easyui-linkbutton" data-options="plain:true"
                            ><i class="fa fa-file-excel-o fa-fw fa-lg"
                                style="color:green"></i>&nbsp;Export</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-redo',plain:true"
                            onclick="_jf_antigen_grid_send_to_lab()">&nbsp;Send To
                            Lab</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                            onclick="_jf_102_grid02_sentmail_result()"><i class="fa fa-paper-plane fa-lg fa-fw"
                                style="color:#3645b7"></i>&nbsp; Send to Mail</a>
                        <a href="#" class="easyui-menubutton"
                            data-options="menu:'#_antigen_grid_action', plain:true, showEvent:'click'">Print</a>

                        <div id="_antigen_grid_action" class="menu-content" style="width:80px;">
                            <hr class="my-1">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                                onclick="printPdf(0,'id')"><i class="fa fa-file-pdf-o fa-fw fa-lg"
                                    style="color:red"></i>
                                &nbsp;Print PDF</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                                onclick="printPdf(1,'id')"><i class="fa fa-file-pdf-o fa-fw fa-lg"
                                    style="color:orange"></i>
                                &nbsp;Print ZIP</a>
                            <hr class="my-1">
                        </div>

                        <a href="javascript:void(0)" data-options="plain:true" class="easyui-linkbutton"
                        onclick="_jf_102_grid02_search_scan_qr()"><i class="fa fa-qrcode fa-fw" style="color:#3572bd;"></i>
                        Set-QR-Patient</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"
                            onclick="_jf_102_grid02_search_scan_qr_label()"><i class="fa fa-qrcode fa-fw"
                                style="color:#3572bd;"></i> Search-QR</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true"
                            onclick="_jf_102_grid02_print_label_v2('label50x25mm')">Label-50x25mm</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true"
                            onclick="_jf_102_grid02_print_label_v2('label2x52x25mm')">Label-2x52x25mm</a>
                    </div>

                    <div style="float:right">
                        <select name="branch_id" id="filter_branch_owner" style="width: 120px; height: 25px;">
                            <option value="">All Owner</option>
                            @foreach ($branchs as $item)
                                <option value="{{$item->id}}">{{$item->owner}}</option>
                            @endforeach
                        </select>
                        {{-- <input type="text" id="filter_branch_owner"> --}}
                        <input type="text" id="filter_referral" placeholder="All Refferal">
                        <select name="desc_nar_status" id="filter_desc_nar_status" style="width: 120px; height: 25px;">
                            <option value="">All Status NAR</option>
                            <option value="1">Valid</option>
                            <option value="0">Not Valid</option>
                        </select>

                        <select style="width: 120px; height: 25px;">
                            <option value="">All</option>
                            {{-- <option value="1">Valid</option>
                            <option value="0">Not Valid</option> --}}
                        </select>
                        <select name="filer_order" id="filter_order" style="width: 120px; height: 25px;">
                            <option value="">All Data</option>
                            <option value="1">Last Data</option>
                            <option value="2">New Data</option>
                        </select>
                        {{-- <input type="text" id="filter_desc_nar_status"> --}}
                        {{-- <input type="text" id="filter_all"> --}}
                        {{-- <input type="text" id="filter_last_data"> --}}
                        {{-- <input type="text" id="search_all_front" placeholder="Type To Search"> --}}
                        <div style="float:right">
                            <input class="easyui-searchbox" id="cari" name="cari" data-options="prompt:'Type To Search',searcher:doSearch" style="width:100%"/>
                        </div> 
                       
                            <a href="#" class="easyui-menubutton mt-1 ml-1"
                                data-options="menu:'#_antigen_grid_filter_mb', iconCls:'icon-filter', plain:true"
                               >Filter</a>
                            <div id="_antigen_grid_filter_mb" class="menu-content-dis" style="width:100px;">
                                <div data-options="iconCls:'icon-filter'" onclick="filterOpen()">Open Filter</div>
                                <div data-options="iconCls:'icon-reload'" onclick="resetFilter()">Reset Filter</div>
                            </div>
                            <a href="#" class="easyui-menubutton mt-1" data-options="menu:'#_grid01_legend', plain:true">Status</a>
                            <div id="_grid01_legend" class="menu-content" style="width:100px;">
                            <div style="color:red">
                                <small class="p-2">Entry</small>
                            </div>
                            <div style="color:blue">
                                <small class="p-2">Processing</small>
                            </div>
                            <div style="color:#15b3c2">
                                <small class="p-2">Result</small>
                            </div>
                            <div style="color:black">
                                <small class="p-2">Printed</small>
                            </div>
                        
                    </div>
                        
                </div>
               
            </div>
        </div>

        
        
    </div>
</div>

{{-- <table id="antigen-table" title="Swab antigen Test" class="easyui-datagrid" url="{{url('antigen-table')}}" toolbar="#toolbar"
    pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" style="width:100%;height:350px;">
</table> --}}