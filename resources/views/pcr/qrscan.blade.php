
<div id="my_modal_frame">
    <div class="modal fade" id="102grid02_search_scan_qr_label" tabindex="-1" aria-labelledby="addPcrLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header my_bg_inatti text-white">
                    <h5 class="modal-title">Search With Scan QR</h5>
                    <button type="button" class="btn-close btn-secondary" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                    <div class="container mt-3 mb-2" id="_102_grid01_form_content">
                        <form id="_102_grid01_form" enctype="multipart/form-data" autocomplete="off">
                            <input type="hidden" name="_token" value="7i0q3BsNdTcPshBfWGGPG0itUAnBcsgn0eHkTTWy">
                            <div class="col-lg-12 col-md form-group">
                            <div class="row">
                                <div class="col-lg">
                                    <div style="width: 100%; position: relative; padding: 0px; border: 1px solid silver;" id="qrCodeScanner_patient2" class="my_qrscanner_frame">
                                        <div style="display: none; text-align: left; margin: 0px; padding: 5px; font-size: 20px; border-bottom: 1px solid rgba(192, 192, 192, 0.18);">
                                            <span>QR Scanner</span><span id="qrCodeScanner_patient2__status_span" style="float: right; padding: 5px 7px; font-size: 14px; background: rgb(238, 238, 255); border: 1px solid rgba(0, 0, 0, 0); color: rgb(17, 17, 17);">IDLE</span>
                                                <div id="qrCodeScanner_patient2__header_message" style="display: block; font-size: 14px; padding: 2px 10px; margin-top: 4px; border-top: 1px solid rgb(246, 246, 246); background: rgba(203, 36, 49, 0.14); color: rgb(203, 36, 49);">
                                                NotFoundError : Requested device not found</div>
                                                </div>
                                                
                                                <div id="qrCodeScanner_patient2__scan_region" style="width: 100%; min-height: 100px; text-align: center;">
                                                <br><img width="64" src="https://raw.githubusercontent.com/mebjas/html5-qrcode/master/assets/camera-scan.gif" style="opacity: 0.3;">
                                                </div>
                                                <div id="qrCodeScanner_patient2__dashboard" style="width: 100%;">
                                                    <div id="qrCodeScanner_patient2__dashboard_section" style="width: 100%; padding: 10px; text-align: left;">
                                                    <div>
                                                        <div style="text-align: center;">
                                                        <a  href="javascript:void(0)"  onclick="request_permission_camera()">Request Camera Permissions</a>
                                                        </div>
                                                    </div>
                                                    <div id="qrCodeScanner_patient2__dashboard_section_fsr" style="text-align: center; display: none;">
                                                    <input id="qrCodeScanner_patient2__filescan_input" accept="image/*" type="file" disabled="" style="width: 200px;">
                                                    <span>&nbsp; Select Image</span>
                                                    </div>
                                                </div>
                                                <div style="text-align: center;">
                                                <a id="qrCodeScanner_patient2__dashboard_section_swaplink" href="#scan-using-file" style="text-decoration: underline;"></a></div>
                                                </div></div></div>
                                </div>
                            </div>
                            </div>    
                        </form>

  
  <span class="text-md text-danger">NOTES : </span><br>
    <span class="text-xs">1. Gunakan QR pada kaset antigene</span><br>
    <span class="text-xs">2. Jika behasil, maka data akan di tampilkan pada tabel<i class="fas fa-qrcode fa-fw"></i>. </span><br>
      <!-- <a href="javascript:void(0)" class="btn btn-block btn-md btn-primary" onclick="_jf_102_grid01_create()">Save</a> -->
  
</div>
@push('scripts')
<script type="text/javascript">
    function request_permission_camera(){
        navigator.permissions.query({name: 'camera'})
        .then((permissionObj) => {
            console.log(permissionObj.state);
        })
        .catch((error) => {
            console.log('Got error :', error);
        })
    }
    function onScanSuccess(qrCodeMessage) {
      if(searchScanQR==0){
        searchScanQRValue = qrCodeMessage;
        
        var filterParams = "f_status_code=sAll&f_patient_id="+qrCodeMessage;
        $("#_102_grid01").datagrid("load", "{{ route('pcr-filter') }}"+filterParams);
        $("#102grid02_search_scan_qr_label").modal('hide');
        searchScanQR++;
        setTimeout(function(){ 
            searchScanQR = 0;
        }, 5000);
        // alert(qrCodeMessage);     
      }
    }
    $(function(){
        var html5QrcodeScanner = new Html5QrcodeScanner("qrCodeScanner_patient2", { fps: 20, qrbox: 250 });
        html5QrcodeScanner.render(onScanSuccess);
        $("#qrCodeScanner_patient2__camera_selection").addClass("form-control");
    })

  $(function(){
    
  })

</script>
@endpush
</div><div class="modal-footer"></div></div></div></div></div>
