<div class="modal fade" id="addexitPcr" tabindex="-1" aria-labelledby="addexitPcrLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="addexitPcrLabel">Add Exist Patient</h5>
				<button type="button" class="btn-close btn-secondary" data-bs-dismiss="modal"
					aria-label="Close">X</button>
			</div>

			<div class="modal-body">
				<div class="modal-content">

					<form id="form-addexitPcr" method="POST" action="{{route('pcr-update')}}" enctype="multipart/form-data">
						@csrf
						<input type="hidden" id="patient_id" name="id">
						<div class="pt-2 pb-2 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-6"><b>Branch Data</b></span>
								</div>
							</div>
							@if(isset($branchs))
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Branch</label>
								<select name="branch_id" id="branch_id_addexit"
									class="form-control {{$errors->has('branch_id') ? 'validatebox-invalid' : '' }}">
									<option value="">-Pilih-</option>
									@foreach ($branchs as $branch)
									<option value="{{$branch->id}}" {{ old('branch_id')==$branch->id ?
										'selected="selected"': '' }}>{{$branch->b_name}}</option>
									@endforeach
								</select>
								<input type="hidden" id="branch_id_addexit" name="branch_name">
								@error('branch_id') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							@endif
						</div>
						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-6"><b>Profile</b></span>
									<div class="col-lg-6">
										<div class="float-right" id="label_use_nar_biodata">
											<label class="textbox-label textbox-label-before" for="_easyui_checkbox_7"
												style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use
													Profile From Medinus?</b></label>
											<input class="easyui-checkbox checkbox-f" value="1" name="status_profile"
												type="checkbox" style="margin-left: 0px; margin-top: 0px;">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Identity
									Type</label>
								@if(isset($identitasTypes))
								<div class="col-lg-12">
									<select id="identitas_type_id" name="identitas_type_id"
										class="form-control {{$errors->has('identitas_type_id') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										@foreach ($identitasTypes as $identitasType)
										<option value="{{$identitasType->id}}" {{
											old('identitas_type_id')==$identitasType->id ? 'selected="selected"': ''
											}}>{{$identitasType->name}}</option>
										@endforeach
									</select>
									@error('identitas_type_id') <span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
								@endif
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">NIK</label>
								<div class="col-lg-5 mb-1">
									<select id="nik_type" name="nik_type"
										class="form-control {{$errors->has('nik_type') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										<option value="id_indonesia" {{ old('nik_type')=="id_indonesia"
											? 'selected="selected"' : '' }}> ID(Indonesia) </option>
										<option value="national" {{ old('nik_type')=="national" ? 'selected="selected"'
											: '' }}> National </option>
									</select>
									@error('nik_type') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								<div class="mb-1 col-lg-7" id="field_nik">
									<input type="number" id="nik2" name="nik" value="{{old('nik')}}"
										class="form-control {{$errors->has('nik') ? 'validatebox-invalid' : '' }}"
										placeholder="NIK">
									@error('nik') <span class="text-danger">{{ $message }}</span> @enderror
									<span style="color: red;" id="msgnik2"></span>		
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Fullname</label>
								<div class="col-lg-12">
									<input type="text" id="fullname" name="fullname" value="{{old('fullname')}}"
										class="form-control {{$errors->has('fullname') ? 'validatebox-invalid' : '' }}"
										placeholder="Fullname">
									@error('fullname') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="_102_f_dob" class="col-lg-12 col-form-label pb-1">DOB</label>
								<div class="col-lg-12">
									<input type="date" id="dob" name="dob" value="{{old('dob')}}"
										class="form-control {{$errors->has('dob') ? 'validatebox-invalid' : '' }}"
										placeholder="DOB">
									@error('dob') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Gender</label>
								<div class="col-lg-12">
									<select id="gender" name="gender"
										class="form-control {{$errors->has('gender') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										<option value="M" {{ old('gender')=="M" ? 'selected="selected"' : '' }}> Male
										</option>
										<option value="F" {{ old('gender')=="F" ? 'selected="selected"' : '' }}> Female
										</option>
									</select>
									@error('gender') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>

						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-6"><b>Domicile address</b></span>
									<div class="col-lg-6">
										<div class="float-right" id="label_use_nar_address">
											<label class="textbox-label textbox-label-before"
												style="text-align: left; width: 100px; height: 20px; line-height: 20px;"><b>Use
													NAR Data?</b></label>
											<input class="easyui-checkbox checkbox-f" value="1" name="domisili_status"
												type="checkbox" style="margin-left: 0px; margin-top: 0px;">
										</div>
									</div>
								</div>
							</div>

							<div class="form-group row mb-1">
								<label for="_102_f_provinsi_code" class="col-lg-12 col-form-label pb-1">Provinsi</label>
								@if(isset($provinces))
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('provinsi_id') ? 'validatebox-invalid' : '' }}"
										name="provinsi_id" id="provinsi-addexit">
										<option value="">-Pilih Provinsi-</option>
										@foreach ($provinces as $item)
										<option id="provinsi-option" value="{{ $item->id}}" {{
											old('provinsi_id')==$item->id ? 'selected="selected"': ''
											}}>{{ $item->name}}</option>
										@endforeach
									</select>
									<img src="{{ asset('public/app-assets/images/loading.gif')}}" width="35" id="load1"
										style="display:none;" />
									@error('provinsi_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								@endif
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Kabupaten/Kota</label>
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('kab_kota_id') ? 'validatebox-invalid' : '' }}"
										name="kab_kota_id" id="kota-addexit">
										<option value="">-Pilih Kabupaten/Kota-</option>
										{{-- @foreach ($regencies as $item)
										<option value="{{ $item->id}}" {{
											old('kab_kota_id')==$item->id ? 'selected="selected"': ''
											}}>{{ $item->name}}</option>
										@endforeach --}}
									</select>
									<img src="{{ asset('public/app-assets/images/loading.gif')}}" width="35" id="load2"
										style="display:none;" />
									@error('kab_kota_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Kecamatan</label>
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('kecamatan_id') ? 'validatebox-invalid' : '' }}"
										name="kecamatan_id" id="kecamatan-addexit">
										<option value="">-Pilih Kecamatan-</option>
										{{-- @foreach ($districts as $item)
										<option value="{{ $item->id}}" {{
											old('kecamatan_id')==$item->id ? 'selected="selected"': ''
											}}>{{ $item->name}}</option>
										@endforeach --}}
									</select>
									<img src="{{ asset('public/app-assets/images/loading.gif')}}" width="35" id="load3"
										style="display:none;" />
									@error('kecamatan_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="_102_f_kelurahan_code"
									class="col-lg-12 col-form-label pb-1">Kelurahan</label>
								<div class="col-lg-12">
									<select
										class="form-control {{$errors->has('kelurahan_id') ? 'validatebox-invalid' : '' }}"
										name="kelurahan_id" id="kelurahan-addexit">
										<option value="">-Pilih Kelurahan-</option>
										{{-- @foreach ($villages as $item)
										<option value="{{ $item->id}}" {{
											old('kelurahan_id')==$item->id ? 'selected="selected"': ''
											}}>{{ $item->name}}</option>
										@endforeach --}}
									</select>
									@error('kelurahan_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<div class="col-lg-6">
									<div class="row">
										<label class="col-lg-12 col-form-label pb-1">RT</label>
										<div class="col-lg-12">
											<input type="text" id="rt" name="rt" value="{{old('rt')}}"
												class="form-control {{$errors->has('rt') ? 'validatebox-invalid' : '' }}"
												placeholder="RT" />
											@error('rt') <span class="text-danger">{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="row">
										<label class="col-lg-12 col-form-label pb-1">RW</label>
										<div class="col-lg-12">
											<input type="text" id="rw" name="rw" value="{{old('rw')}}"
												class="form-control {{$errors->has('rw') ? 'validatebox-invalid' : '' }}"
												placeholder="RW" />
											@error('rw') <span class="text-danger">{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="_102_f_address" class="col-lg-12 col-form-label pb-1">Alamat</label>
								<div class="col-lg-12">
									<textarea name="alamat" id="alamat"
										class="form-control {{$errors->has('alamat') ? 'validatebox-invalid' : '' }}"
										autocomplete="off" tabindex="" placeholder="Alamat..."
										style="vertical-align: top;">{{old('alamat')}}
										</textarea>
									@error('alamat') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>

						</div>

						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-12"><b>More</b></span>
								</div>
							</div>

							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Handphone
									No.</label>
								<div class="col-lg-12">
									<input type="number" id="no_phone" name="no_phone" value="{{old('no_phone')}}"
										class="form-control {{$errors->has('no_phone') ? 'validatebox-invalid' : '' }}"
										placeholder="Handphone No." />
									@error('no_phone') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Email</label>
								<div class="col-lg-12">
									<input type="text" id="email" name="email" value="{{old('email')}}"
										class="form-control {{$errors->has('email') ? 'validatebox-invalid' : '' }}"
										placeholder="Email" />
									@error('email') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>

							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Corporate
									Name</label>
								<div class="col-lg-12">
									<input type="text" id="perusahaan_name" name="perusahaan_name"
										value="{{old('perusahaan_name')}}"
										class="form-control {{$errors->has('perusahaan_name') ? 'validatebox-invalid' : '' }}"
										placeholder="Corporate Name" />
									@error('perusahaan_name') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>

						<div class="mt-3 pt-2 pb-3 pl-4 pr-4" style="background-color: #fff">
							<div class="form-group">
								<div class="row">
									<span class="col-lg-12"><b>Detail Test</b></span>
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Inspection
									purpose</label>
								@if($inspeksis)
								<div class="col-lg-12">
									<select id="inspeksi_id" name="inspeksi_id"
										class="form-control {{$errors->has('inspeksi_id') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										@foreach($inspeksis as $inspeksi)
										<option value="{{$inspeksi->id}}" {{ old('inspeksi_id')==$inspeksi->id ?
											'selected="selected"': '' }}>{{$inspeksi->name}}</option>
										@endforeach
									</select>
									@error('inspeksi_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								@endif
							</div>

							<div id="form_detail" class="d-none d-block">
								<div class="form-group row mb-1">
									<label class="col-lg-12 col-form-label pb-1">Detail</label>
									@if($detailInspeksis)
									<div class="col-lg-12">
										<select id="detail_inspeksi_id" name="detail_inspeksi_id" id="f_branch_name"
											class="form-control {{$errors->has('detail_inspeksi_id') ? 'validatebox-invalid' : '' }}">
											<option value="">-Pilih-</option>
											@foreach ($detailInspeksis as $detailInspeksi)
											<option value="{{$detailInspeksi->id}}" {{
												old('detail_inspeksi_id')==$detailInspeksi->id ? 'selected="selected"':
												'' }}>{{$detailInspeksi->name}}</option>
											@endforeach
										</select>
										@error('detail_inspeksi_id') <span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
									@endif
								</div>
							</div>

							<div class="form-group row mb-1">
								<label for="_102_f_status" class="col-lg-12 col-form-label pb-1">Status</label>
								@if($statusSwabs)
								<div class="col-lg-12">
									<select id="status_swab_id" name="status_swab_id" id="f_branch_name"
										class="form-control {{$errors->has('status_swab_id') ? 'validatebox-invalid' : '' }}">
										<option value="">-Pilih-</option>
										@foreach ($statusSwabs as $statusSwab)
										<option value="{{$statusSwab->id}}" {{ old('status_swab_id')==$statusSwab->id ?
											'selected="selected"': '' }}>{{$statusSwab->name}}</option>
										@endforeach
									</select>
									@error('status_swab_id') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								@endif
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Swabber
									Name</label>
								<div class="col-lg-12">
									<input type="text" id="swabber_name" name="swabber_name"
										value="{{old('swabber_name')}}"
										class="form-control {{$errors->has('swabber_name') ? 'validatebox-invalid' : '' }}">
									@error('swabber_name') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Swab Date</label>
								<div class="col-lg-12">
									<input type="datetime-local" id="swabber_date" name="swabber_date"
										value="{{old('swabber_date')}}"
										class="form-control {{$errors->has('swabber_date') ? 'validatebox-invalid' : '' }}">
									@error('swabber_date') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">Pasien
									No.</label>
								<div class="col-lg-9 col-9">
									<input type="text" id="pasien_no_addexit" name="pasien_no" value="{{old('pasien_no')}}"
										class="form-control {{$errors->has('pasien_no') ? 'validatebox-invalid' : '' }}">
									@error('pasien_no') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
								<div class="col-lg-3 col-3">
									<div class="btn btn-sm btn-block btn-info" onclick="addexitGenerateCodePcr()">
										Get No.</div>
								</div>
							</div>

							<div class="form-group row mb-1">
								<label class="col-lg-12 col-form-label pb-1">External No.</label>
								<div class="col-lg-12">
									<input type="text" id="external_no" name="external_no"
										value="{{old('external_no')}}"
										class="form-control {{$errors->has('external_no') ? 'validatebox-invalid' : '' }}">
									@error('external_no') <span class="text-danger">{{ $message }}</span> @enderror
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<!--<button href="javascript:void(0)" class="btn btn-block btn-md btn-primary"
								type="submit">addexit</button>-->
								<a href="javascript:void(0)" class="btn btn-block btn-md btn-primary" iconCls="icon-ok" onclick="simpanubah()" style="width:90px">Save</a>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
@push('scripts')

<script>
	function simpanubah(){
		$.ajax({
            type: 'POST',
			data:$('#form-addexitPcr').serialize(),
            url: "{{route('pcr-store')}}",
            success: function(res) {
				if (res.success) {
					Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
					$('#addexitPcr').modal('hide');
					$("#pcr-table").datagrid('reload');
                    $("#pcr-table").datagrid('clearSelections');
                    $("#pcr-table").datagrid('clearChecked');
				}
			},
            error:function(err){
				if (err.status == 422) { // when status code is 422, it's a validation issue
					console.log(err.responseJSON);
					$('#success_message').fadeIn().html(err.responseJSON.message);
					
					// you can loop through the errors object and show it to the user
					console.warn(err.responseJSON.errors);
					// display errors on each form field
					$.each(err.responseJSON.errors, function (i, error) {
						var el = $(document).find('[name="'+i+'"]');
						el.after($('<span style="color: red;">'+error[0]+'</span>'));
					});
				}
            }
        })
	}

	function addexitGenerateCodePcr(){
        $.ajax({
            type: 'GET',
            dataType: "JSON",
            url: "{{ route('pcr-generate-code') }}",
            success: function(res) {
				if(res.success == true){
					$('#pasien_no_addexit').val(res.data);
				}
				// console.log(res)
			},
            error:function(err){
               console.log(err)
            }
        })
    }

	$(document).ready(function(){
		$('#nik2').keyup(function(){
			nikParse($(this).val(), function(result) {
				console.log(result); // object
				if(result.status=="success"){
					$("#msgnik2").empty();
					var now = new Date(result.data.lahir);
					var day = ("0" + now.getDate()).slice(-2);
					var month = ("0" + (now.getMonth() + 1)).slice(-2);
					var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
					/*$('#dob').val(today);
					if ((result.data.kelamin)=="LAKI-LAKI"){
						$("div.gender select").val("M").change();
					}
					else{
						$("div.gender select").val("F").change();
					}
					*/
				}else{
					$("#msgnik2").empty();
					$('#msgnik2').append("Nik Not Valid");
				}
			});	
		});
		$('#label_use_nar_biodata').click(function(){
			var check = $(this).val(); 
			if (check==true){
				$('#fullname').attr('disabled', 'disabled');
			}
			else{
				$('#fullname').removeAttr('disabled');
			}
			
		})
		$('#branch_id_addexit').change(function() {
			console.log("a");
			$('#branch_name_addexit').val($('option:selected', this).text());
		});

		$("#provinsi-addexit").change(function(){
			$("img#load1").show();
			var id_provinces = $(this).val(); 
			$.ajax({
				type: "POST",
				dataType: "html",
				url: "{{route('regencies')}}",
				data: "id_provinces="+id_provinces,
				success: function(res){
					
					$("select#kota-addexit").html(res);                                                       
					$("img#load1").hide();
					getAjaxKota();                                                        
				}
			});                    
		});  

		$("#kota-addexit").change(getAjaxKota);
		function getAjaxKota(){
			$("img#load2").show();
			var id_regencies = $("#kota-addexit").val();
			$.ajax({
				type: "POST",
				dataType: "html",
				url: "{{route('districts')}}",
				data: "id_regencies="+id_regencies,
				success: function(res){
					$("select#kecamatan-addexit").html(res);                              
					$("img#load2").hide(); 
				getAjaxKecamatan();                                                    
				}
			});
		}

		$("#kecamatan-addexit").change(getAjaxKecamatan);
		function getAjaxKecamatan(){
			$("img#load3").show();
			var id_district = $("#kecamatan-addexit").val();
			$.ajax({
				type: "POST",
				dataType: "html",
				url: "{{route('villages')}}",
				data: "id_district="+id_district,
				success: function(res){
					$("select#kelurahan-addexit").html(res);                              
					$("img#load3").hide();                                                 
				}
			});
		}
	})
</script>
@endpush