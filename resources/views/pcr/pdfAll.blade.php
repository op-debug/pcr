<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<style>
    .divider{
        border-bottom: 5px solid gray;
        margin-top: -20px;
    }
    .divider-1{
        border-bottom: 5px solid gray;
       
    }
    .title p{
        font-size: 14px;
        font-weight: bold;
        text-align: center;
        padding-top: 20px;
    }
    td{
        font-size: 10px;
        line-height: 1.1;
    }
    .table-wrapper-1{
        margin-top: 50px;
    }
    .flex-1{
        margin-bottom: 50px;
    }
    th{
        font-size: 12px;
    }
    @page { margin-top: 100px; }
    body { margin: 5px; }
</style>
<?php
        foreach ($result as $key => $value) {
    ?>
<body>
        <div>
               
            <div style="float: left; margin-top: -119px;position:relative;z-index:-9999 ">
                <img src="data:image/png;base64, {{$value['logo']}}" width="150px" height="150px">
            </div>
            <div style="float: right;">
                <p style="font-size: 9px; margin-top: 2px;">No. Registrasi Lab. Pemeriksa PCR: {{$value['no_registrasi_lab']}} </p>
            </div>
           
        </div>
        <div class="divider"></div>

        <div class="title" style="margin-top: 20px;">
            <p>HASIL LAPORAN TES CORONAVIRUS (COVID-19) <br> CORONAVIRUS (COVID-19) real time RT-PCR RESULT REPORT</p>
        </div>

        <div class="flex-1">

            <div style="float: left;">
            <div class="">
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama Pasien / Name  </td>
                                <td>:</td>
                                <td><strong>{{$value['fullname']}}</strong></td>
                            </tr>
                            <tr>
                                <td>No. Identitas / Patient ID</td>
                                <td>:</td>
                                <td>{{$value['nik']}}</td>
                            </tr>
                            <tr>
                                <td>Kebangsaan / Nationality</td>
                                <td>:</td>
                                <td>{{ $value['nik_type'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td><strong>Sampling Point</strong></td>
                                <td>:</td>
                                <td><strong>{{ $value['nama_cabang'] }}</strong></td>
                            </tr>
                            <tr>
                                <td>Pengambilan Spesimen <br> &nbsp; Date of Specimen Taken</td>
                                <td>:</td>
                                <td>{{ $value['swabber_date'] }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Sample Diterima <br> Receiving date</td>
                                <td>:</td>
                                <td>{{ $value['result_receive_date'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>

            <div style="float: right;">
            <div class="">
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td>Jenis Kelamin / Gender  &nbsp;</td>
                                <td>:</td>
                                <td>{{ $value['jenis_kelamin'] }}</td>
                            </tr>
                            <tr>
                                <td>Tgl. Lahir / DOB</td>
                                <td>:</td>
                                <td>{{ $value['dob'] }}</td>
                            </tr>
                            <tr>
                                <td>Umur / Age&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>
                                <td>:</td>
                                <td>{{ $value['umur'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="py-2">
                    <table>
                        <tbody>
                            <tr>
                                <td>Tanggal Analisis Date of <br>  Specimen Taken Date of Analysis</td>
                                <td>:</td>
                                <td>{{ $value['result_analysis_date'] }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal hasil keluar <br> Receiving date Result date</td>
                                <td>:</td>
                                <td>{{ $value['result_date'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>

        </div>

        <div class="table-wrapper-1" style="padding-top: 100px;">
            <table class="table">
                <thead>
                    <tr class="table-info">
                        <th class="text-center">Test Name</th>
                        <th class="text-center">Reagent</th>
                        <th class="text-center">Gent Target</th>
                        <th class="text-center">Ct. Value</th>
                        <th class="text-center">Result</th>
                        <th class="text-center">Test Method</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">{{ $value['test_name'] }}</td>
                        <td align="center">eDiagnosis</td>
                        <td align="center">Orf1ab – N Gene</td>
                        <td align="center">-</td>
                        <td align="center"><strong>{{ $value['hasil_test'] }}</strong></td>
                        <td align="center">Real Time RT-PCR</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">Bahwa hasil tes Coronavirus (COVID-19) adalah/That the Corona Virus (COVID-19) test result is: 
                            <strong>{{ $value['hasil_test'] }}</strong>
                        </td>
                    </tr>
                </tfoot>
            </table>
            <p style="font-size: 12px; margin-left:10px;">Jenis Spesimen / Specimen type : Naso and Oropharyngeal swab</p>
            <p style="font-size: 12px; margin-left:10px; font-weight: bold;">Catatan / Note:</p>
            <div>
                <ol style="font-size: 10px;">
                    <ul style="font-size: 10px;">
                        <li>PCR mendeteksi materi genetic virus tanpa dapat membedakan virus hidup atau mati / PCR test only detects viral
                        genetic material without being able to distinguish living and dead virus </li>
                        <li>Hasil pemeriksaan tidak dapat dibandingkan antar laboratorium dikarenakan berbagai factor antara lain jenis reagen,
                         mesin, gen target, dll / PCR test result should not be compared between laboratories due to various factors e.g. type of reagent, machine, target gene, etc</li>
                        <li>Hasil hanya menggambarkan kondisi saat pengambilan sampel, sesuaikan dengan klinis pasien. Pemeriksaan ulang
                            dapat dilakukan atas saran dokter penanggung jawab./ The result may only reflect the day when specimen was
                            taken, please consult to your physician for follow up examination.
                        </li>
                        <li>Cut Off CT value of the target genes : 40 </li>
                    </ul>
                </ol>
            </div>
            
        </div>

        <div style="padding-top: 60px;">
            <div style="float: left;">
                <div>
                    <img src="data:image/png;base64, {{$value['qrpatient']}}">
                </div>
                <div>
                    <p style="font-size: 14px;">{{$value['lab_patient_no']}}</p>
                </div>
            </div>
            <div style="float: right;">
                <div>
                    <p style="font-size: 12px;">Dokter Penanggung Jawab<br/> Doctor in charge</p>
                </div>
                <div>
                    <img src="data:image/png;base64, {{$value['ttd_img']}}"  width="200px" height="90px">
                </div>
                <div>
                    <p style="font-size: 12px; border-bottom: 2px solid gray; font-weight: 700;">{{$value['dokter']}}</p>
                    <p style="font-size: 10px; margin-top: -10px;">{{$value['no_sip']}}</p>
                </div>
            </div>      
        </div>
        
        
        <div style="padding-top: 190px;padding-buttom:10px">
            <div class="divider-1"></div>
            <strong>{{$value['title_footer']}}</strong>
            <label style="font-size: 12px;">{{$value['alamat']}}<br/>
            Contact Person : {{$value['phone']}}</label>
        </div>

    </body>
        
    <?php
            }
        ?>
</html>