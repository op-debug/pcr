@extends('app')

@section('content')
<section id="user">
  <div class="row match-height">
      <div class="col-xl-12 col-md-6 col-12">
        <div class="easyui-layout" data-options="fit:true" style="width: 1237px; height: 154px;">
          <div data-options="region:'center', border:false, split:true">
            <div class="easyui-layout" data-options="fit:true">
              <div data-options="region:'center', split:true, title:'List user'" >
                <table id="_user_grid"
                url="{{url('api/user/getdata')}}",>
                </table>

                  <div id="_user_grid_toolbar" class="p-1">
                      <div class="row">
                        <div class="col-12">
                          
                          <div style="float: left">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_user_grid_add()"><i class="fa fa-plus fa-fw fa-lg" style="color:green"></i>&nbsp;Add</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_user_grid_edit()"><i class="fa fa-edit fa-fw fa-lg" style="color:green"></i>&nbsp;Edit</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true" onclick="_jf_user_grid_delete()"><i class="fa fa-trash fa-fw fa-lg" style="color:green"></i>&nbsp;Delete</a>
                          </div>
                        </div>
                      </div>
              </div>
          </div>
        </div>
    </div>
  </div>

  <div id="_user_grid_dialog" class="easyui-dialog" style="width:50%;height:420px;padding:10px 25px"
        closed="true" buttons="#dlg-buttons">
    <form id="fm" method="post"  novalidate>
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="u_name" class="form-control" required="true">
        </div>
        <div  class="form-group">
            <label>Level User</label>
            <select class="form-control" id="typeuser" name="u_usertype">
                <option value="admin">Admin</option>
                <option value="labekstraksi">Lab Ekstrasi</option>
                <option value="labpcr">Lab PCR</option>
                <option value="mitra">User Mitra</option>
                <option value="branch">User Branch</option>
            </select>
        </div>
        <div class="form-group" id="selectmitra" style="display:none;">
            <label>Mitra</label>
            <select name="mitra_id" id="mitra_id"
									class="form-control {{$errors->has('mitra_id') ? 'validatebox-invalid' : '' }}">
                  <option value="">-Pilih-</option>
									@foreach ($mitras as $mitra)
									<option value="{{$mitra->id}}" {{ old('mitra_id')==$mitra->id ?
										'selected="selected"': '' }}>{{$mitra->m_name}}</option>
									@endforeach
            </select>
        </div>
        <div class="form-group" style="display:none;" id="selectbranch">
            <label>Branch</label>
            <select
                class="form-control"
                name="branch_id" id="branch">
                <option value="">-Select Branch-</option>
              </select>
              <img src="{{ asset('public/app-assets/images/loading.gif')}}" width="35" id="load2"
                style="display:none;" />
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="u_username" class="form-control" >
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="u_password" class="form-control">
        </div>
    </form>
  </div>
  <div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveuser()" style="width:90px">Save</a>
</div>
</section>
<script type="text/javascript" src="{{ asset('public/easyui/jquery.min.js')}}"></script>

<script type="text/javascript">
  var url;
  $(function(){
    $("#_user_grid").datagrid({
      toolbar: "#_user_grid_toolbar",
      border:false,
      striped: true,
      pagination: true,
      fit: true,
      fitColumns: false,
      rownumbers: true,
      checkbox: true,
      singleSelect: true,
      selectOnCheck: false,
      checkOnSelect: false,
      nowrap: false,
      pageList: [10, 20, 30, 40, 50, 100],
      pageSize: 100,
      method: "GET",
      idField: "id",
      
      columns: [[
        {field: "u_name",title: "<b>Name</b>",align: "left",width: 200},
        {field: "mitra",title: "<b>Mitra</b>",align: "left",width: 200},
        {field: "branch",title: "<b>Branch</b>",align: "left",width: 200},
        {field: "u_username",title: "<b>Username</b>",align: "left",width: 200},
        {field: "u_usertype",title: "<b>Type Type</b>",align: "left",width: 225},
      ]],
    })
    $("#typeuser").change(function(){
      var type = $(this).val();
      if (type=="mitra"){
          $("#selectmitra").show();
          $("#selectbranch").hide();
      }
      else if(type=="branch"){
          $("#selectmitra").show();
          $("#selectbranch").show();
      }
      else if(type!="branch" && type!="mitra"){
        $("#selectmitra").hide();
        $("#selectbranch").hide();
      }
    });
    $("#mitra_id").change(function(){
        $("img#load1").show();
        var mitra_id = $(this).val();  
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "{{route('selectbranch')}}",
            data: "mitra_id="+mitra_id,
            success: function(res){
                $("select#branch").html(res);                                                       
                $("img#load1").hide();                                                      
            }
        });                    
    });  
  })

  function _jf_pcr_grid_filter_g(){
        var narStatus = $("#_mitra_grid_filter_nar_status").combobox('getValue');
        $("#_mitra_grid_filter_nar_status_front").combobox('setValue',narStatus);

        var filterParams = $("#_mitra_grid_filter_f").serialize();
        return filterParams;
    }
 

    function _jf_mitra_grid_filter_r(){
        $("#_mitra_grid_filter_nar_status").combobox('setValue','sAll');
        _jf_mitra_grid_filter_p();
    }

  function _jf_user_grid_add(){
    $('#_user_grid_dialog').dialog('open').dialog('setTitle','New user');
    $('#fm').form('clear');
    url= "{{url('api/user/store')}}";
  }
  
  function _jf_user_grid_edit(){
    var row = $('#_user_grid').datagrid('getSelected');
		if (row){
      $('#_user_grid_dialog').dialog('open').dialog('setTitle','Edit User');
      $('#fm').form('load',row);
      url = "{{url('api/user/update')}}/"+row.id;
    }
    else{
      $.messager.alert("Edit User","Select Mitra first.");
    }
  }

  function saveuser(){
    $.ajax({
        type: "POST",
        url: url,
        data:$('#fm').serialize(),
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#_user_grid_dialog').dialog('close'); 
                $('#_user_grid').datagrid('reload');   
            }
        }
    });
  }

  function _jf_user_grid_delete(){
			var row = $('#_user_grid').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this User?',function(r){
					if (r){
						$.post("{{url('api/user/delete')}}",{id:row.id},function(result){
							if (result.success){
								$('#_user_grid').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							}
						},'json');
					}
				});
			}
		}
 
</script>
@endsection

