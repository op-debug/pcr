<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/dashbord/json_grafik','App\Http\Controllers\DashboardController@json_grafik')->name('dashboard.json_grafik');
Route::get('/type/getdata','App\Http\Controllers\TypeController@getdata')->name('type.getdata');
Route::post('/type/store','App\Http\Controllers\TypeController@store')->name('type.store');
Route::post('/type/update/{id}','App\Http\Controllers\TypeController@update')->name('type.update');
Route::post('/type/delete','App\Http\Controllers\TypeController@delete')->name('type.delete');
Route::get('/branch/getdata','App\Http\Controllers\BranchController@getdata')->name('branch.getdata');
Route::post('/branch/store','App\Http\Controllers\BranchController@store')->name('branch.store');
Route::post('/branch/update/{id}','App\Http\Controllers\BranchController@update')->name('branch.update');
Route::post('/branch/delete','App\Http\Controllers\BranchController@delete')->name('branch.delete');
Route::get('/user/getdata','App\Http\Controllers\UserController@getdata')->name('user.getdata');
Route::post('/user/store','App\Http\Controllers\UserController@store')->name('user.store');
Route::post('/user/update/{id}','App\Http\Controllers\UserController@update')->name('user.update');
Route::post('/user/delete','App\Http\Controllers\UserController@delete')->name('user.delete');
Route::get('/mitra/getdata','App\Http\Controllers\MitraController@getdata')->name('mitra.getdata');
Route::post('/mitra/store','App\Http\Controllers\MitraController@store')->name('mitra.store');
Route::post('/mitra/update/{id}','App\Http\Controllers\MitraController@update')->name('mitra.update');
Route::post('/mitra/delete','App\Http\Controllers\MitraController@delete')->name('mitra.delete');
Route::get('/pcr/getdata','App\Http\Controllers\PcrController@getdata')->name('pcr.getdata');
Route::delete('/pcr/delete','App\Http\Controllers\PcrController@delete')->name('pcr.delete');
Route::post('/pcr/store','App\Http\Controllers\PcrController@store')->name('pcr.store');
Route::post('/pcr/update','App\Http\Controllers\PcrController@update')->name('pcr.update');
Route::get('/pcr/getbranch','App\Http\Controllers\PcrController@getbranch')->name('pcr.getbranch');
Route::post('/pcr/send_to_lab','App\Http\Controllers\PcrController@send_to_lab')->name('pcr.send_to_lab');
Route::get('/ekstraksi/getdata','App\Http\Controllers\EkstraksiController@getdata')->name('ekstraksi.getdata');
Route::delete('/ekstraksi/delete','App\Http\Controllers\EkstraksiController@destroy')->name('ekstraksi.delete');
Route::post('/ekstraksi/update/{id}','App\Http\Controllers\EkstraksiController@update')->name('ekstraksi.update');
Route::get('/ekstraksi/validasi','App\Http\Controllers\EkstraksiController@validasi')->name('ekstraksi.validasi');
Route::get('/ekstraksi/interpretasi','App\Http\Controllers\EkstraksiController@interpretasi')->name('ekstraksi.interpretasi');
Route::get('/interpretasi/getdata','App\Http\Controllers\InterpretasiController@getdata')->name('interpretasi.getdata');
Route::delete('/interpretasi/delete','App\Http\Controllers\InterpretasiController@destroy')->name('interpretasi.delete');
Route::post('/interpretasi/update/{id}','App\Http\Controllers\InterpretasiController@update')->name('interpretasi.update');
Route::post('/pcrdikirim/send_to_diterima','App\Http\Controllers\PcrDikirimController@send_to_diterima')->name('pcr.send_to_diterima');
Route::post('/pcrdikirim/send_to_entry','App\Http\Controllers\PcrDikirimController@send_to_entry')->name('pcr.send_to_entry');
Route::post('/pcrditerima/send_to_ekstraksi','App\Http\Controllers\PcrDiterimaController@send_to_ekstraksi')->name('pcr.send_to_ekstraksi');
Route::post('/pcrditerima/send_to_dikirim','App\Http\Controllers\PcrDiterimaController@send_to_dikirim')->name('pcr.send_to_dikirim');
Route::post('/pcrekstraksi/send_to_finishekstraksi','App\Http\Controllers\PcrEkstraksiController@send_to_selesaiekstraksi')->name('pcr.send_to_finishekstraksi');
Route::post('/pcrekstraksi/send_to_diterima','App\Http\Controllers\PcrEkstraksiController@send_to_diterima')->name('pcr.send_to_diterima');
Route::post('/labpcr/send_to_analize','App\Http\Controllers\LabPCRController@send_to_analize')->name('pcr.analize');
Route::post('/labpcr/send_to_ekstraksi','App\Http\Controllers\LabPCRController@send_to_ekstraksi')->name('pcr.ekstraksi');
