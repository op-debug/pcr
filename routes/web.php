<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'App\Http\Controllers\LoginController@login')->name('login');
Route::post('actionlogin', 'App\Http\Controllers\LoginController@actionlogin')->name('actionlogin');
Route::get('importnationaly', 'App\Http\Controllers\NationalyController@import_excel')->name('importnationaly');


Route::group(['middleware' => 'checkRole:admin,mitra,branch,labekstraksi,labpcr'], function () {
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
    Route::get('/actionlogout', 'App\Http\Controllers\LoginController@actionlogout')->name('actionlogout');
    Route::get('/setting_user', 'App\Http\Controllers\UserController@setting_user')->name('setting_user');
    Route::post('/setting_user_save', 'App\Http\Controllers\UserController@setting_user_save')->name('setting_user_save');
    Route::post('/selectbranch', 'App\Http\Controllers\DependentDropdownController@branch')->name('selectbranch');
    Route::post('/selectallbranch', 'App\Http\Controllers\DependentDropdownController@allbranch')->name('selectallbranch');
    Route::post('/getpatient', 'App\Http\Controllers\DependentDropdownController@getpatient')->name('getpatient');
});

Route::group(['middleware' => 'checkRole:admin,mitra,branch'], function () {
    Route::post('/pcr-send-email', 'App\Http\Controllers\PcrController@sendEmail')->name('pcr-send-email');
    Route::get('/pcr', 'App\Http\Controllers\PcrController@index')->name('pcr');
    Route::get('/pcr-generate-code', 'App\Http\Controllers\PcrController@generateCode')->name('pcr-generate-code');
    Route::post('/pcr-store', 'App\Http\Controllers\PcrController@store')->name('pcr-store');
    Route::post('/pcr-update', 'App\Http\Controllers\PcrController@update')->name('pcr-update');
    Route::delete('/pcr-destroy', 'App\Http\Controllers\PcrController@destroy')->name('pcr-destroy');
    Route::post('/pcr-table', 'App\Http\Controllers\PcrController@table')->name('pcr-table');
    Route::post('/pcr-table/{id}', 'App\Http\Controllers\PcrController@pcrBranch')->name('pcr-branch');
    Route::get('/pcr-pdfAll', 'App\Http\Controllers\PcrController@pdfAll')->name('pcr-pdfAll');
    Route::get('/get-branch', 'App\Http\Controllers\PcrController@getBranch')->name('get-branch');
    Route::post('/filter-branch', 'App\Http\Controllers\PcrController@filterBranch')->name('filter-branch');
    Route::get('/antigent', 'App\Http\Controllers\AntigentController@index')->name('antigent');
    Route::get('/antigent-table', 'App\Http\Controllers\AntigentController@table')->name('antigent-table');
    Route::get('/antigent-store', 'App\Http\Controllers\AntigentController@store')->name('antigent-store');
    Route::post('/regencies', 'App\Http\Controllers\DependentDropdownController@regencies')->name('regencies');
    Route::post('/districts', 'App\Http\Controllers\DependentDropdownController@districts')->name('districts');
    Route::post('/villages', 'App\Http\Controllers\DependentDropdownController@villages')->name('villages');
    Route::get('/pcr-excelAll', 'App\Http\Controllers\PcrController@excelAll')->name('pcr-excelAll');
    Route::get('/pcr-excelAll', 'App\Http\Controllers\PcrController@excelAll')->name('pcr-excelAll');
    Route::post('/filter-branch', 'App\Http\Controllers\PcrController@filterBranch')->name('filter-branch');
    Route::post('/pcr-filter', 'App\Http\Controllers\PcrController@filter')->name('pcr-filter');
    Route::get('/antigen', 'App\Http\Controllers\AntigenController@index')->name('antigen');
    Route::post('/antigen-table', 'App\Http\Controllers\AntigenController@table')->name('antigen-table');
    Route::post('/antigen-store', 'App\Http\Controllers\AntigenController@store')->name('antigen-store');
    Route::post('/antigen-update', 'App\Http\Controllers\AntigenController@update')->name('antigen-update');
    Route::delete('/antigen-destroy', 'App\Http\Controllers\AntigenController@destroy')->name('antigen-destroy');
    Route::get('/antigen-generate-code', 'App\Http\Controllers\AntigenController@generateCode')->name('antigen-generate-code');
    Route::get('/antigen-excelAll', 'App\Http\Controllers\AntigenController@store')->name('antigen-excelAll');
    Route::get('/qrpatient', 'App\Http\Controllers\QrpatientController@index')->name('qrpatient');
    Route::post('/qrpatient-store', 'App\Http\Controllers\QrpatientController@store')->name('qrpatient-store');
    Route::post('/qrpatient-table', 'App\Http\Controllers\QrpatientController@table')->name('qrpatient-table');
    Route::get('/kwitansi', 'App\Http\Controllers\KwitansiController@index')->name('kwitansi');
    Route::post('/kwitansi-store', 'App\Http\Controllers\KwitansiController@store')->name('kwitansi-store');
    Route::post('/kwitansi-update', 'App\Http\Controllers\KwitansiController@update')->name('kwitansi-update');
    Route::delete('/kwitansi-destroy', 'App\Http\Controllers\KwitansiController@destroy')->name('kwitansi-destroy');
    Route::post('/kwitansi-addmore', 'App\Http\Controllers\KwitansiController@addmore')->name('kwitansi-addmore');
    Route::post('/kwitansi-table', 'App\Http\Controllers\KwitansiController@table')->name('kwitansi-table');
    Route::post('/pcr/importexcel', 'App\Http\Controllers\PcrController@import_excel')->name('pcr-importexcel');
    Route::get('/qr-pdfAll', 'App\Http\Controllers\QrpatientController@printPDF')->name('qr-pdfAll');
    Route::get('/kwitansi-pdfAll', 'App\Http\Controllers\KwitansiController@printkwitansi')->name('kwitansi-pdfAll');
    Route::get('/printlabelsatu', 'App\Http\Controllers\PcrController@printlabelsatu')->name('printlabelsatu');
    Route::get('/printlabeldua', 'App\Http\Controllers\PcrController@printlabeldua')->name('printlabeldua');
    Route::post('/pcr-setqr', 'App\Http\Controllers\PcrController@pcrsetqr')->name('pcr-setqr');
    Route::post('/pcr-validdata', 'App\Http\Controllers\PcrController@validdata')->name('pcr-validdata');
    Route::get('/pcr-zip', 'App\Http\Controllers\PcrController@downloadzip')->name('pcr-zip');
});

Route::group(['middleware' => 'checkRole:admin'], function () {
    Route::get('/branch','App\Http\Controllers\BranchController@index')->name('branch');
    Route::get('/mitra','App\Http\Controllers\MitraController@index')->name('mitra');
    Route::get('/type','App\Http\Controllers\TypeController@index')->name('type');
    Route::get('/program','App\Http\Controllers\SettingController@index')->name('setting-program');
    Route::get('/user','App\Http\Controllers\UserController@index')->name('user');
    Route::get('/setting', 'App\Http\Controllers\SettingController@index')->name('setting');
    Route::post('/setting-save', 'App\Http\Controllers\SettingController@save')->name('setting-save');
});

Route::group(['middleware' => 'checkRole:labekstraksi,admin'], function () {
    Route::get('/pcrdikirim', 'App\Http\Controllers\PCRDikirimController@index')->name('pcrdikirim');
    Route::post('/pcrdikirim-table', 'App\Http\Controllers\PCRDikirimController@table')->name('pcrdikirim-table');
    Route::post('/pcrdikirim-filter', 'App\Http\Controllers\PCRDikirimController@filter')->name('pcrdikirim-filter');
    Route::get('/pcrditerima', 'App\Http\Controllers\PCRDiterimaController@index')->name('pcrditerima');
    Route::post('/pcrditerima-table', 'App\Http\Controllers\PCRDiterimaController@table')->name('pcrditerima-table');
    Route::post('/pcrditerima-filter', 'App\Http\Controllers\PCRDiterimaController@filter')->name('pcrditerima-filter');
    Route::get('/pcrprosessekstraksi', 'App\Http\Controllers\PCREkstraksiController@index')->name('pcrprosessekstraksi');
    Route::post('/pcrprosessekstraksi-table', 'App\Http\Controllers\PCREkstraksiController@table')->name('pcrprosessekstraksi-table');
    Route::post('/pcrprosessekstraksi-filter', 'App\Http\Controllers\PCREkstraksiController@filter')->name('pcrprosessekstraksi-filter');    
});
Route::group(['middleware' => 'checkRole:labpcr,admin'], function () {
    Route::get('/interpretasi', 'App\Http\Controllers\InterpretasiController@index')->name('interpretasi');
    Route::get('/listpcr', 'App\Http\Controllers\LabPCRController@index')->name('listpcr');
    Route::post('/labpcr-table', 'App\Http\Controllers\LabPCRController@table')->name('labpcr-table');
    Route::post('/labpcr-filter', 'App\Http\Controllers\LabPCRController@filter')->name('labpcr-filter');
    Route::get('/analizepcr', 'App\Http\Controllers\AnalizePCRController@index')->name('analizepcr');
    Route::post('/analizepcr-table', 'App\Http\Controllers\AnalizePCRController@table')->name('analizepcr-table');
    Route::post('/analizepcr-filter', 'App\Http\Controllers\AnalizePCRController@filter')->name('analizepcr-filter');
    Route::get('/pcr_result', 'App\Http\Controllers\EkstraksiController@index')->name('pcr_result');
    Route::post('/pcr_result/import_excel', 'App\Http\Controllers\EkstraksiController@import_excel')->name('pcr_result.import_excel');    
});
